USE db_hollvale;
INSERT INTO t_role (id, creation_time, name) VALUES (1, sysdate(), 'ADMIN');
INSERT INTO t_role (id, creation_time, name) VALUES (2, sysdate(), 'JOBSEEKER');
INSERT INTO t_role (id, creation_time, name) VALUES (3, sysdate(), 'COMPANY_ADMIN');
INSERT INTO t_role (id, creation_time, name) VALUES (4, sysdate(), 'COMPANY_JOBSEEKER_MANAGER');
INSERT INTO t_role (id, creation_time, name) VALUES (5, sysdate(), 'COMPANY_JOB_ADVERTISEMENT_MANAGER');
INSERT INTO t_role (id, creation_time, name) VALUES (6, sysdate(), 'COMPANY_PROFILE_MANAGER');

INSERT INTO t_permission(id, creation_time, name) VALUES (1, sysdate(), 'CAN_DELETE_OWN_PROFILE');
INSERT INTO t_permission(id, creation_time, name) VALUES (2, sysdate(), 'CAN_DELETE_COMPANY');
INSERT INTO t_permission(id, creation_time, name) VALUES (3, sysdate(), 'CAN_DELETE_EMPLOYEE');

INSERT INTO t_permission(id, creation_time, name) VALUES (4, sysdate(), 'CAN_SEARCH');
INSERT INTO t_permission(id, creation_time, name) VALUES (5, sysdate(), 'CAN_DECIDE');
INSERT INTO t_permission(id, creation_time, name) VALUES (6, sysdate(), 'CAN_SEND_MESSAGE');

INSERT INTO t_permission(id, creation_time, name) VALUES (7, sysdate(), 'CAN_LIST_EMPLOYEES');
INSERT INTO t_permission(id, creation_time, name) VALUES (8, sysdate(), 'CAN_CREATE_EMPLOYEE');
INSERT INTO t_permission(id, creation_time, name) VALUES (9, sysdate(), 'CAN_MODIFY_COMPANYPROFILE');

INSERT INTO t_permission(id, creation_time, name) VALUES (10, sysdate(), 'CAN_CREATE_JOBADVERTISEMENT');
INSERT INTO t_permission(id, creation_time, name) VALUES (11, sysdate(), 'CAN_MODIFY_JOBADVERTISEMENT');
INSERT INTO t_permission(id, creation_time, name) VALUES (12, sysdate(), 'CAN_DELETE_JOBADVERTISEMENT');

INSERT INTO t_permission(id, creation_time, name) VALUES (13, sysdate(), 'JOBSEEKER_PERMISSIONS');

INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,1);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,2);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,3);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,4);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,5);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,6);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,7);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,8);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,9);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,10);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,11);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,12);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(1,13);

INSERT INTO role_x_permission(role_id, permission_id) VALUES(2,13);

INSERT INTO role_x_permission(role_id, permission_id) VALUES(3,1);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(3,2);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(3,3);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(3,4);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(3,5);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(3,6);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(3,7);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(3,8);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(3,9);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(3,10);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(3,11);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(3,12);

INSERT INTO role_x_permission(role_id, permission_id) VALUES(4,4);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(4,5);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(4,6);

INSERT INTO role_x_permission(role_id, permission_id) VALUES(5,10);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(5,11);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(5,12);

INSERT INTO role_x_permission(role_id, permission_id) VALUES(6,7);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(6,8);
INSERT INTO role_x_permission(role_id, permission_id) VALUES(6,9);








