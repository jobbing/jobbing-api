package hu.hollvale.jobbing.util;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import hu.hollvale.jobbing.enums.MatchStateType;
import hu.hollvale.jobbing.exception.InvalidInputException;
import hu.hollvale.jobbing.exception.NotFoundException;
import hu.hollvale.jobbing.model.match.Match;

class MatchUtilTest {

	@Test
	void rejectMatchByJobseekerWhenCompanyAccepted() {
		Optional<Match> matchOptional = this.createMatchOptionalWithState(MatchStateType.COMPANY_ACCEPT);

		Match matchToUpdate = MatchUtil.jobseekerRejectCompany(matchOptional);

		assertTrue(matchToUpdate.getState().equals(MatchStateType.JOBSEEKER_REJECT));
	}

	@Test
	void rejectMatchByJobseekerWhenCompanyRejected() {
		Optional<Match> matchOptional = this.createMatchOptionalWithState(MatchStateType.COMPANY_REJECT);

		Assertions.assertThrows(InvalidInputException.class, () -> MatchUtil.jobseekerRejectCompany(matchOptional));
	}

	@Test
	void rejectMatchByJobseekerWhenJobseekerRejected() {
		Optional<Match> matchOptional = this.createMatchOptionalWithState(MatchStateType.JOBSEEKER_REJECT);

		Assertions.assertThrows(InvalidInputException.class, () -> MatchUtil.jobseekerRejectCompany(matchOptional));
	}

	@Test
	void rejectMatchByJobseekerWhenMissingMatch() {
		Optional<Match> empty = Optional.empty();

		Assertions.assertThrows(NotFoundException.class, () -> MatchUtil.jobseekerRejectCompany(empty));
	}

	@Test
	void jobseekerAcceptCompanyWhenCompanyAccepted() {
		Optional<Match> matchOptional = this.createMatchOptionalWithState(MatchStateType.COMPANY_ACCEPT);

		Match matchToUpdate = MatchUtil.jobseekerAcceptCompany(matchOptional);

		assertTrue(matchToUpdate.getState().equals(MatchStateType.MATCH));
	}

	@Test
	void jobbseekerAcceptCompanyWhenCompanyRejected() {
		Optional<Match> matchOptional = this.createMatchOptionalWithState(MatchStateType.COMPANY_REJECT);

		Assertions.assertThrows(InvalidInputException.class, () -> MatchUtil.jobseekerAcceptCompany(matchOptional));

	}

	@Test
	void jobbseekerAcceptCompanyWhenMissingMatch() {
		Optional<Match> empty = Optional.empty();

		Assertions.assertThrows(NotFoundException.class, () -> MatchUtil.jobseekerAcceptCompany(empty));
	}

	@Test
	void jobseekerAcceptCompanyWhenAlreadyAccepted() {
		Optional<Match> matchOptional = this.createMatchOptionalWithState(MatchStateType.JOBSEEKER_ACCEPT);

		Assertions.assertThrows(InvalidInputException.class, () -> MatchUtil.jobseekerAcceptCompany(matchOptional));

	}

	@Test
	void jobseekerAcceptCompanyWhenAlreadyMatch() {
		Optional<Match> matchOptional = this.createMatchOptionalWithState(MatchStateType.MATCH);

		Assertions.assertThrows(InvalidInputException.class, () -> MatchUtil.jobseekerAcceptCompany(matchOptional));

	}

	private Optional<Match> createMatchOptionalWithState(MatchStateType matchStateType) {
		// @formatter:off
		return Optional.of(Match.builder()
				.state(matchStateType)
				.build()
		);
		// @formatter:on

	}

}
