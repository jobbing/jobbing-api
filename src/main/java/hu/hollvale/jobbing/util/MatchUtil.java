package hu.hollvale.jobbing.util;

import java.util.Optional;

import hu.hollvale.jobbing.enums.MatchStateType;
import hu.hollvale.jobbing.enums.Message;
import hu.hollvale.jobbing.exception.InvalidInputException;
import hu.hollvale.jobbing.exception.NotFoundException;
import hu.hollvale.jobbing.model.match.Match;

public class MatchUtil {

	public static Match jobseekerRejectCompany(Optional<Match> foundedMatch) {

		Match match = getMatchIfExists(foundedMatch);

		checkAlreadyDecidedWhenJobseekerRejects(match);

		return setMatchState(match, MatchStateType.JOBSEEKER_REJECT);
	}

	public static Match companyAcceptJobseeker(Optional<Match> foundedMatch) {

		throwAlreadyDecidedIfMatchExists(foundedMatch);

		return createMatchWithState(MatchStateType.COMPANY_ACCEPT);
	}

	public static Match jobseekerAcceptCompany(Optional<Match> matchOptional) {
		Match match = getMatchIfExists(matchOptional);

		checkAlreadyDecidedWhenJobseekerAccept(match);

		return createMatchWithState(MatchStateType.MATCH);

	}

	public static Match companyRejectJobseeker(Optional<Match> foundedMatch) {

		throwAlreadyDecidedIfMatchExists(foundedMatch);

		return createMatchWithState(MatchStateType.COMPANY_REJECT);
	}

	private static void checkAlreadyDecidedWhenJobseekerAccept(Match match) {
		if (match.getState() == MatchStateType.COMPANY_REJECT || match.getState() == MatchStateType.JOBSEEKER_ACCEPT
				|| match.getState() == MatchStateType.MATCH) {
			throw new InvalidInputException(Message.ALREADY_DECIDED);
		}

	}

	private static Match createMatchWithState(MatchStateType state) {
		return Match.builder().state(state).build();
	}

	private static Match setMatchState(Match match, MatchStateType matchStateType) {
		match.setState(matchStateType);

		return match;
	}

	private static Match getMatchIfExists(Optional<Match> foundedMatch) {
		return foundedMatch.orElseThrow(() -> new NotFoundException(Message.MATCH_NOT_FOUND));
	}

	private static void throwAlreadyDecidedIfMatchExists(Optional<Match> foundedMatch) {
		if (foundedMatch.isPresent()) {
			throw new InvalidInputException(Message.ALREADY_DECIDED);
		}
	}

	private static void checkAlreadyDecidedWhenJobseekerRejects(Match match) {
		if (match.getState() == MatchStateType.COMPANY_REJECT || match.getState() == MatchStateType.JOBSEEKER_REJECT) {
			throw new InvalidInputException(Message.ALREADY_DECIDED);
		}
	}

	private static void checkAlreadyDecidedWhenCompanyRejects(Match match) {
		if (match.getState() == MatchStateType.COMPANY_REJECT || match.getState() == MatchStateType.JOBSEEKER_REJECT) {
			throw new InvalidInputException(Message.ALREADY_DECIDED);
		}
	}

}
