package hu.hollvale.jobbing.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;

import hu.hollvale.jobbing.dto.company.JobseekerDto;
import hu.hollvale.jobbing.dto.filter.JobseekerFilter;

public interface CompanyFindJobseekerController {

	Optional<JobseekerDto> pageJobseekers(@RequestBody JobseekerFilter jobseekerFilter, Pageable pageable);

	List<JobseekerDto> getMatchedJobseekers(Long companyId);

}
