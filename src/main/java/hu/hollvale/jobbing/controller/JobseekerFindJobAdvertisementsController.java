package hu.hollvale.jobbing.controller;

import java.util.Optional;

import org.springframework.data.domain.Pageable;

import hu.hollvale.jobbing.dto.company.JobAdvertisementFindDto;
import hu.hollvale.jobbing.dto.filter.JobAdvertisementsFilter;

public interface JobseekerFindJobAdvertisementsController {

	Optional<JobAdvertisementFindDto> pageJobAdvertisements(JobAdvertisementsFilter pageJobAdvertisementsDto,
			Pageable pageable);
}
