package hu.hollvale.jobbing.controller;

import hu.hollvale.jobbing.dto.UserRegisterDto;
import hu.hollvale.jobbing.dto.company.CompanyRegisterDto;
import hu.hollvale.jobbing.dto.user.LoginDto;
import hu.hollvale.jobbing.dtol.authentication.AuthenticationRequest;

public interface PublicController {

	public Long createUser(UserRegisterDto userRegisterDto);

	public LoginDto loginUser(AuthenticationRequest authenticationRequest);

	public Long registerCompany(CompanyRegisterDto companyRegisterDto);

}
