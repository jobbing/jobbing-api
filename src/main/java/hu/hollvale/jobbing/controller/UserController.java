package hu.hollvale.jobbing.controller;

import hu.hollvale.jobbing.dto.user.EmailChangeDto;
import hu.hollvale.jobbing.dto.user.GetMyDatasDto;
import hu.hollvale.jobbing.dto.user.JobseekerSettingsDto;
import hu.hollvale.jobbing.dto.user.NameChangeDto;
import hu.hollvale.jobbing.dto.user.PasswordChangeDto;
import hu.hollvale.jobbing.dto.user.UsernameChangeDto;
import hu.hollvale.jobbing.enums.HolderType;

public interface UserController {

	public void changeUsername(UsernameChangeDto usernameChangeDto);

	public void changeName(NameChangeDto nameChangeDto);

	public void changePassword(PasswordChangeDto passwordChangeDto);

	public void changeEmail(EmailChangeDto emailChangeDto);

	public void deleteUser(String username);

	public JobseekerSettingsDto getUserDatasToSettings(String username);

	public GetMyDatasDto getMyDatas(HolderType holderType);

}
