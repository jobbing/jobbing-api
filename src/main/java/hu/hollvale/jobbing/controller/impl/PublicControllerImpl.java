package hu.hollvale.jobbing.controller.impl;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.hollvale.jobbing.controller.PublicController;
import hu.hollvale.jobbing.dto.UserRegisterDto;
import hu.hollvale.jobbing.dto.company.CompanyRegisterDto;
import hu.hollvale.jobbing.dto.user.LoginDto;
import hu.hollvale.jobbing.dtol.authentication.AuthenticationRequest;
import hu.hollvale.jobbing.service.PublicService;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/public")
public class PublicControllerImpl implements PublicController {

	private final PublicService publicService;

	@Override
	@ResponseBody
	@PostMapping(value = "/register-jobseeker")
	public Long createUser(@RequestBody UserRegisterDto userRegisterDto) {
		return this.publicService.registerUser(userRegisterDto);
	}

	@Override
	@ResponseBody
	@PostMapping(value = "/login")
	public LoginDto loginUser(@RequestBody AuthenticationRequest authenticationRequest) {
		return this.publicService.loginUser(authenticationRequest);
	}

	@Override
	@ResponseBody
	@PostMapping(value = "/register-company")
	public Long registerCompany(@Valid @RequestBody CompanyRegisterDto companyRegisterDto) {
		return this.publicService.registerCompany(companyRegisterDto);
	}

}