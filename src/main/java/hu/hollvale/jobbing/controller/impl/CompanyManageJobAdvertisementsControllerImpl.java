package hu.hollvale.jobbing.controller.impl;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.hollvale.jobbing.controller.CompanyManageJobAdvertisementsController;
import hu.hollvale.jobbing.dto.match.CompanyManageJobAdvertisementsDto;
import hu.hollvale.jobbing.service.CompanyManageJobAdvertisementsService;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/company-manage-job-advertisements")
public class CompanyManageJobAdvertisementsControllerImpl implements CompanyManageJobAdvertisementsController {

	private final CompanyManageJobAdvertisementsService companyManageJobAdvertisementsService;

	@ResponseBody
	@Override
	@PostMapping("/page")
	public List<CompanyManageJobAdvertisementsDto> getJobAdvertisementsWithApplicants(
			@RequestParam("companyId") Long companyId, Pageable pageable) {
		return this.companyManageJobAdvertisementsService.getJobAdvertisementWithAppliciants(companyId, pageable)
				.toList();
	}

}
