package hu.hollvale.jobbing.controller.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import hu.hollvale.jobbing.controller.CompanyProfileController;
import hu.hollvale.jobbing.dto.company.CompanyIntroductionUpdateDto;
import hu.hollvale.jobbing.dto.company.CompanyJobAdvertisementListDto;
import hu.hollvale.jobbing.dto.company.CompanyJobAdvertisementSummariesDto;
import hu.hollvale.jobbing.dto.company.CompanyPersonalDataUpdateDto;
import hu.hollvale.jobbing.dto.company.CompanyProfileDataListDto;
import hu.hollvale.jobbing.service.CompanyHelperService;
import hu.hollvale.jobbing.service.CompanyProfileService;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/company-profile")
public class CompanyProfileControllerImpl implements CompanyProfileController {

	private final CompanyProfileService companyProfileService;
	private final CompanyHelperService companyHelperService;

	@Override
	@ResponseBody
	@GetMapping
	public CompanyProfileDataListDto loadProfileData(@RequestParam("companyId") Long companyId) {
		return this.companyHelperService.loadProfileData(companyId);
	}

	@Override
	@PostMapping("/update-introduction")
	public void updateIntroduction(@RequestBody CompanyIntroductionUpdateDto companyIntroductionUpdateDto) {
		this.companyProfileService.updateIntroduction(companyIntroductionUpdateDto);

	}

	@Override
	@PostMapping("/update-personal-data")
	public Long updatePersonalData(@RequestBody CompanyPersonalDataUpdateDto companyPersonalDataUpdateDto) {
		return this.companyProfileService.updatePersonalData(companyPersonalDataUpdateDto);

	}

	@Override
	@PostMapping("/save-logo")
	@ResponseBody
	public String saveLogo(@RequestParam("companyId") Long companyId, @RequestParam("file") MultipartFile file)
			throws IOException {
		return this.companyProfileService.saveLogo(companyId, file);

	}

	@Override
	@DeleteMapping("/delete-logo")
	public void deleteLogo(@RequestParam("companyId") Long companyId) throws IOException {
		this.companyProfileService.deleteLogo(companyId);

	}

	@Override
	@GetMapping("/load-jobAdvertisements")
	public List<CompanyJobAdvertisementListDto> loadCompanyJobAdvertisements(
			@RequestParam("companyId") Long companyId) {
		return this.companyHelperService.loadCompanyJobAdvertisements(companyId);
	}

	@Override
	@GetMapping("/load-jobAdvertisement-summaries")
	public List<CompanyJobAdvertisementSummariesDto> loadCompanyJobAdvertisementSummaries(
			@RequestParam("companyId") Long companyId) {
		return this.companyHelperService.loadCompanyJobAdvertisementSummaries(companyId);
	}

	@Override
	@DeleteMapping("delete-company")
	public void deleteCompany(@RequestParam("companyId") Long companyId) {
		this.companyProfileService.deleteCompany(companyId);

	}

}
