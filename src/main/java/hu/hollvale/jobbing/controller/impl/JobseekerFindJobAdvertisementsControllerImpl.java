package hu.hollvale.jobbing.controller.impl;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.hollvale.jobbing.controller.JobseekerFindJobAdvertisementsController;
import hu.hollvale.jobbing.dto.company.JobAdvertisementFindDto;
import hu.hollvale.jobbing.dto.filter.JobAdvertisementsFilter;
import hu.hollvale.jobbing.service.JobseekerFindJobAdvertisementsService;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/Jobseeker-find-jobAdvertisements")
public class JobseekerFindJobAdvertisementsControllerImpl implements JobseekerFindJobAdvertisementsController {

	private final JobseekerFindJobAdvertisementsService jobseekerFindJobAdvertisementsService;

	@ResponseBody
	@PostMapping("/page")
	@Override
	public Optional<JobAdvertisementFindDto> pageJobAdvertisements(
			@RequestBody JobAdvertisementsFilter jobAdvertisementsFilter, Pageable pageable) {

		Page<JobAdvertisementFindDto> jobAdvertisementFindDtoPage = this.jobseekerFindJobAdvertisementsService
				.pageJobAdvertisements(jobAdvertisementsFilter, pageable);
		return Optional.of(jobAdvertisementFindDtoPage.toList().get(0));
	}

}
