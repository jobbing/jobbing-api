package hu.hollvale.jobbing.controller.impl;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.hollvale.jobbing.controller.JobseekerMyJobAdvertisementsController;
import hu.hollvale.jobbing.dto.JobAdvertisementListDto;
import hu.hollvale.jobbing.dto.filter.AdvertisementStateFilter;
import hu.hollvale.jobbing.dto.match.InterestedCompanyListDTO;
import hu.hollvale.jobbing.service.JobseekerMyJobAdvertisementsService;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/my-job-advertisements")
public class JobseekerMyJobAdvertisementsControllerImpl implements JobseekerMyJobAdvertisementsController {

	private final JobseekerMyJobAdvertisementsService jobseekerMyJobAdvertisementsService;

	@Override
	@ResponseBody
	@PostMapping("/list")
	public List<JobAdvertisementListDto> listJobAdvertisements(
			@RequestBody AdvertisementStateFilter advertisementStateFilter) {
		return this.jobseekerMyJobAdvertisementsService.listJobAdvertisements(advertisementStateFilter);
	}

	@Override
	@ResponseBody
	@GetMapping("/interested-companies")
	public List<InterestedCompanyListDTO> interestedCompanies(@RequestParam("jobseekerId") Long jobseekerId) {
		return this.jobseekerMyJobAdvertisementsService.interestedCompanies(jobseekerId);
	}

}
