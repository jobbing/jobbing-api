package hu.hollvale.jobbing.controller.impl;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.hollvale.jobbing.controller.JobAdvertisementController;
import hu.hollvale.jobbing.dto.GetJobAdvertisementDto;
import hu.hollvale.jobbing.dto.JobseekerJobAdvertisementDto;
import hu.hollvale.jobbing.dto.company.GetListValuesDto;
import hu.hollvale.jobbing.dto.company.JobAdvertisementDto;
import hu.hollvale.jobbing.service.JobAdvertisementHelperService;
import hu.hollvale.jobbing.service.JobAdvertisementService;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/job-advertisement")
public class JobAdvertisementControllerImpl implements JobAdvertisementController {

	private final JobAdvertisementService jobAdvertisementService;
	private final JobAdvertisementHelperService jobAdvertisementHelperService;

	@Override
	@ResponseBody
	@PostMapping("/save")
	public Long saveJobAdvertisement(@RequestBody JobAdvertisementDto jobAdvertisementDto) {
		return this.jobAdvertisementService.saveJobAdvertisement(jobAdvertisementDto);
	}

	@Override
	@ResponseBody
	@GetMapping("/company-get")
	public GetJobAdvertisementDto getJobAdvertisement(@RequestParam("jobAdvertisementId") Long jobAdvertisementId) {
		return this.jobAdvertisementHelperService.getJobAdvertisement(jobAdvertisementId);
	}

	@Override
	@ResponseBody
	@GetMapping("/jobseeker-get")
	public JobseekerJobAdvertisementDto getJobseekerJobAdvertisement(
			@RequestParam("jobAdvertisementId") Long jobAdvertisementId) {
		return this.jobAdvertisementHelperService.getJobseekerJobAdvertisement(jobAdvertisementId);
	}

	@Override
	@DeleteMapping("/delete")
	public void deleteJobAdvertisement(@RequestParam("jobAdvertisementId") Long jobAdvertisementId) {
		this.jobAdvertisementService.deleteJobAdvertisement(jobAdvertisementId);

	}

	@Override
	@ResponseBody
	@GetMapping("/get-list-values")
	public GetListValuesDto getListValues() {
		return this.jobAdvertisementHelperService.getListValues();
	}
}
