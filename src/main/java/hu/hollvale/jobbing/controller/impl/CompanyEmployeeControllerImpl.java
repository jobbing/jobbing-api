package hu.hollvale.jobbing.controller.impl;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.hollvale.jobbing.controller.CompanyEmployeeController;
import hu.hollvale.jobbing.dto.company.EmployeeDataDto;
import hu.hollvale.jobbing.dto.company.EmployeeRegisterDto;
import hu.hollvale.jobbing.service.CompanyEmployeeService;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/company-empoyee")
public class CompanyEmployeeControllerImpl implements CompanyEmployeeController {

	private final CompanyEmployeeService companyEmployeeService;

	@Override
	@ResponseBody
	@GetMapping
	public List<EmployeeDataDto> getCompanyEmployees(@RequestParam("companyId") Long companyId) {
		return this.companyEmployeeService.getCompanyEmployees(companyId);
	}

	@Override
	@ResponseBody
	@PostMapping(value = "/register-employee")
	public Long registerEmployee(@RequestBody EmployeeRegisterDto employeeRegisterDto) {
		return this.companyEmployeeService.registerEmployee(employeeRegisterDto);
	}

}
