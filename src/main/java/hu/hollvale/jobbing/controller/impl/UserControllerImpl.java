package hu.hollvale.jobbing.controller.impl;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hu.hollvale.jobbing.controller.UserController;
import hu.hollvale.jobbing.dto.user.EmailChangeDto;
import hu.hollvale.jobbing.dto.user.GetMyDatasDto;
import hu.hollvale.jobbing.dto.user.JobseekerSettingsDto;
import hu.hollvale.jobbing.dto.user.NameChangeDto;
import hu.hollvale.jobbing.dto.user.PasswordChangeDto;
import hu.hollvale.jobbing.dto.user.UsernameChangeDto;
import hu.hollvale.jobbing.enums.HolderType;
import hu.hollvale.jobbing.service.UserService;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/user")
public class UserControllerImpl implements UserController {

	private final UserService userService;

	@Override
	@DeleteMapping("/delete-user")
	public void deleteUser(@RequestParam String username) {
		this.userService.deleteUser(username);
	}

	@Override
	@PostMapping("/change-username")
	public void changeUsername(@RequestBody UsernameChangeDto usernameChangeDto) {
		this.userService.changeUsername(usernameChangeDto);

	}

	@Override
	@PostMapping("/change-name")
	public void changeName(@RequestBody NameChangeDto nameChangeDto) {
		this.userService.changeName(nameChangeDto);

	}

	@Override
	@PostMapping("/change-password")
	public void changePassword(@RequestBody PasswordChangeDto passwordChangeDto) {
		this.userService.changePassword(passwordChangeDto);

	}

	@Override
	@PostMapping("/change-email")
	public void changeEmail(@RequestBody EmailChangeDto emailChangeDto) {
		this.userService.changeEmail(emailChangeDto);

	}

	@Override
	@GetMapping("/setting-datas")
	public JobseekerSettingsDto getUserDatasToSettings(@RequestParam String username) {
		return this.userService.getUserDatasToSettings(username);
	}

	@Override
	@GetMapping("/get-my-datas")
	public GetMyDatasDto getMyDatas(@RequestParam HolderType holderType) {
		return this.userService.getMyDatas(holderType);
	}

}
