package hu.hollvale.jobbing.controller.impl;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.hollvale.jobbing.controller.MatchController;
import hu.hollvale.jobbing.dto.match.CompanyMatchDto;
import hu.hollvale.jobbing.service.MatchService;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/match")
public class MatchControllerImpl implements MatchController {

	private final MatchService matchService;

	@Override
	@ResponseBody
	@PostMapping("/jobseekeer-accept-company")
	public Long jobseekeerAcceptCompany(@RequestBody CompanyMatchDto companyMatchDto) {
		return this.matchService.jobseekeerAcceptCompany(companyMatchDto);

	}

	@Override
	@ResponseBody
	@PostMapping("/jobseekeer-reject-company")
	public Long jobseekeerRejectCompany(@RequestBody CompanyMatchDto companyMatchDto) {
		return this.matchService.jobseekeerRejectCompany(companyMatchDto);

	}

	@Override
	@PostMapping("/company-accept-jobseeker")
	public void companyAcceptJobseekeer(@RequestBody CompanyMatchDto companyMatchDto) {
		this.matchService.companyAcceptJobseekeer(companyMatchDto);

	}

	@Override
	@PostMapping("/company-reject-jobseeker")
	public void companyRejectJobseekeer(@RequestBody CompanyMatchDto companyMatchDto) {
		this.matchService.companyRejectJobseekeer(companyMatchDto);

	}
}
