package hu.hollvale.jobbing.controller.impl;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hu.hollvale.jobbing.controller.MatchByAdvertisementController;
import hu.hollvale.jobbing.dto.match.JobseekerMatchDto;
import hu.hollvale.jobbing.service.MatchByAdvertisementService;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/match-by-advertisement")
public class MatchByAdvertisementControllerImpl implements MatchByAdvertisementController {

	private final MatchByAdvertisementService matchByAdvertisementService;

	@Override
	@PostMapping("/jobseeker-reject-job-advertisement")
	public void jobseekerRejectJobAdvertisement(@RequestBody JobseekerMatchDto jobseekerMatchDto) {
		this.matchByAdvertisementService.jobseekerRejectJobAdvertisement(jobseekerMatchDto);

	}

	@Override
	@PostMapping("/jobseeker-accept-job-advertisement")
	public void jobseekerAcceptJobAdvertisement(@RequestBody JobseekerMatchDto jobseekerMatchDto) {
		this.matchByAdvertisementService.jobseekerAcceptJobAdvertisement(jobseekerMatchDto);

	}

	@Override
	@PostMapping("/company-reject-jobseeker-by-advertisement")
	public void companyRejectJobseekerByAdvertisement(@RequestBody JobseekerMatchDto jobseekerMatchDto) {
		this.matchByAdvertisementService.companyRejectJobseekerByAdvertisement(jobseekerMatchDto);

	}

	@Override
	@PostMapping("/company-accept-jobseeker-by-advertisement")
	public void companyAcceptJobseekerByAdvertisement(@RequestBody JobseekerMatchDto jobseekerMatchDto) {
		this.matchByAdvertisementService.companyAcceptJobseekerByAdvertisement(jobseekerMatchDto);

	}

}
