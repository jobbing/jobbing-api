package hu.hollvale.jobbing.controller.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.hollvale.jobbing.controller.CompanyFindJobseekerController;
import hu.hollvale.jobbing.dto.company.JobseekerDto;
import hu.hollvale.jobbing.dto.filter.JobseekerFilter;
import hu.hollvale.jobbing.service.CompanyFindJobseekerService;
import hu.hollvale.jobbing.service.CompanyHelperService;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/company-find-jobseekers")
public class CompanyFindJobseekerControllerImpl implements CompanyFindJobseekerController {

	@Autowired
	private final CompanyFindJobseekerService companyFindJobseekerService;

	@Autowired
	private final CompanyHelperService companyHelperService;

	@ResponseBody
	@PostMapping("/page")
	@Override
	public Optional<JobseekerDto> pageJobseekers(@RequestBody JobseekerFilter jobseekerFilter, Pageable pageable) {

		Page<JobseekerDto> jobseekerDtoPage = this.companyFindJobseekerService.pageJobseekers(jobseekerFilter,
				pageable);
		return Optional.of(jobseekerDtoPage.toList().get(0));
	}

	@ResponseBody
	@GetMapping("/get-matched-jobseekers")
	@Override
	public List<JobseekerDto> getMatchedJobseekers(@RequestParam Long companyId) {

		return this.companyHelperService.getMatchedJobseekers(companyId);
	}

}
