package hu.hollvale.jobbing.controller.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import hu.hollvale.jobbing.controller.JobseekerProfileController;
import hu.hollvale.jobbing.dto.DeleteDrivingLicenceDto;
import hu.hollvale.jobbing.dto.DeleteLanguageExamDto;
import hu.hollvale.jobbing.dto.DrivingLicenceDto;
import hu.hollvale.jobbing.dto.DrivingLicenceListDto;
import hu.hollvale.jobbing.dto.ExperienceDto;
import hu.hollvale.jobbing.dto.ExperienceListDto;
import hu.hollvale.jobbing.dto.IntroductionUpdateDto;
import hu.hollvale.jobbing.dto.LanguageExamDto;
import hu.hollvale.jobbing.dto.LanguageExamListDto;
import hu.hollvale.jobbing.dto.PersonalDataUpdateDto;
import hu.hollvale.jobbing.dto.ProfileDataListDto;
import hu.hollvale.jobbing.dto.QualityDto;
import hu.hollvale.jobbing.dto.QualityListDto;
import hu.hollvale.jobbing.dto.WorkILookForUpdateDto;
import hu.hollvale.jobbing.service.JobseekerHelperService;
import hu.hollvale.jobbing.service.JobseekerProfileService;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/jobseeker-profile")
public class JobseekerProfileControllerImpl implements JobseekerProfileController {

	private final JobseekerProfileService jobseekerProfileService;
	private final JobseekerHelperService jobseekerHelperService;

	@Override
	@ResponseBody
	@GetMapping
	public ProfileDataListDto loadProfileData() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return this.jobseekerHelperService.loadProfileData(username);
	}

	@Override
	@ResponseBody
	@GetMapping("/company-load")
	public ProfileDataListDto companyLoadJobseekerProfileData(@RequestParam("jobseekerId") Long jobseekerId) {
		return this.jobseekerHelperService.companyLoadJobseekerProfileData(jobseekerId);
	}

	@Override
	@PostMapping("/save-picture")
	public String savePicture(@RequestParam("file") MultipartFile file) throws IOException {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return this.jobseekerProfileService.savePicture(username, file);
	}

	@Override
	@PostMapping("/save-cv")
	public String saveCV(String username, MultipartFile file) throws IOException {
		return this.jobseekerProfileService.saveCV(username, file);
	}

	@Override
	@DeleteMapping(value = "/delete-cv")
	public void deleteCV(@RequestParam("jobseekerId") Long jobseekerId) throws IOException {
		this.jobseekerProfileService.deleteCV(jobseekerId);
	}

	@Override
	@ResponseBody
	@PostMapping("/update-introduction")
	public void updateIntroduction(@RequestBody IntroductionUpdateDto introductionUpdateDto) {
		this.jobseekerProfileService.updateIntroduction(introductionUpdateDto);

	}

	@Override
	@ResponseBody
	@PostMapping("/update-work-i-look-for")
	public void updateWorkILookFor(@RequestBody WorkILookForUpdateDto workILookForUpdateDto) {
		this.jobseekerProfileService.updateWorkILookFor(workILookForUpdateDto);
	}

	@Override
	@ResponseBody
	@PostMapping("/update-personal-data")
	public void updatePersonalDatas(@RequestBody PersonalDataUpdateDto personalDataUpdateDto) {
		this.jobseekerProfileService.updatePersonalDatas(personalDataUpdateDto);

	}

	@Override
	@ResponseBody
	@GetMapping(value = "/get-driving-licences")
	public List<DrivingLicenceListDto> getDrivingLicences(@RequestParam("id") Long jobseekerId) {
		return this.jobseekerHelperService.getDrivingLicences(jobseekerId);
	}

	@Override
	@ResponseBody
	@GetMapping(value = "/get-language-exams")
	public List<LanguageExamListDto> getLanguageExams(@RequestParam("id") Long jobseekerId) {
		return this.jobseekerHelperService.getLanguageExams(jobseekerId);
	}

	@Override
	@ResponseBody
	@GetMapping(value = "/get-qualities")
	public List<QualityListDto> getQualities(@RequestParam("id") Long jobseekerId) {
		return this.jobseekerHelperService.getQualities(jobseekerId);
	}

	@Override
	@ResponseBody
	@GetMapping(value = "/get-experiences")
	public List<ExperienceListDto> getExperiences(@RequestParam("id") Long jobseekerId) {
		return this.jobseekerHelperService.getExperiences(jobseekerId);
	}

	@Override
	@DeleteMapping(value = "delete-driving-licence")
	public void deleteDrivingLicence(@RequestBody DeleteDrivingLicenceDto deleteDrivingLicenceDto) {
		this.jobseekerProfileService.deleteDrivingLicence(deleteDrivingLicenceDto);

	}

	@Override
	@DeleteMapping(value = "delete-language-exam")
	public Long deleteLanguageExam(@RequestParam("jobseekerId") Long jobseekerId,
			@RequestParam("languageExamId") Long languageExamId) {
		DeleteLanguageExamDto deleteLanguageExamDto = new DeleteLanguageExamDto();
		deleteLanguageExamDto.setJobseekerId(jobseekerId);
		deleteLanguageExamDto.setLanguageExamId(languageExamId);

		return this.jobseekerProfileService.deleteLanguageExam(deleteLanguageExamDto);

	}

	@Override
	@DeleteMapping(value = "delete-quality")
	public Long deleteQuality(@RequestParam("id") Long qualityId) {
		return this.jobseekerProfileService.deleteQualtity(qualityId);

	}

	@Override
	@DeleteMapping(value = "delete-experience")
	public Long deleteExperience(@RequestParam("id") Long experienceId) {
		return this.jobseekerProfileService.deleteExperience(experienceId);

	}

	@Override
	@DeleteMapping("/delete-picture")
	public void deletePicture(@RequestParam("id") Long jobseekerId) throws IOException {

		this.jobseekerProfileService.deletePicture(jobseekerId);
	}

	@Override
	@PostMapping("/save-driving-licence")
	public Long saveDrivingLicences(@RequestBody DrivingLicenceDto drivingLicenceDto) {
		return this.jobseekerProfileService.saveDrivingLicences(drivingLicenceDto);
	}

	@Override
	@PostMapping("/save-language-exam")
	public Long saveLanguageExam(@RequestBody LanguageExamDto languageExamDto) {
		return this.jobseekerProfileService.saveLanguageExam(languageExamDto);

	}

	@Override
	@PostMapping("/save-quality")
	public Long saveQuality(@RequestBody QualityDto qualityDto) {
		return this.jobseekerProfileService.saveQuality(qualityDto);
	}

	@Override
	@PostMapping("/save-experience")
	public Long saveExperience(@RequestBody ExperienceDto experienceDto) {
		return this.jobseekerProfileService.saveExperience(experienceDto);
	}

}
