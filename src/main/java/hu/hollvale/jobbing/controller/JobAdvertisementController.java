package hu.hollvale.jobbing.controller;

import hu.hollvale.jobbing.dto.GetJobAdvertisementDto;
import hu.hollvale.jobbing.dto.JobseekerJobAdvertisementDto;
import hu.hollvale.jobbing.dto.company.GetListValuesDto;
import hu.hollvale.jobbing.dto.company.JobAdvertisementDto;

public interface JobAdvertisementController {

	public Long saveJobAdvertisement(JobAdvertisementDto jobAdvertisementDto);

	public GetJobAdvertisementDto getJobAdvertisement(Long jobAdvertisementId);

	public JobseekerJobAdvertisementDto getJobseekerJobAdvertisement(Long jobAdvertisementId);

	public void deleteJobAdvertisement(Long jobAdvertisementId);

	public GetListValuesDto getListValues();
}
