package hu.hollvale.jobbing.controller;

import java.util.List;

import hu.hollvale.jobbing.dto.company.EmployeeDataDto;
import hu.hollvale.jobbing.dto.company.EmployeeRegisterDto;

public interface CompanyEmployeeController {

	public List<EmployeeDataDto> getCompanyEmployees(Long companyId);

	public Long registerEmployee(EmployeeRegisterDto employeeRegisterDto);

}
