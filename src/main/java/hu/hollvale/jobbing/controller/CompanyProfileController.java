package hu.hollvale.jobbing.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import hu.hollvale.jobbing.dto.company.CompanyIntroductionUpdateDto;
import hu.hollvale.jobbing.dto.company.CompanyJobAdvertisementListDto;
import hu.hollvale.jobbing.dto.company.CompanyJobAdvertisementSummariesDto;
import hu.hollvale.jobbing.dto.company.CompanyPersonalDataUpdateDto;
import hu.hollvale.jobbing.dto.company.CompanyProfileDataListDto;

public interface CompanyProfileController {

	public CompanyProfileDataListDto loadProfileData(Long companyId);

	public void updateIntroduction(CompanyIntroductionUpdateDto companyIntroductionUpdateDto);

	public Long updatePersonalData(CompanyPersonalDataUpdateDto companyPersonalDataUpdateDto);

	public String saveLogo(Long companyId, MultipartFile file) throws IOException;

	public void deleteLogo(Long companyId) throws IOException;

	public List<CompanyJobAdvertisementListDto> loadCompanyJobAdvertisements(Long companyId);

	public List<CompanyJobAdvertisementSummariesDto> loadCompanyJobAdvertisementSummaries(Long companyId);

	void deleteCompany(Long companyId);

}
