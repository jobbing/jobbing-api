package hu.hollvale.jobbing.controller;

import java.util.List;

import org.springframework.data.domain.Pageable;

import hu.hollvale.jobbing.dto.match.CompanyManageJobAdvertisementsDto;

public interface CompanyManageJobAdvertisementsController {

	public List<CompanyManageJobAdvertisementsDto> getJobAdvertisementsWithApplicants(Long companyId,
			Pageable pageable);

}
