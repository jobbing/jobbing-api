package hu.hollvale.jobbing.controller;

import java.util.List;

import hu.hollvale.jobbing.dto.JobAdvertisementListDto;
import hu.hollvale.jobbing.dto.filter.AdvertisementStateFilter;
import hu.hollvale.jobbing.dto.match.InterestedCompanyListDTO;

public interface JobseekerMyJobAdvertisementsController {

	List<JobAdvertisementListDto> listJobAdvertisements(AdvertisementStateFilter advertisementStateFilter);

	List<InterestedCompanyListDTO> interestedCompanies(Long jobseekerId);

}
