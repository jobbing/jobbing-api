package hu.hollvale.jobbing.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import hu.hollvale.jobbing.dto.DeleteDrivingLicenceDto;
import hu.hollvale.jobbing.dto.DrivingLicenceDto;
import hu.hollvale.jobbing.dto.DrivingLicenceListDto;
import hu.hollvale.jobbing.dto.ExperienceDto;
import hu.hollvale.jobbing.dto.ExperienceListDto;
import hu.hollvale.jobbing.dto.IntroductionUpdateDto;
import hu.hollvale.jobbing.dto.LanguageExamDto;
import hu.hollvale.jobbing.dto.LanguageExamListDto;
import hu.hollvale.jobbing.dto.PersonalDataUpdateDto;
import hu.hollvale.jobbing.dto.ProfileDataListDto;
import hu.hollvale.jobbing.dto.QualityDto;
import hu.hollvale.jobbing.dto.QualityListDto;
import hu.hollvale.jobbing.dto.WorkILookForUpdateDto;

public interface JobseekerProfileController {

	public ProfileDataListDto loadProfileData();

	public ProfileDataListDto companyLoadJobseekerProfileData(Long jobseekerId);

	public void updateIntroduction(IntroductionUpdateDto introductionUpdateDto);

	public void updateWorkILookFor(WorkILookForUpdateDto workILookForUpdateDto);

	public void updatePersonalDatas(PersonalDataUpdateDto personalDataUpdateDto);

	public List<DrivingLicenceListDto> getDrivingLicences(Long jobseekerId);

	public List<LanguageExamListDto> getLanguageExams(Long jobseekerId);

	public List<QualityListDto> getQualities(Long jobseekerId);

	public List<ExperienceListDto> getExperiences(Long jobseekerId);

	void deleteDrivingLicence(DeleteDrivingLicenceDto deleteDrivingLicenceDto);

	public Long deleteLanguageExam(Long jobseekerId, Long languageExamId);

	public Long deleteQuality(Long qualityId);

	public Long deleteExperience(Long experienceId);

	public Long saveDrivingLicences(DrivingLicenceDto drivingLicenceDto);

	public Long saveLanguageExam(LanguageExamDto languageExamDto);

	public Long saveQuality(QualityDto qualityDto);

	public Long saveExperience(ExperienceDto experienceDto);

	public String savePicture(MultipartFile file) throws IOException;

	public void deletePicture(Long jobseekerId) throws IOException;

	public String saveCV(String username, MultipartFile file) throws IOException;

	public void deleteCV(Long jobseekerId) throws IOException;
}
