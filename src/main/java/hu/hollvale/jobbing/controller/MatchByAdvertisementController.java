package hu.hollvale.jobbing.controller;

import hu.hollvale.jobbing.dto.match.CompanyMatchDto;
import hu.hollvale.jobbing.dto.match.JobseekerMatchDto;

public interface MatchByAdvertisementController {

	public void jobseekerRejectJobAdvertisement(JobseekerMatchDto jobseekerMatchDto);

	public void jobseekerAcceptJobAdvertisement(JobseekerMatchDto jobseekerMatchDto);

	public void companyRejectJobseekerByAdvertisement(JobseekerMatchDto jobseekerMatchDto);

	public void companyAcceptJobseekerByAdvertisement(JobseekerMatchDto jobseekerMatchDto);
}
