package hu.hollvale.jobbing.controller;

import hu.hollvale.jobbing.dto.match.CompanyMatchDto;

public interface MatchController {

	public Long jobseekeerAcceptCompany(CompanyMatchDto companyMatchDto);

	public Long jobseekeerRejectCompany(CompanyMatchDto companyMatchDto);

	public void companyAcceptJobseekeer(CompanyMatchDto companyMatchDto);

	public void companyRejectJobseekeer(CompanyMatchDto companyMatchDto);

}
