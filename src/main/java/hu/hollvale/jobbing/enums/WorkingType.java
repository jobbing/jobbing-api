package hu.hollvale.jobbing.enums;

public enum WorkingType {
	//// @formatter:off

	PART_TIME_JOB,
	STUDENT_JOB,
	HOME_JOB,
	FULL_TIME_JOB,
	PRACTICAL_JOB,  //szakmai gyakorlat
	ENTRIVERAL_JOB  //pályakezdő
	 
	// @formatter:on

}
