package hu.hollvale.jobbing.enums;

public enum MatchStateType {

	//// @formatter:off
	MATCH,
	JOBSEEKER_ACCEPT,
	COMPANY_ACCEPT,
	JOBSEEKER_REJECT,
	COMPANY_REJECT
	 
	// @formatter:on

}
