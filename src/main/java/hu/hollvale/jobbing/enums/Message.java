package hu.hollvale.jobbing.enums;

public enum Message {

	//// @formatter:off

	USER_EXIST("user.exist"),
	JOBSEEKER_NOT_FOUND("jobseeker.not.found"),
	USER_NOT_FOUND("user.not.found"),
	DRIVING_LICENCE_NOT_FOUND("driving.licence.not.found"),
	LANGUAGE_EXAM_NOT_FOUND("language.exam.not.found"),
	QUALITY_NOT_FOUND("quality.not.found"),
	EXPERIENCE_NOT_FOUND("experience.not.found"),
	PAIR_NOT_FOUND("pair.not.found"),
	USER_ALREADY_HAS_JOBSEEKER("user.already.has.jobseeker"),
	USER_EMAIL_EXIST("user.email.exist"),
	COMPANY_EMAIL_EXIST("company.email.exist"),
	FILE_NULL("file.null"),
	PATH_NOT_FOUND("path.not.found"),
	NOT_DELETABLE("not.deletable"),
	IMAGE_REQUIRED("image.required"),
	PDF_REQUIRED("pdf.required"), 
	COMPANY_NOT_FOUND("company.not.found"), 
	JOBADVERTISEMENT_NOT_FOUND("jobadvertisement.not.found"),
	CITY_NOT_FOUND("city.not.found"),
	WORKCATEGORY_NOT_FOUND("workcategory.not.found"),
	WORKTYPE_NOT_FOUND("worktype.not.found"), 
	JOBSEEKER_ALREADY_LIKES_JOBADVERTISEMENT("jobseeker.already.likes.jobadvertisement"),
	ALREADY_MATCHED("already.matched"),
	COMPANY_ALREADY_LIKES_JOBSEEKER("company.already.likes.jobseeker"),
	MATCH_NOT_FOUND("match.not.found"),
	ALREADY_DECIDED("already.decided"),
	PASSWORDS_NOT_MATCH("passwords.not.match"),
	EXPERIENCE_ALREADY_EXIST("experience.already.exist"),
	BAD_DATES("bad.dates"), 
	JOBSEEKER_ALREADY_HAS_THIS_LANGUAGEEXAM("jobseeker.already.has.languageexam"),
	ROLE_NOT_FOUND("role.not.found"),
	MUST_HAVE_ROLE("must.have.role"),
	ROLE_NOT_ALLOWED("role.not.allowed"),
	INVALID_INPUTS("invalid.inputs"), 
	NO_PERMISSION("no.permission"),
	QUALITY_ALREADY_EXIST("quality.already.exist"),
	INVALID_EMAIL_FORMAT("invalid.email.format"),
	INVALID_PASSWORD_FORMAT("invalid.password.format"), 
	INPUT_IS_NULL("input.is.null"), 
	COMPANY_EMPLOYEE_NOT_FOUND("company.employee.not.found"),
	INVALID_USERNAME_INPUT("invalid.username.input"),
	INVALID_COMPANY_INPUT("invalid.company.input"),
	INVALID_JOBSEEKER_INPUT("invalid.jobseeker.input"),
	INVALID_QUALITY_INPUT("invalid.quality.input"),
	INVALID_EXPERIENCE_INPUT("invalid.experience.input"),
	COMPANY_NOT_MATCH("company.mot.match"),
	COMPANY_NOT_DELETABLE("company.not.deletable");
	
// @formatter:on

	private String value;

	Message(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return this.value;
	}
}
