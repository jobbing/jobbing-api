package hu.hollvale.jobbing.enums;

public enum LevelType {

	//// @formatter:off

	A1,
	A2,
	B1,
	B2,
	C1,
	C2
	 
	// @formatter:on

}
