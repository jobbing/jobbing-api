package hu.hollvale.jobbing.enums;

public enum QualityType {

	//// @formatter:off
	
	EIGHT_CLASS,
	GRADUATION,  //érettségi
	PROFESSION,  //szakma
	TECHNICUM,
	CERTIFICATE  //diploma
	 
	// @formatter:on

}
