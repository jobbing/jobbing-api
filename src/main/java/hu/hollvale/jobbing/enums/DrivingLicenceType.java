package hu.hollvale.jobbing.enums;

public enum DrivingLicenceType {

	A, A1, A2, AM, B, C, D, E, K, T, TR, V

}
