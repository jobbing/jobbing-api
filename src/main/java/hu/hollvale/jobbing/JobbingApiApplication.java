package hu.hollvale.jobbing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobbingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobbingApiApplication.class, args);
	}

}
