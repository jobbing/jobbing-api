/*
 * This file is generated by jOOQ.
 */
package hu.hollvale.jobbing.jooq.tables;


import hu.hollvale.jobbing.jooq.DefaultSchema;
import hu.hollvale.jobbing.jooq.Keys;
import hu.hollvale.jobbing.jooq.tables.records.JobseekerXDrivingLicenceAudRecord;

import java.util.Arrays;
import java.util.List;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row4;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JobseekerXDrivingLicenceAud extends TableImpl<JobseekerXDrivingLicenceAudRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>JOBSEEKER_X_DRIVING_LICENCE_AUD</code>
     */
    public static final JobseekerXDrivingLicenceAud JOBSEEKER_X_DRIVING_LICENCE_AUD = new JobseekerXDrivingLicenceAud();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<JobseekerXDrivingLicenceAudRecord> getRecordType() {
        return JobseekerXDrivingLicenceAudRecord.class;
    }

    /**
     * The column <code>JOBSEEKER_X_DRIVING_LICENCE_AUD.REV</code>.
     */
    public final TableField<JobseekerXDrivingLicenceAudRecord, Integer> REV = createField(DSL.name("REV"), SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>JOBSEEKER_X_DRIVING_LICENCE_AUD.JOBSEEKER_ID</code>.
     */
    public final TableField<JobseekerXDrivingLicenceAudRecord, Long> JOBSEEKER_ID = createField(DSL.name("JOBSEEKER_ID"), SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>JOBSEEKER_X_DRIVING_LICENCE_AUD.DRIVING_LICENCE_ID</code>.
     */
    public final TableField<JobseekerXDrivingLicenceAudRecord, Long> DRIVING_LICENCE_ID = createField(DSL.name("DRIVING_LICENCE_ID"), SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>JOBSEEKER_X_DRIVING_LICENCE_AUD.REVTYPE</code>.
     */
    public final TableField<JobseekerXDrivingLicenceAudRecord, Byte> REVTYPE = createField(DSL.name("REVTYPE"), SQLDataType.TINYINT, this, "");

    private JobseekerXDrivingLicenceAud(Name alias, Table<JobseekerXDrivingLicenceAudRecord> aliased) {
        this(alias, aliased, null);
    }

    private JobseekerXDrivingLicenceAud(Name alias, Table<JobseekerXDrivingLicenceAudRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>JOBSEEKER_X_DRIVING_LICENCE_AUD</code> table reference
     */
    public JobseekerXDrivingLicenceAud(String alias) {
        this(DSL.name(alias), JOBSEEKER_X_DRIVING_LICENCE_AUD);
    }

    /**
     * Create an aliased <code>JOBSEEKER_X_DRIVING_LICENCE_AUD</code> table reference
     */
    public JobseekerXDrivingLicenceAud(Name alias) {
        this(alias, JOBSEEKER_X_DRIVING_LICENCE_AUD);
    }

    /**
     * Create a <code>JOBSEEKER_X_DRIVING_LICENCE_AUD</code> table reference
     */
    public JobseekerXDrivingLicenceAud() {
        this(DSL.name("JOBSEEKER_X_DRIVING_LICENCE_AUD"), null);
    }

    public <O extends Record> JobseekerXDrivingLicenceAud(Table<O> child, ForeignKey<O, JobseekerXDrivingLicenceAudRecord> key) {
        super(child, key, JOBSEEKER_X_DRIVING_LICENCE_AUD);
    }

    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    @Override
    public UniqueKey<JobseekerXDrivingLicenceAudRecord> getPrimaryKey() {
        return Keys.CONSTRAINT_9;
    }

    @Override
    public List<UniqueKey<JobseekerXDrivingLicenceAudRecord>> getKeys() {
        return Arrays.<UniqueKey<JobseekerXDrivingLicenceAudRecord>>asList(Keys.CONSTRAINT_9);
    }

    @Override
    public List<ForeignKey<JobseekerXDrivingLicenceAudRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<JobseekerXDrivingLicenceAudRecord, ?>>asList(Keys.FK128KP89AL9VCM6F2N6AB4GVRW);
    }

    private transient Revinfo _revinfo;

    public Revinfo revinfo() {
        if (_revinfo == null)
            _revinfo = new Revinfo(this, Keys.FK128KP89AL9VCM6F2N6AB4GVRW);

        return _revinfo;
    }

    @Override
    public JobseekerXDrivingLicenceAud as(String alias) {
        return new JobseekerXDrivingLicenceAud(DSL.name(alias), this);
    }

    @Override
    public JobseekerXDrivingLicenceAud as(Name alias) {
        return new JobseekerXDrivingLicenceAud(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public JobseekerXDrivingLicenceAud rename(String name) {
        return new JobseekerXDrivingLicenceAud(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public JobseekerXDrivingLicenceAud rename(Name name) {
        return new JobseekerXDrivingLicenceAud(name, null);
    }

    // -------------------------------------------------------------------------
    // Row4 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row4<Integer, Long, Long, Byte> fieldsRow() {
        return (Row4) super.fieldsRow();
    }
}
