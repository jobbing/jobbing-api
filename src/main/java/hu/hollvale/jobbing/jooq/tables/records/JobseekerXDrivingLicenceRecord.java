/*
 * This file is generated by jOOQ.
 */
package hu.hollvale.jobbing.jooq.tables.records;


import hu.hollvale.jobbing.jooq.tables.JobseekerXDrivingLicence;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JobseekerXDrivingLicenceRecord extends TableRecordImpl<JobseekerXDrivingLicenceRecord> implements Record2<Long, Long> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>JOBSEEKER_X_DRIVING_LICENCE.JOBSEEKER_ID</code>.
     */
    public void setJobseekerId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>JOBSEEKER_X_DRIVING_LICENCE.JOBSEEKER_ID</code>.
     */
    public Long getJobseekerId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>JOBSEEKER_X_DRIVING_LICENCE.DRIVING_LICENCE_ID</code>.
     */
    public void setDrivingLicenceId(Long value) {
        set(1, value);
    }

    /**
     * Getter for <code>JOBSEEKER_X_DRIVING_LICENCE.DRIVING_LICENCE_ID</code>.
     */
    public Long getDrivingLicenceId() {
        return (Long) get(1);
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row2<Long, Long> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    @Override
    public Row2<Long, Long> valuesRow() {
        return (Row2) super.valuesRow();
    }

    @Override
    public Field<Long> field1() {
        return JobseekerXDrivingLicence.JOBSEEKER_X_DRIVING_LICENCE.JOBSEEKER_ID;
    }

    @Override
    public Field<Long> field2() {
        return JobseekerXDrivingLicence.JOBSEEKER_X_DRIVING_LICENCE.DRIVING_LICENCE_ID;
    }

    @Override
    public Long component1() {
        return getJobseekerId();
    }

    @Override
    public Long component2() {
        return getDrivingLicenceId();
    }

    @Override
    public Long value1() {
        return getJobseekerId();
    }

    @Override
    public Long value2() {
        return getDrivingLicenceId();
    }

    @Override
    public JobseekerXDrivingLicenceRecord value1(Long value) {
        setJobseekerId(value);
        return this;
    }

    @Override
    public JobseekerXDrivingLicenceRecord value2(Long value) {
        setDrivingLicenceId(value);
        return this;
    }

    @Override
    public JobseekerXDrivingLicenceRecord values(Long value1, Long value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached JobseekerXDrivingLicenceRecord
     */
    public JobseekerXDrivingLicenceRecord() {
        super(JobseekerXDrivingLicence.JOBSEEKER_X_DRIVING_LICENCE);
    }

    /**
     * Create a detached, initialised JobseekerXDrivingLicenceRecord
     */
    public JobseekerXDrivingLicenceRecord(Long jobseekerId, Long drivingLicenceId) {
        super(JobseekerXDrivingLicence.JOBSEEKER_X_DRIVING_LICENCE);

        setJobseekerId(jobseekerId);
        setDrivingLicenceId(drivingLicenceId);
    }
}
