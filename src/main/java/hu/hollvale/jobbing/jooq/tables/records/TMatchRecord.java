/*
 * This file is generated by jOOQ.
 */
package hu.hollvale.jobbing.jooq.tables.records;


import hu.hollvale.jobbing.jooq.tables.TMatch;

import java.time.LocalDateTime;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record8;
import org.jooq.Row8;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TMatchRecord extends UpdatableRecordImpl<TMatchRecord> implements Record8<Long, String, LocalDateTime, String, LocalDateTime, String, Long, Long> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>T_MATCH.ID</code>.
     */
    public void setId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>T_MATCH.ID</code>.
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>T_MATCH.CREATED_BY</code>.
     */
    public void setCreatedBy(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>T_MATCH.CREATED_BY</code>.
     */
    public String getCreatedBy() {
        return (String) get(1);
    }

    /**
     * Setter for <code>T_MATCH.CREATION_TIME</code>.
     */
    public void setCreationTime(LocalDateTime value) {
        set(2, value);
    }

    /**
     * Getter for <code>T_MATCH.CREATION_TIME</code>.
     */
    public LocalDateTime getCreationTime() {
        return (LocalDateTime) get(2);
    }

    /**
     * Setter for <code>T_MATCH.MODIFIED_BY</code>.
     */
    public void setModifiedBy(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>T_MATCH.MODIFIED_BY</code>.
     */
    public String getModifiedBy() {
        return (String) get(3);
    }

    /**
     * Setter for <code>T_MATCH.MODIFIED_TIME</code>.
     */
    public void setModifiedTime(LocalDateTime value) {
        set(4, value);
    }

    /**
     * Getter for <code>T_MATCH.MODIFIED_TIME</code>.
     */
    public LocalDateTime getModifiedTime() {
        return (LocalDateTime) get(4);
    }

    /**
     * Setter for <code>T_MATCH.STATE</code>.
     */
    public void setState(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>T_MATCH.STATE</code>.
     */
    public String getState() {
        return (String) get(5);
    }

    /**
     * Setter for <code>T_MATCH.COMPANY_ID</code>.
     */
    public void setCompanyId(Long value) {
        set(6, value);
    }

    /**
     * Getter for <code>T_MATCH.COMPANY_ID</code>.
     */
    public Long getCompanyId() {
        return (Long) get(6);
    }

    /**
     * Setter for <code>T_MATCH.JOBSEEKER_ID</code>.
     */
    public void setJobseekerId(Long value) {
        set(7, value);
    }

    /**
     * Getter for <code>T_MATCH.JOBSEEKER_ID</code>.
     */
    public Long getJobseekerId() {
        return (Long) get(7);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record8 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row8<Long, String, LocalDateTime, String, LocalDateTime, String, Long, Long> fieldsRow() {
        return (Row8) super.fieldsRow();
    }

    @Override
    public Row8<Long, String, LocalDateTime, String, LocalDateTime, String, Long, Long> valuesRow() {
        return (Row8) super.valuesRow();
    }

    @Override
    public Field<Long> field1() {
        return TMatch.T_MATCH.ID;
    }

    @Override
    public Field<String> field2() {
        return TMatch.T_MATCH.CREATED_BY;
    }

    @Override
    public Field<LocalDateTime> field3() {
        return TMatch.T_MATCH.CREATION_TIME;
    }

    @Override
    public Field<String> field4() {
        return TMatch.T_MATCH.MODIFIED_BY;
    }

    @Override
    public Field<LocalDateTime> field5() {
        return TMatch.T_MATCH.MODIFIED_TIME;
    }

    @Override
    public Field<String> field6() {
        return TMatch.T_MATCH.STATE;
    }

    @Override
    public Field<Long> field7() {
        return TMatch.T_MATCH.COMPANY_ID;
    }

    @Override
    public Field<Long> field8() {
        return TMatch.T_MATCH.JOBSEEKER_ID;
    }

    @Override
    public Long component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getCreatedBy();
    }

    @Override
    public LocalDateTime component3() {
        return getCreationTime();
    }

    @Override
    public String component4() {
        return getModifiedBy();
    }

    @Override
    public LocalDateTime component5() {
        return getModifiedTime();
    }

    @Override
    public String component6() {
        return getState();
    }

    @Override
    public Long component7() {
        return getCompanyId();
    }

    @Override
    public Long component8() {
        return getJobseekerId();
    }

    @Override
    public Long value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getCreatedBy();
    }

    @Override
    public LocalDateTime value3() {
        return getCreationTime();
    }

    @Override
    public String value4() {
        return getModifiedBy();
    }

    @Override
    public LocalDateTime value5() {
        return getModifiedTime();
    }

    @Override
    public String value6() {
        return getState();
    }

    @Override
    public Long value7() {
        return getCompanyId();
    }

    @Override
    public Long value8() {
        return getJobseekerId();
    }

    @Override
    public TMatchRecord value1(Long value) {
        setId(value);
        return this;
    }

    @Override
    public TMatchRecord value2(String value) {
        setCreatedBy(value);
        return this;
    }

    @Override
    public TMatchRecord value3(LocalDateTime value) {
        setCreationTime(value);
        return this;
    }

    @Override
    public TMatchRecord value4(String value) {
        setModifiedBy(value);
        return this;
    }

    @Override
    public TMatchRecord value5(LocalDateTime value) {
        setModifiedTime(value);
        return this;
    }

    @Override
    public TMatchRecord value6(String value) {
        setState(value);
        return this;
    }

    @Override
    public TMatchRecord value7(Long value) {
        setCompanyId(value);
        return this;
    }

    @Override
    public TMatchRecord value8(Long value) {
        setJobseekerId(value);
        return this;
    }

    @Override
    public TMatchRecord values(Long value1, String value2, LocalDateTime value3, String value4, LocalDateTime value5, String value6, Long value7, Long value8) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TMatchRecord
     */
    public TMatchRecord() {
        super(TMatch.T_MATCH);
    }

    /**
     * Create a detached, initialised TMatchRecord
     */
    public TMatchRecord(Long id, String createdBy, LocalDateTime creationTime, String modifiedBy, LocalDateTime modifiedTime, String state, Long companyId, Long jobseekerId) {
        super(TMatch.T_MATCH);

        setId(id);
        setCreatedBy(createdBy);
        setCreationTime(creationTime);
        setModifiedBy(modifiedBy);
        setModifiedTime(modifiedTime);
        setState(state);
        setCompanyId(companyId);
        setJobseekerId(jobseekerId);
    }
}
