/*
 * This file is generated by jOOQ.
 */
package hu.hollvale.jobbing.jooq.tables.records;


import hu.hollvale.jobbing.jooq.tables.TCompanyEmployee;

import java.time.LocalDateTime;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record7;
import org.jooq.Row7;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TCompanyEmployeeRecord extends UpdatableRecordImpl<TCompanyEmployeeRecord> implements Record7<Long, String, LocalDateTime, String, LocalDateTime, Long, Long> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>T_COMPANY_EMPLOYEE.ID</code>.
     */
    public void setId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>T_COMPANY_EMPLOYEE.ID</code>.
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>T_COMPANY_EMPLOYEE.CREATED_BY</code>.
     */
    public void setCreatedBy(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>T_COMPANY_EMPLOYEE.CREATED_BY</code>.
     */
    public String getCreatedBy() {
        return (String) get(1);
    }

    /**
     * Setter for <code>T_COMPANY_EMPLOYEE.CREATION_TIME</code>.
     */
    public void setCreationTime(LocalDateTime value) {
        set(2, value);
    }

    /**
     * Getter for <code>T_COMPANY_EMPLOYEE.CREATION_TIME</code>.
     */
    public LocalDateTime getCreationTime() {
        return (LocalDateTime) get(2);
    }

    /**
     * Setter for <code>T_COMPANY_EMPLOYEE.MODIFIED_BY</code>.
     */
    public void setModifiedBy(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>T_COMPANY_EMPLOYEE.MODIFIED_BY</code>.
     */
    public String getModifiedBy() {
        return (String) get(3);
    }

    /**
     * Setter for <code>T_COMPANY_EMPLOYEE.MODIFIED_TIME</code>.
     */
    public void setModifiedTime(LocalDateTime value) {
        set(4, value);
    }

    /**
     * Getter for <code>T_COMPANY_EMPLOYEE.MODIFIED_TIME</code>.
     */
    public LocalDateTime getModifiedTime() {
        return (LocalDateTime) get(4);
    }

    /**
     * Setter for <code>T_COMPANY_EMPLOYEE.COMPANY_ID</code>.
     */
    public void setCompanyId(Long value) {
        set(5, value);
    }

    /**
     * Getter for <code>T_COMPANY_EMPLOYEE.COMPANY_ID</code>.
     */
    public Long getCompanyId() {
        return (Long) get(5);
    }

    /**
     * Setter for <code>T_COMPANY_EMPLOYEE.USER_ID</code>.
     */
    public void setUserId(Long value) {
        set(6, value);
    }

    /**
     * Getter for <code>T_COMPANY_EMPLOYEE.USER_ID</code>.
     */
    public Long getUserId() {
        return (Long) get(6);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record7 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row7<Long, String, LocalDateTime, String, LocalDateTime, Long, Long> fieldsRow() {
        return (Row7) super.fieldsRow();
    }

    @Override
    public Row7<Long, String, LocalDateTime, String, LocalDateTime, Long, Long> valuesRow() {
        return (Row7) super.valuesRow();
    }

    @Override
    public Field<Long> field1() {
        return TCompanyEmployee.T_COMPANY_EMPLOYEE.ID;
    }

    @Override
    public Field<String> field2() {
        return TCompanyEmployee.T_COMPANY_EMPLOYEE.CREATED_BY;
    }

    @Override
    public Field<LocalDateTime> field3() {
        return TCompanyEmployee.T_COMPANY_EMPLOYEE.CREATION_TIME;
    }

    @Override
    public Field<String> field4() {
        return TCompanyEmployee.T_COMPANY_EMPLOYEE.MODIFIED_BY;
    }

    @Override
    public Field<LocalDateTime> field5() {
        return TCompanyEmployee.T_COMPANY_EMPLOYEE.MODIFIED_TIME;
    }

    @Override
    public Field<Long> field6() {
        return TCompanyEmployee.T_COMPANY_EMPLOYEE.COMPANY_ID;
    }

    @Override
    public Field<Long> field7() {
        return TCompanyEmployee.T_COMPANY_EMPLOYEE.USER_ID;
    }

    @Override
    public Long component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getCreatedBy();
    }

    @Override
    public LocalDateTime component3() {
        return getCreationTime();
    }

    @Override
    public String component4() {
        return getModifiedBy();
    }

    @Override
    public LocalDateTime component5() {
        return getModifiedTime();
    }

    @Override
    public Long component6() {
        return getCompanyId();
    }

    @Override
    public Long component7() {
        return getUserId();
    }

    @Override
    public Long value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getCreatedBy();
    }

    @Override
    public LocalDateTime value3() {
        return getCreationTime();
    }

    @Override
    public String value4() {
        return getModifiedBy();
    }

    @Override
    public LocalDateTime value5() {
        return getModifiedTime();
    }

    @Override
    public Long value6() {
        return getCompanyId();
    }

    @Override
    public Long value7() {
        return getUserId();
    }

    @Override
    public TCompanyEmployeeRecord value1(Long value) {
        setId(value);
        return this;
    }

    @Override
    public TCompanyEmployeeRecord value2(String value) {
        setCreatedBy(value);
        return this;
    }

    @Override
    public TCompanyEmployeeRecord value3(LocalDateTime value) {
        setCreationTime(value);
        return this;
    }

    @Override
    public TCompanyEmployeeRecord value4(String value) {
        setModifiedBy(value);
        return this;
    }

    @Override
    public TCompanyEmployeeRecord value5(LocalDateTime value) {
        setModifiedTime(value);
        return this;
    }

    @Override
    public TCompanyEmployeeRecord value6(Long value) {
        setCompanyId(value);
        return this;
    }

    @Override
    public TCompanyEmployeeRecord value7(Long value) {
        setUserId(value);
        return this;
    }

    @Override
    public TCompanyEmployeeRecord values(Long value1, String value2, LocalDateTime value3, String value4, LocalDateTime value5, Long value6, Long value7) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TCompanyEmployeeRecord
     */
    public TCompanyEmployeeRecord() {
        super(TCompanyEmployee.T_COMPANY_EMPLOYEE);
    }

    /**
     * Create a detached, initialised TCompanyEmployeeRecord
     */
    public TCompanyEmployeeRecord(Long id, String createdBy, LocalDateTime creationTime, String modifiedBy, LocalDateTime modifiedTime, Long companyId, Long userId) {
        super(TCompanyEmployee.T_COMPANY_EMPLOYEE);

        setId(id);
        setCreatedBy(createdBy);
        setCreationTime(creationTime);
        setModifiedBy(modifiedBy);
        setModifiedTime(modifiedTime);
        setCompanyId(companyId);
        setUserId(userId);
    }
}
