/*
 * This file is generated by jOOQ.
 */
package hu.hollvale.jobbing.jooq.tables.records;


import hu.hollvale.jobbing.jooq.tables.JobAdvertisementXWorkCategory;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JobAdvertisementXWorkCategoryRecord extends TableRecordImpl<JobAdvertisementXWorkCategoryRecord> implements Record2<Long, Long> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>JOB_ADVERTISEMENT_X_WORK_CATEGORY.JOB_ADVERTISEMENT_ID</code>.
     */
    public void setJobAdvertisementId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>JOB_ADVERTISEMENT_X_WORK_CATEGORY.JOB_ADVERTISEMENT_ID</code>.
     */
    public Long getJobAdvertisementId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>JOB_ADVERTISEMENT_X_WORK_CATEGORY.WORK_CATEGORY_ID</code>.
     */
    public void setWorkCategoryId(Long value) {
        set(1, value);
    }

    /**
     * Getter for <code>JOB_ADVERTISEMENT_X_WORK_CATEGORY.WORK_CATEGORY_ID</code>.
     */
    public Long getWorkCategoryId() {
        return (Long) get(1);
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row2<Long, Long> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    @Override
    public Row2<Long, Long> valuesRow() {
        return (Row2) super.valuesRow();
    }

    @Override
    public Field<Long> field1() {
        return JobAdvertisementXWorkCategory.JOB_ADVERTISEMENT_X_WORK_CATEGORY.JOB_ADVERTISEMENT_ID;
    }

    @Override
    public Field<Long> field2() {
        return JobAdvertisementXWorkCategory.JOB_ADVERTISEMENT_X_WORK_CATEGORY.WORK_CATEGORY_ID;
    }

    @Override
    public Long component1() {
        return getJobAdvertisementId();
    }

    @Override
    public Long component2() {
        return getWorkCategoryId();
    }

    @Override
    public Long value1() {
        return getJobAdvertisementId();
    }

    @Override
    public Long value2() {
        return getWorkCategoryId();
    }

    @Override
    public JobAdvertisementXWorkCategoryRecord value1(Long value) {
        setJobAdvertisementId(value);
        return this;
    }

    @Override
    public JobAdvertisementXWorkCategoryRecord value2(Long value) {
        setWorkCategoryId(value);
        return this;
    }

    @Override
    public JobAdvertisementXWorkCategoryRecord values(Long value1, Long value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached JobAdvertisementXWorkCategoryRecord
     */
    public JobAdvertisementXWorkCategoryRecord() {
        super(JobAdvertisementXWorkCategory.JOB_ADVERTISEMENT_X_WORK_CATEGORY);
    }

    /**
     * Create a detached, initialised JobAdvertisementXWorkCategoryRecord
     */
    public JobAdvertisementXWorkCategoryRecord(Long jobAdvertisementId, Long workCategoryId) {
        super(JobAdvertisementXWorkCategory.JOB_ADVERTISEMENT_X_WORK_CATEGORY);

        setJobAdvertisementId(jobAdvertisementId);
        setWorkCategoryId(workCategoryId);
    }
}
