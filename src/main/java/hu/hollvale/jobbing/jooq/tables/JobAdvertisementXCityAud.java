/*
 * This file is generated by jOOQ.
 */
package hu.hollvale.jobbing.jooq.tables;


import hu.hollvale.jobbing.jooq.DefaultSchema;
import hu.hollvale.jobbing.jooq.Keys;
import hu.hollvale.jobbing.jooq.tables.records.JobAdvertisementXCityAudRecord;

import java.util.Arrays;
import java.util.List;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row4;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JobAdvertisementXCityAud extends TableImpl<JobAdvertisementXCityAudRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>JOB_ADVERTISEMENT_X_CITY_AUD</code>
     */
    public static final JobAdvertisementXCityAud JOB_ADVERTISEMENT_X_CITY_AUD = new JobAdvertisementXCityAud();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<JobAdvertisementXCityAudRecord> getRecordType() {
        return JobAdvertisementXCityAudRecord.class;
    }

    /**
     * The column <code>JOB_ADVERTISEMENT_X_CITY_AUD.REV</code>.
     */
    public final TableField<JobAdvertisementXCityAudRecord, Integer> REV = createField(DSL.name("REV"), SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>JOB_ADVERTISEMENT_X_CITY_AUD.JOB_ADVERTISEMENT_ID</code>.
     */
    public final TableField<JobAdvertisementXCityAudRecord, Long> JOB_ADVERTISEMENT_ID = createField(DSL.name("JOB_ADVERTISEMENT_ID"), SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>JOB_ADVERTISEMENT_X_CITY_AUD.CITY_ID</code>.
     */
    public final TableField<JobAdvertisementXCityAudRecord, Long> CITY_ID = createField(DSL.name("CITY_ID"), SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>JOB_ADVERTISEMENT_X_CITY_AUD.REVTYPE</code>.
     */
    public final TableField<JobAdvertisementXCityAudRecord, Byte> REVTYPE = createField(DSL.name("REVTYPE"), SQLDataType.TINYINT, this, "");

    private JobAdvertisementXCityAud(Name alias, Table<JobAdvertisementXCityAudRecord> aliased) {
        this(alias, aliased, null);
    }

    private JobAdvertisementXCityAud(Name alias, Table<JobAdvertisementXCityAudRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>JOB_ADVERTISEMENT_X_CITY_AUD</code> table reference
     */
    public JobAdvertisementXCityAud(String alias) {
        this(DSL.name(alias), JOB_ADVERTISEMENT_X_CITY_AUD);
    }

    /**
     * Create an aliased <code>JOB_ADVERTISEMENT_X_CITY_AUD</code> table reference
     */
    public JobAdvertisementXCityAud(Name alias) {
        this(alias, JOB_ADVERTISEMENT_X_CITY_AUD);
    }

    /**
     * Create a <code>JOB_ADVERTISEMENT_X_CITY_AUD</code> table reference
     */
    public JobAdvertisementXCityAud() {
        this(DSL.name("JOB_ADVERTISEMENT_X_CITY_AUD"), null);
    }

    public <O extends Record> JobAdvertisementXCityAud(Table<O> child, ForeignKey<O, JobAdvertisementXCityAudRecord> key) {
        super(child, key, JOB_ADVERTISEMENT_X_CITY_AUD);
    }

    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    @Override
    public UniqueKey<JobAdvertisementXCityAudRecord> getPrimaryKey() {
        return Keys.CONSTRAINT_F;
    }

    @Override
    public List<UniqueKey<JobAdvertisementXCityAudRecord>> getKeys() {
        return Arrays.<UniqueKey<JobAdvertisementXCityAudRecord>>asList(Keys.CONSTRAINT_F);
    }

    @Override
    public List<ForeignKey<JobAdvertisementXCityAudRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<JobAdvertisementXCityAudRecord, ?>>asList(Keys.FKRST402TUT0BEN5CNE9YONSJA5);
    }

    private transient Revinfo _revinfo;

    public Revinfo revinfo() {
        if (_revinfo == null)
            _revinfo = new Revinfo(this, Keys.FKRST402TUT0BEN5CNE9YONSJA5);

        return _revinfo;
    }

    @Override
    public JobAdvertisementXCityAud as(String alias) {
        return new JobAdvertisementXCityAud(DSL.name(alias), this);
    }

    @Override
    public JobAdvertisementXCityAud as(Name alias) {
        return new JobAdvertisementXCityAud(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public JobAdvertisementXCityAud rename(String name) {
        return new JobAdvertisementXCityAud(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public JobAdvertisementXCityAud rename(Name name) {
        return new JobAdvertisementXCityAud(name, null);
    }

    // -------------------------------------------------------------------------
    // Row4 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row4<Integer, Long, Long, Byte> fieldsRow() {
        return (Row4) super.fieldsRow();
    }
}
