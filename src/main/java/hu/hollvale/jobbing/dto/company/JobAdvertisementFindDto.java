package hu.hollvale.jobbing.dto.company;

public interface JobAdvertisementFindDto {

	Long getId();

	String getName();

	String getCompanyLogo();

	String getCompanyName();

	String getPlace();

	String getSummary();
}
