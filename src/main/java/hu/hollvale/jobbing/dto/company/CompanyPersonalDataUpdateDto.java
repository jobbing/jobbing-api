package hu.hollvale.jobbing.dto.company;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyPersonalDataUpdateDto {

	@NotNull
	private Long companyId;
	private String email;
	private String contactPerson;
	private String address;
	private String phoneNumber;

}
