package hu.hollvale.jobbing.dto.company;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDataDto {

	private Long id;
	private String name;
	private String username;
	private String email;
	private String roleName;
}
