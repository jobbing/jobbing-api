package hu.hollvale.jobbing.dto.company;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetListValuesDto {

	private List<CityDto> cities;
	private List<WorkCategoryDto> workCategories;
	private List<WorkTypeDto> workTypes;
	
}
