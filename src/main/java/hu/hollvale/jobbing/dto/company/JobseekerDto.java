package hu.hollvale.jobbing.dto.company;

public interface JobseekerDto {

	Long getId();

	String getName();

	String getPicture();

	String getIntroduction();

}
