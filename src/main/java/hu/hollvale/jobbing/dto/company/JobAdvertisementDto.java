package hu.hollvale.jobbing.dto.company;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobAdvertisementDto {

	@NotNull
	private Long companyId;
	@NotNull
	private Long id;
	@NotBlank
	private String name;
	@NotBlank
	private String place;
	private String summary;
	private String textEditor;
	private List<Long> workCategoryIdList;
	private List<Long> workTypeIdList;
	private List<Long> cityIdList;
	private List<String> newWorkCategoryNameList;

}
