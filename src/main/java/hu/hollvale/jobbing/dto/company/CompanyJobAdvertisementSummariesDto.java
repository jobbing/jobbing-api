package hu.hollvale.jobbing.dto.company;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyJobAdvertisementSummariesDto {

	private Long id;
	private String name;
	private String summary;

}
