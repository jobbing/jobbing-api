package hu.hollvale.jobbing.dto.company;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyIntroductionUpdateDto {

	@NotNull
	private Long companyId;
	private String introduction;

}
