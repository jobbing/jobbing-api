package hu.hollvale.jobbing.dto.company;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyRegisterDto {

	@NotBlank
	private String username;

	@NotBlank
	private String name;

	@NotBlank
	private String email;

	@NotBlank
	private String companyName;

	@NotBlank
	private String password;

}
