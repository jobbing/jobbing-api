package hu.hollvale.jobbing.dto.company;

import java.util.List;

import hu.hollvale.jobbing.dto.match.ApplicantDto;

public interface CompanyJobAdvertisementListDto {

	Long getId();

	String getName();

	String getSummary();

	List<ApplicantDto> getApplicants();

}
