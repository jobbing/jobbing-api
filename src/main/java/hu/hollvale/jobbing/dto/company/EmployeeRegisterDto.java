package hu.hollvale.jobbing.dto.company;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRegisterDto {
	@NotNull
	private Long companyId;
	@NotBlank
	private String username;
	@NotBlank
	private String email;
	@NotBlank
	private String name;
	@NotBlank
	private String password;
	@NotNull
	private Long roleId;

}
