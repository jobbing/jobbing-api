package hu.hollvale.jobbing.dto.company;

import hu.hollvale.jobbing.enums.WorkingType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkTypeDto {

	private Long id;
	private WorkingType name;
}
