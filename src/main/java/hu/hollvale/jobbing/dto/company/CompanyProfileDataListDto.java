package hu.hollvale.jobbing.dto.company;

public interface CompanyProfileDataListDto {

	Long getId();

	String getName();

	String getEmail();

	String getLogo();

	String getIntroduction();

	String getPhoneNumber();

	String getAddress();

	String getContactPerson();
}
