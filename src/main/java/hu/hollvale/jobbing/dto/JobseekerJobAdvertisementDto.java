package hu.hollvale.jobbing.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobseekerJobAdvertisementDto extends GetJobAdvertisementDto {
	@NotNull
	private Long companyId;
	private String companyName;
	private String companyLogo;
	private String companyIntroduction;

}
