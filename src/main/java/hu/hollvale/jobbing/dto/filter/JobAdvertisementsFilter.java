package hu.hollvale.jobbing.dto.filter;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobAdvertisementsFilter {

	private Long jobseekerId;
	private List<Long> cityIdList;
	private List<Long> workCategoryIdList;
	private List<Long> workTypeIdList;
}
