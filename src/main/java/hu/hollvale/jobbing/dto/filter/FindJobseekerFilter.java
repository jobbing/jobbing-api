package hu.hollvale.jobbing.dto.filter;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import hu.hollvale.jobbing.enums.DrivingLicenceType;
import hu.hollvale.jobbing.enums.LanguageType;
import hu.hollvale.jobbing.enums.LevelType;
import hu.hollvale.jobbing.enums.QualityType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FindJobseekerFilter {

	@NotNull
	private Long companyId;
	private LocalDate youngerThan;
	private LocalDate olderThan;
	private LanguageType language;
	private LevelType level;
	private DrivingLicenceType drivingLicence;
	private QualityType quality;

}
