package hu.hollvale.jobbing.dto.filter;

import javax.validation.constraints.NotNull;

import hu.hollvale.jobbing.enums.MatchStateType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdvertisementStateFilter {

	@NotNull
	private Long jobseekerId;
	@NotNull
	private MatchStateType matchStateType;
}
