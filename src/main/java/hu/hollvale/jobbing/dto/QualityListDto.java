package hu.hollvale.jobbing.dto;

import hu.hollvale.jobbing.enums.QualityType;

public interface QualityListDto {

	Long getId();
	
	String getCity();

	String getSchool();

	QualityType getType();

	String getYear();

}
