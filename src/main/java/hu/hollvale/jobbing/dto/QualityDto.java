package hu.hollvale.jobbing.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;

import hu.hollvale.jobbing.enums.QualityType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QualityDto {
	@NotNull
	private Long jobseekerId;
	@NotBlank
	private String city;
	@NotBlank
	private String school;
	@NotNull
	private QualityType type;
	@NotNull
	@Min(value = 1900)

	@NumberFormat
	private Integer year;

}
