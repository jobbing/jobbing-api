package hu.hollvale.jobbing.dto;

import hu.hollvale.jobbing.enums.LanguageType;
import hu.hollvale.jobbing.enums.LevelType;

public interface LanguageExamListDto {

	Long getLanguageExamId();

	LanguageType getLanguage();

	LevelType getLevel();

}
