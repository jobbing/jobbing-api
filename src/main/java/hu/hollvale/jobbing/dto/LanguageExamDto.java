package hu.hollvale.jobbing.dto;

import javax.validation.constraints.NotNull;

import hu.hollvale.jobbing.enums.LanguageType;
import hu.hollvale.jobbing.enums.LevelType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LanguageExamDto {
	@NotNull
	private Long jobseekerId;
	@NotNull
	private LanguageType language;
	@NotNull
	private LevelType level;
}
