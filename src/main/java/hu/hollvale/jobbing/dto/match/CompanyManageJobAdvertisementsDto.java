package hu.hollvale.jobbing.dto.match;

import java.util.List;

import hu.hollvale.jobbing.dto.company.CompanyJobAdvertisementListDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyManageJobAdvertisementsDto {

	private Long id;
	private String name;
	private String summary;
	private List<ApplicantDto> applicants;

	public static CompanyManageJobAdvertisementsDto fromCompanyJobAdvertisementListDto(
			CompanyJobAdvertisementListDto companyJobAdvertisementListDto) {

		CompanyManageJobAdvertisementsDto companyManageJobAdvertisementsDto = new CompanyManageJobAdvertisementsDto();
		companyManageJobAdvertisementsDto.setId(companyJobAdvertisementListDto.getId());
		companyManageJobAdvertisementsDto.setName(companyJobAdvertisementListDto.getName());
		companyManageJobAdvertisementsDto.setSummary(companyJobAdvertisementListDto.getSummary());

		return companyManageJobAdvertisementsDto;
	}

}
