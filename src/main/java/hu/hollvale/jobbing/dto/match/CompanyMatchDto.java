package hu.hollvale.jobbing.dto.match;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyMatchDto {

	@NotNull
	private Long companyId;
	@NotNull
	private Long jobseekerId;

}
