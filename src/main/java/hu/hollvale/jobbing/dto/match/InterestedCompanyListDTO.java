package hu.hollvale.jobbing.dto.match;

public interface InterestedCompanyListDTO {

	Long getId();

	String getName();

	String getLogo();
}
