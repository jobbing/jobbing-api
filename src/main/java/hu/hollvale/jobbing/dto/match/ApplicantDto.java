package hu.hollvale.jobbing.dto.match;

import hu.hollvale.jobbing.enums.MatchStateType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApplicantDto {

	private Long jobseekerId;
	private Long jobAdvertisementId;
	private String jobseekerName;
	private MatchStateType matchState;
}
