package hu.hollvale.jobbing.dto.match;

import javax.validation.constraints.NotNull;

import hu.hollvale.jobbing.model.company.Company;
import hu.hollvale.jobbing.model.company.JobAdvertisement;
import hu.hollvale.jobbing.model.jobseeker.Jobseeker;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MatchDto {
	@NotNull
	private Jobseeker jobseeker;
	@NotNull
	private JobAdvertisement jobAdvertisement;
	@NotNull
	private Company company;

}
