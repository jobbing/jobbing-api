package hu.hollvale.jobbing.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import hu.hollvale.jobbing.enums.WorkingType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetJobAdvertisementDto {
	@NotNull
	private Long id;
	private String name;
	private String place;
	private String summary;
	private String textEditor;
	private List<String> workCategoryNameList;
	private List<WorkingType> workTypeNameList;
	private List<String> cityNameList;

}
