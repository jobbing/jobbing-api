package hu.hollvale.jobbing.dto;

import java.sql.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExperienceDto {
	@NotNull
	private Long jobseekerId;
	@NotBlank
	private String name;
	@NotNull
	private Date startDate;
	@NotNull
	private Date endDate;

}
