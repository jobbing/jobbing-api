package hu.hollvale.jobbing.dto;

public interface JobAdvertisementListDto {

	Long getId();

	String getName();

	String getLogo();

	String getSummary();

}
