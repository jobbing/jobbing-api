package hu.hollvale.jobbing.dto.user;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import hu.hollvale.jobbing.model.user.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

	@NotNull
	private String username;
	private String password;
	private Collection<Role> roles;
}
