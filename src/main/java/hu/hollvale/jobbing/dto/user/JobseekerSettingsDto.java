package hu.hollvale.jobbing.dto.user;

public interface JobseekerSettingsDto {

	String getName();

	String getUsername();

	String getEmail();
}
