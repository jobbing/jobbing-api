package hu.hollvale.jobbing.dto.user;

import hu.hollvale.jobbing.enums.HolderType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginDto {

	String jwt;
	HolderType holderType;
}
