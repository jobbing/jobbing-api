package hu.hollvale.jobbing.dto.user;

import java.util.List;

import hu.hollvale.jobbing.enums.HolderType;
import hu.hollvale.jobbing.model.user.Permission;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetMyDatasDto {

	String username;
	String name;
	Long userId;
	HolderType holderType;
	Long jobseekerId;
	Long companyId;
	List<Permission> permissions;
}
