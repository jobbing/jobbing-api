package hu.hollvale.jobbing.dto.user;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsernameChangeDto {

	@NotNull
	private String username;
	private String newUsername;
}
