package hu.hollvale.jobbing.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProfileDataDto {

	private String name;
	private String picture;
	private String cv;
	private String introduction;
	private String birthPlace;
	private Date birthTime;
	private String email;
	private String address;
	private String residence;
	private String workILookfor;

}
