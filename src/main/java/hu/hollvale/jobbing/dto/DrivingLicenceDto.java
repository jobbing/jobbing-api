package hu.hollvale.jobbing.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import hu.hollvale.jobbing.enums.DrivingLicenceType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DrivingLicenceDto {
	@NotNull
	private Long jobseekerId;
	private List<DrivingLicenceType> value;
}
