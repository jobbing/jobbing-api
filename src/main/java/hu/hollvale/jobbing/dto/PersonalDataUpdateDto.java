package hu.hollvale.jobbing.dto;

import java.sql.Date;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonalDataUpdateDto {
	@NotNull
	private Long jobseekerId;
	private String birthPlace;
	private Date birthTime;
	private String address;
	private String residence;
}
