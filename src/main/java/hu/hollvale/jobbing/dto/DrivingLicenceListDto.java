package hu.hollvale.jobbing.dto;

import hu.hollvale.jobbing.enums.DrivingLicenceType;

public interface DrivingLicenceListDto {

	DrivingLicenceType getValue();
}
