package hu.hollvale.jobbing.dto;

import java.sql.Date;

public interface ExperienceListDto {

	Long getId();

	String getName();

	Date getStartDate();

	Date getEndDate();
}
