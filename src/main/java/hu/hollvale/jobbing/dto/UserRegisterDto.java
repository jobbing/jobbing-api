package hu.hollvale.jobbing.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRegisterDto {

	@NotBlank
	private String name;
	@NotBlank
	private String username;
	@Email
	@NotBlank
	private String email;
	@NotBlank
	private String password;

}
