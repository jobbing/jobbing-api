package hu.hollvale.jobbing.dto;

import javax.validation.constraints.NotBlank;

import hu.hollvale.jobbing.model.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginDto {
	@NotBlank
	private String username;
	@NotBlank
	private String password;

	public void loginDto(User user) {

		this.password = user.getPassword();
		this.username = user.getUsername();
	}
}
