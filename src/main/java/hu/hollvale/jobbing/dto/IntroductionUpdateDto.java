package hu.hollvale.jobbing.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IntroductionUpdateDto {
	@NotNull
	private Long jobseekerId;
	private String introduction;

}
