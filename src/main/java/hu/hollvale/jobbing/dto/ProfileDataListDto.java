package hu.hollvale.jobbing.dto;

import java.sql.Date;

public interface ProfileDataListDto {

	Long getId();

	String getName();

	String getPicture();

	String getCV();

	String getIntroduction();

	String getBirthPlace();

	Date getBirthTime();

	String getEmail();

	String getAddress();

	String getResidence();

	String getWorkILookFor();
}
