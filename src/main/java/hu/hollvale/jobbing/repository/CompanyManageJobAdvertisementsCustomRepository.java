package hu.hollvale.jobbing.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import hu.hollvale.jobbing.dto.match.CompanyManageJobAdvertisementsDto;

public interface CompanyManageJobAdvertisementsCustomRepository {

	public Page<CompanyManageJobAdvertisementsDto> getJobAdvertisementWithAppliciants(Long companyId,
			Pageable pageable);

}
