package hu.hollvale.jobbing.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hu.hollvale.jobbing.dto.DrivingLicenceListDto;
import hu.hollvale.jobbing.dto.ExperienceDto;
import hu.hollvale.jobbing.dto.ExperienceListDto;
import hu.hollvale.jobbing.dto.LanguageExamListDto;
import hu.hollvale.jobbing.dto.PersonalDataUpdateDto;
import hu.hollvale.jobbing.dto.ProfileDataListDto;
import hu.hollvale.jobbing.dto.QualityDto;
import hu.hollvale.jobbing.dto.QualityListDto;
import hu.hollvale.jobbing.model.jobseeker.Jobseeker;

@Repository
public interface JobseekerProfileRepository extends JpaRepository<Jobseeker, Long> {

	public Jobseeker findByUserId(@Param("filter") Long userId);

	// @formatter:off
	@Query(""" 
			SELECT 
				j.id as id,
				j.user.name as name,
				j.user.email as email,
				j.picture as picture,
				j.introduction as introduction,
				j.workILookFor as workILookFor,
				j.CV as CV,
				j.birthPlace as birthPlace,
				j.birthTime as birthTime,
				j.address as address,
				j.residence as residence
			FROM 
				Jobseeker j
			WHERE 
				 j.user.username LIKE :#{#filter} 
	""")
	public ProfileDataListDto loadProfileData(@Param("filter") String username);
	// @formatter:on

	//@formatter:off
	@Query("""
			SELECT
				j.id
			FROM
				Jobseeker j
			WHERE
				j.user.username LIKE :#{#filter}
			""")
	//@formatter:on
	public Long JobseekerIdByUsername(@Param("filter") String username);

	// @formatter:off
		@Query("""
				UPDATE 
					Jobseeker j 
				SET 
					j.birthPlace = :#{#filter.birthPlace},
				 	j.birthTime = :#{#filter.birthTime},
				 	j.address = :#{#filter.address},
				 	j.residence = :#{#filter.residence}
				WHERE
					j.id = :#{#filter.jobseekerId} 
				""")
		@Modifying
		// @formatter:on
	public void updatePersonalData(@Param("filter") PersonalDataUpdateDto personalDataUpdateDto);

	// @formatter:off
		@Query("""
				SELECT 
					dl.value as value 
				FROM 
					Jobseeker j
				JOIN 
					j.drivingLicences dl
				WHERE 
					:#{#filter} = j.id
				""")
	// @formatter:on
	public List<DrivingLicenceListDto> getDrivingLicences(@Param("filter") Long jobseekerId);

	// @formatter:off
		@Query("""
				SELECT
					le.id as languageExamId,
					le.language as language,
					le.level as level
				FROM 
					Jobseeker j
				JOIN
					j.languageExams le
				WHERE
					:#{#filter} = j.id
				""")
		// @formatter:on
	public List<LanguageExamListDto> getLanguageExams(@Param("filter") Long jobseekerId);

	// @formatter:off
		@Query("""
				SELECT
					q.id as id,
					q.city as city,
					q.school as school,
					q.type as type,
					q.year as year
				FROM 
					Quality q
				JOIN 
					Jobseeker j 
				ON 
					q.jobseeker = j.id
				WHERE 
					:#{#filter} = j.id
				""")
		// @formatter:on
	public List<QualityListDto> getQualities(@Param("filter") Long jobseekerId);

	// @formatter:off
		@Query("""
				SELECT
					e.id as id,
					e.name as name,
					e.startDate as startDate,
					e.endDate as endDate
				FROM 
					Experience e
				JOIN 
					Jobseeker j 
				ON 	
					e.jobseeker = j.id
				WHERE
					:#{#filter} = j.id
				""")
		// @formatter:on
	public List<ExperienceListDto> getExperiences(@Param("filter") Long jobseekerId);

	// @formatter:off
		@Query("""
				SELECT
					j
				FROM 
					Jobseeker j
				WHERE
					:#{#filter} = j.user.id
				""")
		// @formatter:on
	public Optional<Jobseeker> userHasJobseeker(@Param("filter") Long userId);

	//@formatter:off
		@Query("""
				SELECT
					j.picture
				FROM
					Jobseeker j
				WHERE 
					j.id = :#{#filter}
				""")
		//@formatter:on
	public String getPicture(@Param("filter") Long jobseekerId);

	// @formatter:off
			@Query("""
					SELECT
						count(e) > 0
					FROM 
						Experience e
					JOIN 
						Jobseeker j 
					ON 	
						e.jobseeker = j.id
					WHERE
						:#{#filter.jobseekerId} = j.id
					AND 
						:#{#filter.name} = e.name
					AND 
						:#{#filter.startDate} = e.startDate
					AND 
						:#{#filter.endDate} = e.endDate
					""")
			// @formatter:on
	public Boolean checkExperienceExist(@Param("filter") ExperienceDto experienceDto);

	// @formatter:off
			@Query("""
					SELECT 
						count(q) > 0
					FROM 
						Quality q
					JOIN 
						Jobseeker j 
					ON 
						q.jobseeker = j.id
					WHERE 
						:#{#filter.jobseekerId} = j.id
					AND
						:#{#filter.city} = q.city
					AND
						:#{#filter.school} = q.school
					AND
						:#{#filter.year} = q.year
					AND
						:#{#filter.type} = q.type
					
					""")
			// @formatter:on
	public Boolean checkQualityExist(@Param("filter") QualityDto qualityDto);

	public Jobseeker getByUserId(Long id);

}
