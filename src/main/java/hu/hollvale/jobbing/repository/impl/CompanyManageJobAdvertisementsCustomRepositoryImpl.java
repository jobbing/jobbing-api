package hu.hollvale.jobbing.repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import hu.hollvale.jobbing.dto.match.ApplicantDto;
import hu.hollvale.jobbing.dto.match.CompanyManageJobAdvertisementsDto;
import hu.hollvale.jobbing.jooq.tables.TJobAdvertisement;
import hu.hollvale.jobbing.jooq.tables.TJobseeker;
import hu.hollvale.jobbing.jooq.tables.TMatchByAdvertisement;
import hu.hollvale.jobbing.jooq.tables.TUser;
import hu.hollvale.jobbing.repository.CompanyManageJobAdvertisementsCustomRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CompanyManageJobAdvertisementsCustomRepositoryImpl
		implements CompanyManageJobAdvertisementsCustomRepository {

	private final DSLContext dslContext;
	private final TJobAdvertisement JOBADVERTISEMENT = TJobAdvertisement.T_JOB_ADVERTISEMENT;
	private final TMatchByAdvertisement MATCH_BY_ADVERTISEMENT = TMatchByAdvertisement.T_MATCH_BY_ADVERTISEMENT;
	private final TJobseeker JOBSEEKER = TJobseeker.T_JOBSEEKER;
	private final TUser USER = TUser.T_USER;
	private final String id = "id";
	private final String name = "name";
	private final String summary = "summary";
	private final String jobseekerId = "jobseekerId";
	private final String jobAdvertisementId = "jobAdvertisementId";
	private final String jobseekerName = "jobseekerName";
	private final String matchState = "matchState";

	@Override
	public Page<CompanyManageJobAdvertisementsDto> getJobAdvertisementWithAppliciants(Long companyId,
			Pageable pageable) {

		Condition condition = DSL.trueCondition();
		//// @formatter:off
		List<CompanyManageJobAdvertisementsDto> jobAdvertisementsWithAppliciants = dslContext
				.select(JOBADVERTISEMENT.ID.as(id),
						JOBADVERTISEMENT.NAME.as(name),
						JOBADVERTISEMENT.SUMMARY.as(summary)
				)
				.from(JOBADVERTISEMENT)
				.where(JOBADVERTISEMENT.COMPANY_ID.eq(companyId))
				.fetchInto(CompanyManageJobAdvertisementsDto.class);
		 
			// @formatter:on

		List<Long> jobAdvertismentIdList = this.loadIdList(jobAdvertisementsWithAppliciants);
		List<ApplicantDto> selectApplicants = this.selectApplicants(jobAdvertismentIdList);
		this.addApplicants(jobAdvertisementsWithAppliciants, selectApplicants);

		PageImpl<CompanyManageJobAdvertisementsDto> resultPage = new PageImpl<>(jobAdvertisementsWithAppliciants);

		return resultPage;

	}

	private void addApplicants(List<CompanyManageJobAdvertisementsDto> jobAdvertisementsWithAppliciants,
			List<ApplicantDto> selectApplicants) {

		//// @formatter:off
		
		jobAdvertisementsWithAppliciants.stream()
		.forEach(
			j -> {
				List<ApplicantDto> myApplicants = new ArrayList<>();
			selectApplicants.stream()
				.filter(a -> j.getId().equals(a.getJobAdvertisementId()))
				.forEach(a -> {
					myApplicants.add(a);
				});
			j.setApplicants(myApplicants);
		});

		// @formatter:on
	}

	private List<Long> loadIdList(List<CompanyManageJobAdvertisementsDto> jobAdvertisementsWithAppliciants) {
		return jobAdvertisementsWithAppliciants.stream().map(CompanyManageJobAdvertisementsDto::getId)
				.collect(Collectors.toList());

	}

	private List<ApplicantDto> selectApplicants(List<Long> advertismentIdList) {

		//// @formatter:off
		return this.dslContext
			.select(JOBSEEKER.ID.as(jobseekerId),
					JOBADVERTISEMENT.ID.as(jobAdvertisementId),
					USER.NAME.as(jobseekerName),
					MATCH_BY_ADVERTISEMENT.STATE.as(matchState))
			.from(JOBSEEKER)
			.join(USER)
				.on(USER.ID.eq(JOBSEEKER.USER_ID))
			.join(MATCH_BY_ADVERTISEMENT)
				.on(MATCH_BY_ADVERTISEMENT.JOBSEEKER_ID.eq(JOBSEEKER.ID))
			.join(JOBADVERTISEMENT)
				.on(MATCH_BY_ADVERTISEMENT.JOB_ADVERTISEMENT_ID.eq(JOBADVERTISEMENT.ID))
			.where(MATCH_BY_ADVERTISEMENT.JOB_ADVERTISEMENT_ID.in(advertismentIdList))
			.fetchInto(ApplicantDto.class)
				// @formatter:on
		;
	}

}
