package hu.hollvale.jobbing.repository.impl;

import java.util.List;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;

import hu.hollvale.jobbing.dto.company.EmployeeDataDto;
import hu.hollvale.jobbing.jooq.tables.TCompany;
import hu.hollvale.jobbing.jooq.tables.TCompanyEmployee;
import hu.hollvale.jobbing.jooq.tables.TRole;
import hu.hollvale.jobbing.jooq.tables.TUser;
import hu.hollvale.jobbing.jooq.tables.UserXRole;
import hu.hollvale.jobbing.repository.CompanyEmployeeCustomRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CompanyEmployeeCustomRepositoryImpl implements CompanyEmployeeCustomRepository {

	private final DSLContext dslContext;
	private final TUser USER = TUser.T_USER;
	private final TRole ROLE = TRole.T_ROLE;
	private final UserXRole USER_X_ROLE = UserXRole.USER_X_ROLE;
	private final TCompanyEmployee COMPANY_EMPLOYEE = TCompanyEmployee.T_COMPANY_EMPLOYEE;
	private final TCompany COMPANY = TCompany.T_COMPANY;

	@Override
	public Long getCompanyId(String username) {

		Condition condition = DSL.trueCondition();

		//// @formatter:off
		return dslContext
				.select(COMPANY_EMPLOYEE.COMPANY_ID)
				.from(COMPANY_EMPLOYEE)
				.join(USER)
					.on(COMPANY_EMPLOYEE.USER_ID.eq(USER.ID))
				.where(USER.USERNAME.eq(username)).fetchOne().into(Long.class);
		
				
		// @formatter:on

	}

	@Override
	public List<EmployeeDataDto> getcompanyEmployeesData(Long companyId) {

		Condition condition = DSL.trueCondition();

		List<EmployeeDataDto> companyEmpoyeesData = dslContext
		//// @formatter:off
				
				.select(USER.ID.as("id"),
						USER.NAME.as("name"),
						USER.USERNAME.as("username"),
						USER.EMAIL.as("email"),
						ROLE.NAME.as("roleName")
						)
				.from(USER)
				.join(USER_X_ROLE)
					.on(USER.ID.eq(USER_X_ROLE.USER_ID))
				.join(ROLE)
					.on(ROLE.ID.eq(USER_X_ROLE.ROLE_ID))
				.join(COMPANY_EMPLOYEE)
					.on(USER.ID.eq(COMPANY_EMPLOYEE.USER_ID))
				.where(COMPANY_EMPLOYEE.COMPANY_ID.eq(companyId))
				.fetchInto(EmployeeDataDto.class);
		 
		
		
			// @formatter:on

		return companyEmpoyeesData;
	}

}
