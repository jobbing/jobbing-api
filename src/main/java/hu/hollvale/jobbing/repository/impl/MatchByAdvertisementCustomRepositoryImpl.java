package hu.hollvale.jobbing.repository.impl;

import java.util.List;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import hu.hollvale.jobbing.dto.filter.JobAdvertisementsFilter;
import hu.hollvale.jobbing.jooq.tables.JobAdvertisementXCity;
import hu.hollvale.jobbing.jooq.tables.JobAdvertisementXWorkCategory;
import hu.hollvale.jobbing.jooq.tables.JobAdvertisementXWorkType;
import hu.hollvale.jobbing.jooq.tables.TJobAdvertisement;
import hu.hollvale.jobbing.jooq.tables.TMatchByAdvertisement;
import hu.hollvale.jobbing.repository.MatchByAdvertisementCustomRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MatchByAdvertisementCustomRepositoryImpl implements MatchByAdvertisementCustomRepository {

	private final DSLContext dslContext;
	private final TJobAdvertisement JOBADVERTISEMENT = TJobAdvertisement.T_JOB_ADVERTISEMENT;
	private final JobAdvertisementXCity JOBADVERTISEMENT_X_CITY = JobAdvertisementXCity.JOB_ADVERTISEMENT_X_CITY;
	private final JobAdvertisementXWorkCategory JOBADVERTISEMENT_X_WORKCATEGORY = JobAdvertisementXWorkCategory.JOB_ADVERTISEMENT_X_WORK_CATEGORY;
	private final JobAdvertisementXWorkType JOBADVERTISEMENT_X_WORKTYPE = JobAdvertisementXWorkType.JOB_ADVERTISEMENT_X_WORK_TYPE;
	private final TMatchByAdvertisement MATCH_BY_ADVERTISEMENT = TMatchByAdvertisement.T_MATCH_BY_ADVERTISEMENT;

	@Override
	public Page<Long> filterMatchAdvertisements(JobAdvertisementsFilter jobAdvertisementsFilter, Pageable pageable) {

		//// @formatter:off
		
		List<Long> advertisementIdList = 
			dslContext
				.select(JOBADVERTISEMENT.ID)
				.from(JOBADVERTISEMENT)
				.where(this.createJobAdvertisementCondition(jobAdvertisementsFilter))
				.fetch(JOBADVERTISEMENT.ID);
				
		// @formatter:on

		PageImpl<Long> resultPage = new PageImpl<>(advertisementIdList, pageable, advertisementIdList.size());

		return resultPage;
	}

	private Condition createJobAdvertisementCondition(JobAdvertisementsFilter jobAdvertisementsFilter) {
		Condition condition = DSL.trueCondition();

		condition = this.createWorkTypeCondition(condition, jobAdvertisementsFilter)
				.and(this.createWorkCategoryCondition(condition, jobAdvertisementsFilter))
				.and(this.createCityCondition(condition, jobAdvertisementsFilter))
				.and(this.createSeenCondition(condition, jobAdvertisementsFilter));

		return condition;
	}

	private Condition createSeenCondition(Condition condition, JobAdvertisementsFilter jobAdvertisementsFilter) {

		//// @formatter:off
				condition = 
					condition
						.and(this.dslContext
							.selectCount()
							.from(MATCH_BY_ADVERTISEMENT)
							.where(DSL.trueCondition()
								.and(MATCH_BY_ADVERTISEMENT.JOB_ADVERTISEMENT_ID.eq(JOBADVERTISEMENT.ID))
								.and(MATCH_BY_ADVERTISEMENT.JOBSEEKER_ID.eq(jobAdvertisementsFilter.getJobseekerId())))
							.asField()
									.eq(0));
			// @formatter:on
		return condition;
	}

	private Condition createCityCondition(Condition condition, JobAdvertisementsFilter jobAdvertisementsFilter) {

		if (jobAdvertisementsFilter.getCityIdList() != null && jobAdvertisementsFilter.getCityIdList().size() > 0) {

			//// @formatter:off
			condition = 
				condition
					.and(this.dslContext
							.selectCount()
							.from(JOBADVERTISEMENT_X_CITY)
							.where(DSL.trueCondition()
									.and(JOBADVERTISEMENT_X_CITY.JOB_ADVERTISEMENT_ID.eq(JOBADVERTISEMENT.ID))
									.and(JOBADVERTISEMENT_X_CITY.CITY_ID.in(jobAdvertisementsFilter.getCityIdList())))
							.asField()
									.gt(0));
			// @formatter:on
		}

		return condition;
	}

	private Condition createWorkCategoryCondition(Condition condition,
			JobAdvertisementsFilter jobAdvertisementsFilter) {

		if (jobAdvertisementsFilter.getWorkCategoryIdList() != null
				&& jobAdvertisementsFilter.getWorkCategoryIdList().size() > 0) {

			//// @formatter:off
			condition = 
				condition
					.and(this.dslContext
							.selectCount()
							.from(JOBADVERTISEMENT_X_WORKCATEGORY)
							.where(DSL.trueCondition()
									.and(JOBADVERTISEMENT_X_WORKCATEGORY.JOB_ADVERTISEMENT_ID.eq(JOBADVERTISEMENT.ID))
									.and(JOBADVERTISEMENT_X_WORKCATEGORY.WORK_CATEGORY_ID.in(jobAdvertisementsFilter.getWorkCategoryIdList())))
							.asField()
								.gt(0));
			// @formatter:on
		}

		return condition;
	}

	private Condition createWorkTypeCondition(Condition condition, JobAdvertisementsFilter jobAdvertisementsFilter) {

		if (jobAdvertisementsFilter.getWorkTypeIdList() != null
				&& jobAdvertisementsFilter.getWorkTypeIdList().size() > 0) {

			//// @formatter:off
		condition = 
				condition
					.and(this.dslContext
							.selectCount()
							.from(JOBADVERTISEMENT_X_WORKTYPE)
							.where(DSL.trueCondition()
									.and(JOBADVERTISEMENT_X_WORKTYPE.JOB_ADVERTISEMENT_ID.eq(JOBADVERTISEMENT.ID))
									.and(JOBADVERTISEMENT_X_WORKTYPE.WORK_TYPE_ID.in(jobAdvertisementsFilter.getWorkTypeIdList())))
							.asField()
									.gt(0));
			// @formatter:on
		}

		return condition;
	}

}
