package hu.hollvale.jobbing.repository.impl;

import java.util.List;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import hu.hollvale.jobbing.dto.filter.JobseekerFilter;
import hu.hollvale.jobbing.jooq.tables.JobseekerXDrivingLicence;
import hu.hollvale.jobbing.jooq.tables.JobseekerXLanguageExam;
import hu.hollvale.jobbing.jooq.tables.TDrivingLicence;
import hu.hollvale.jobbing.jooq.tables.TJobseeker;
import hu.hollvale.jobbing.jooq.tables.TLanguageExam;
import hu.hollvale.jobbing.jooq.tables.TMatch;
import hu.hollvale.jobbing.jooq.tables.TQuality;
import hu.hollvale.jobbing.repository.MatchCustomRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MatchCustomRepositoryImpl implements MatchCustomRepository {

	private final DSLContext dslContext;
	private final TJobseeker JOBSEEKER = TJobseeker.T_JOBSEEKER;
	private final TLanguageExam LANGUAGE_EXAM = TLanguageExam.T_LANGUAGE_EXAM;
	private final JobseekerXLanguageExam JOBSEEKER_X_LANGUAGE_EXAM = JobseekerXLanguageExam.JOBSEEKER_X_LANGUAGE_EXAM;
	private final TDrivingLicence DRIVING_LICENCE = TDrivingLicence.T_DRIVING_LICENCE;
	private final JobseekerXDrivingLicence JOBSEEKER_X_DRIVING_LICENCE = JobseekerXDrivingLicence.JOBSEEKER_X_DRIVING_LICENCE;
	private final TQuality QUALITY = TQuality.T_QUALITY;
	private final TMatch MATCH = TMatch.T_MATCH;

	@Override
	public Page<Long> filterMatchJobseekers(JobseekerFilter jobseekerFilter, Pageable pageable) {

		Condition condition = DSL.trueCondition();
	//// @formatter:off
		
			List<Long> jobseekerIdList = 
				dslContext
				.select(JOBSEEKER.ID)
				.from(JOBSEEKER)
				.leftJoin(JOBSEEKER_X_LANGUAGE_EXAM)
					.on(JOBSEEKER.ID.eq(JOBSEEKER_X_LANGUAGE_EXAM.JOBSEEKER_ID))
				.leftJoin(LANGUAGE_EXAM)
					.on(LANGUAGE_EXAM.ID.eq(JOBSEEKER_X_LANGUAGE_EXAM.LANGUAGE_EXAM_ID))
				.leftJoin(JOBSEEKER_X_DRIVING_LICENCE)
					.on(JOBSEEKER.ID.eq(JOBSEEKER_X_DRIVING_LICENCE.JOBSEEKER_ID))
				.leftJoin(DRIVING_LICENCE)
					.on(DRIVING_LICENCE.ID.eq(JOBSEEKER_X_DRIVING_LICENCE.DRIVING_LICENCE_ID))
				.leftJoin(QUALITY)
					.on(JOBSEEKER.ID.eq(QUALITY.JOBSEEKER_ID))
					.where(createAgeCondition(condition, jobseekerFilter)
						.and(createLanguageExamCondition(condition, jobseekerFilter)
						.and(createDrivingLicenceCondition(condition, jobseekerFilter)
						.and(createQualityCondition(condition, jobseekerFilter)
						.and(createSeenCondition(condition, jobseekerFilter))))))
					.fetch(JOBSEEKER.ID);
					
		
		PageImpl<Long> resultPage = new PageImpl<>(jobseekerIdList, pageable, jobseekerIdList.size());

		return resultPage;

	}

	private Condition createSeenCondition(Condition condition, JobseekerFilter jobseekerFilter) {

		//// @formatter:off
				condition = 
					condition
						.and(this.dslContext
							.selectCount()
							.from(MATCH)
							.where(DSL.trueCondition()
								.and(MATCH.JOBSEEKER_ID.eq(JOBSEEKER.ID))
								.and(MATCH.COMPANY_ID.eq(jobseekerFilter.getCompanyId())))
							.asField()
									.eq(0));
			// @formatter:on
		return condition;
	}

	private Condition createAgeCondition(Condition condition, JobseekerFilter jobseekerFilter) {

		if (jobseekerFilter.getYoungerThan() != null && jobseekerFilter.getOlderThan() != null) {

			condition = condition.and(JOBSEEKER.BIRTH_TIME.lt(jobseekerFilter.getOlderThan()))
					.and(JOBSEEKER.BIRTH_TIME.gt(jobseekerFilter.getYoungerThan()));
		}
		return condition;
	}

	private Condition createLanguageExamCondition(Condition condition, JobseekerFilter jobseekerFilter) {

		if (jobseekerFilter.getLanguage() != null && jobseekerFilter.getLevel() != null) {

		//// @formatter:off
 
			condition = 
				condition
					.and(LANGUAGE_EXAM.LANGUAGE
						.eq(jobseekerFilter.getLanguage().toString())
					.and(LANGUAGE_EXAM.LEVEL
						.eq(jobseekerFilter.getLevel().toString())));

			// @formatter:on
		}
		return condition;
	}

	private Condition createDrivingLicenceCondition(Condition condition, JobseekerFilter jobseekerFilter) {

		if (jobseekerFilter.getDrivingLicence() != null) {

		//// @formatter:off
 
			condition = condition
					.and(DRIVING_LICENCE.VALUE
						.eq(jobseekerFilter.getDrivingLicence().toString()));

			// @formatter:on
		}
		return condition;
	}

	private Condition createQualityCondition(Condition condition, JobseekerFilter jobseekerFilter) {

		if (jobseekerFilter.getQuality() != null) {

		//// @formatter:off
 
			condition = condition
					.and(QUALITY.TYPE
						.eq(jobseekerFilter.getQuality().toString()));

			// @formatter:on
		}
		return condition;
	}

}
