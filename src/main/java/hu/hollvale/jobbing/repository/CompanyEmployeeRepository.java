package hu.hollvale.jobbing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hu.hollvale.jobbing.model.company.Company;
import hu.hollvale.jobbing.model.company.CompanyEmployee;

@Repository
public interface CompanyEmployeeRepository
		extends JpaRepository<CompanyEmployee, Long>, CompanyEmployeeCustomRepository {

	//// @formatter:off
		@Query("""
				SELECT 
					e.company
				FROM 
					CompanyEmployee e
				WHERE
					e.user.username LIKE :#{#filter}
				""")
		// @formatter:on

	Company findCompanyByUser(@Param("filter") String username);

	//// @formatter:off
			@Query("""
					SELECT 
						e
					FROM 
						CompanyEmployee e
					WHERE
						e.user.username LIKE :#{#filter}
					""")
			// @formatter:on

	CompanyEmployee findCompanyEmployeeByUsername(@Param("filter") String username);
}
