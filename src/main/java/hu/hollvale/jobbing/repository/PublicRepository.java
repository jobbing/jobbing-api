package hu.hollvale.jobbing.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hu.hollvale.jobbing.model.user.User;

@Repository
public interface PublicRepository extends PagingAndSortingRepository<User, Long> {

}
