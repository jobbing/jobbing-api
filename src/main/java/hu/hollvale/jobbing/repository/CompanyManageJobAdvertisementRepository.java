package hu.hollvale.jobbing.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.hollvale.jobbing.model.company.JobAdvertisement;

public interface CompanyManageJobAdvertisementRepository
		extends JpaRepository<JobAdvertisement, Long>, CompanyManageJobAdvertisementsCustomRepository {

}
