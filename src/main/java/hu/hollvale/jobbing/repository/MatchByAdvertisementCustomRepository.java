package hu.hollvale.jobbing.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import hu.hollvale.jobbing.dto.filter.JobAdvertisementsFilter;

public interface MatchByAdvertisementCustomRepository {

	public Page<Long> filterMatchAdvertisements(JobAdvertisementsFilter jobAdvertisementsFilter, Pageable pageable);

}
