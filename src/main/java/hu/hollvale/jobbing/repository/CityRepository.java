package hu.hollvale.jobbing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.hollvale.jobbing.model.company.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {

}
