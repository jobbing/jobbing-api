package hu.hollvale.jobbing.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hu.hollvale.jobbing.model.user.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

	@Query("""
			SELECT
				r
			FROM
				Role r
			JOIN
				User u
			ON
				u.id = r.id
			WHERE
				:#{#filter} LIKE u.username
			""")
	public List<Role> getRole(@Param("filter") String username);

	public Role findByName(String roleName);

	@Query("""
			SELECT
				u.roles
			FROM
				User u

			WHERE
				:#{#filter} LIKE u.username
			""")

	public List<Role> findByUsername(@Param("filter") String username);

}
