package hu.hollvale.jobbing.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hu.hollvale.jobbing.enums.DrivingLicenceType;
import hu.hollvale.jobbing.model.jobseeker.DrivingLicence;

@Repository
public interface DrivingLicenceRepository extends JpaRepository<DrivingLicence, Long> {

	//ide kell a Query rész?
	@Query("""
			SELECT
				d
			FROM
				DrivingLicence d
			WHERE
				d.value LIKE :#{#filter}
			""")
	Optional<DrivingLicence> findByValue(@Param("filter") DrivingLicenceType value);

}
