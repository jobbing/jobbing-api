package hu.hollvale.jobbing.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import hu.hollvale.jobbing.dto.company.JobseekerDto;
import hu.hollvale.jobbing.dto.match.InterestedCompanyListDTO;
import hu.hollvale.jobbing.model.company.Company;
import hu.hollvale.jobbing.model.jobseeker.Jobseeker;
import hu.hollvale.jobbing.model.match.Match;

public interface MatchRepository extends JpaRepository<Match, Long>, MatchCustomRepository {

	@Query("""
			SELECT
				m
			FROM
				Match m
			WHERE
				m.jobseeker = :#{#filter.jobseeker}
			AND
				m.company = :#{#filter.company}
			AND
				m.state = 'COMPANY_ACCEPT'
			""")
	Optional<Match> jobseekerCompany(@Param("filter") Match match);

	@Query("""
			SELECT
				m
			FROM
				Match m
			WHERE
				m.jobseeker = :#{#jobseeker}
			AND
				m.company = :#{#company}
			""")
	Optional<Match> companyJobseekeer(@Param("company") Company company, @Param("jobseeker") Jobseeker jobseeker);

	@Query("""
			SELECT
				m.company.id as id,
				m.company.name as name,
				m.company.logo as logo
			FROM
				Match m
			WHERE
				m.jobseeker.id = :#{#filter}
			AND
				m.state = 'COMPANY_ACCEPT'

			""")
	List<InterestedCompanyListDTO> interestedCompanies(@Param("filter") Long jobseekerId);

	//// @formatter:off

	@Query("""
			SELECT
				j.id as id,
				j.user.name as name,
				j.picture as picture,
				j.introduction as introduction
			FROM
				Jobseeker j
			WHERE
				j.id IN :#{#filter}
			""")
	
// @formatter:on
	Page<JobseekerDto> getJobseekersByIdList(@Param("filter") List<Long> jobseekerIdList, Pageable pageable);

	@Query("""
			SELECT
				m.jobseeker.id as id,
				m.jobseeker.user.name as name,
				m.jobseeker.picture as picture,
				m.jobseeker.introduction as introduction
			FROM
				Match m
			WHERE
				m.company.id = :#{#filter}
			AND
				m.state LIKE 'MATCH'
			""")

	// @formatter:on
	List<JobseekerDto> getMatchedJobseekers(@Param("filter") Long companyId);
}
