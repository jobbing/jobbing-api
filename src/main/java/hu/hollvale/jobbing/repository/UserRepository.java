package hu.hollvale.jobbing.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hu.hollvale.jobbing.dto.user.JobseekerSettingsDto;
import hu.hollvale.jobbing.model.user.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query("SELECT count(u) > 0 FROM User u WHERE :#{#filter} = u.username")
	public Boolean usernameExist(@Param("filter") String username);

	@Query("SELECT u.email as email FROM User u WHERE :#{#filter} = u.email")
	public Optional<String> userEmailExist(@Param("filter") String email);

	public Optional<User> findByUsername(String username);

	//// @formatter:off
 
	@Query("""
			SELECT 
				u.name as name,
				u.username as username,
				u.email as email
			FROM
				User u
			WHERE 
				:#{#filter} = u.username
			""")
	
// @formatter:on
	public Optional<JobseekerSettingsDto> getSettingsDataByUsername(@Param("filter") String username);
}
