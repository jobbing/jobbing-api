package hu.hollvale.jobbing.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hu.hollvale.jobbing.dto.JobAdvertisementListDto;
import hu.hollvale.jobbing.dto.filter.AdvertisementStateFilter;
import hu.hollvale.jobbing.dto.match.ApplicantDto;
import hu.hollvale.jobbing.model.company.City;
import hu.hollvale.jobbing.model.company.JobAdvertisement;
import hu.hollvale.jobbing.model.company.WorkCategory;
import hu.hollvale.jobbing.model.company.WorkType;

@Repository
public interface JobAdvertisementRepository extends PagingAndSortingRepository<JobAdvertisement, Long> {

	@Query("""
			SELECT
				m.jobAdvertisement.id as id,
				m.jobAdvertisement.name as name,
				m.company.logo as logo,
				m.jobAdvertisement.summary as summary
			FROM
				MatchByAdvertisement m
			WHERE
				m.jobseeker.id = :#{#filter.jobseekerId}
			AND (
				m.state = :#{#filter.matchStateType})

			""")
	List<JobAdvertisementListDto> listJobAdvertisements(
			@Param("filter") AdvertisementStateFilter advertisementStateFilter);

	// @formatter:off
	@Query("""
			SELECT
				m.jobseeker.id as jobseekerId,
				m.jobseeker.user.name as jobseekerName,
				m.state as matchState
			FROM
				MatchByAdvertisement m
			WHERE 
				:#{#filter} = m.jobAdvertisement.id
			AND 
				m.state NOT LIKE 'jobseeker_reject'
			""")
	// @formatter:on
	List<ApplicantDto> getApplicantsByJobAdvertisementId(@Param("filter") Long JobAdvertisementId);

	///// @formatter:off

	@Query("""
			SELECT
				c
			FROM
				City c
			WHERE 
				c.id IN :#{#filter}
			""")
	
// @formatter:on
	List<City> getCitiesByIdList(@Param("filter") List<Long> cityIdList);

	@Query("""
			SELECT
				t
			FROM
				WorkType t
			WHERE
				t.id IN :#{#filter}
			""")

	// @formatter:on
	List<WorkType> getWorkTypesByIdList(@Param("filter") List<Long> workTypeIdList);

	@Query("""
			SELECT
				w
			FROM
				WorkCategory w
			WHERE
				w.id IN :#{#filter}
			""")

	// @formatter:on
	List<WorkCategory> getWorkCategoriesByIdList(@Param("filter") List<Long> workCategoryIdList);

}
