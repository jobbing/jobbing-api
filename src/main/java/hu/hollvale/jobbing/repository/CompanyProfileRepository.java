package hu.hollvale.jobbing.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hu.hollvale.jobbing.dto.company.CompanyJobAdvertisementListDto;
import hu.hollvale.jobbing.dto.company.CompanyProfileDataListDto;
import hu.hollvale.jobbing.model.company.Company;

@Repository
public interface CompanyProfileRepository extends JpaRepository<Company, Long> {

	//// @formatter:off
		@Query("""
				SELECT
					c.id as id,
					c.name as name,
					c.email as email,
					c.logo as logo,
					c.introduction as introduction,
					c.phoneNumber as phoneNumber,
					c.address as address,
					c.contactPerson as contactPerson
				FROM
					Company c
				WHERE
					c.id = :#{#filter}
				""")
	
// @formatter:on

	public CompanyProfileDataListDto loadProfileData(@Param("filter") Long companyId);

	@Query("SELECT c.email as email FROM Company c WHERE :#{#filter} like c.email")
	public Optional<String> companyEmailExist(@Param("filter") String email);

	/// @formatter:off
		@Query("""
			SELECT 
				e.company.id as id
			FROM 
				CompanyEmployee e
			WHERE
				e.user.username = :#{#filter}
				""")
// @formatter:on
	public Optional<Long> userCompanyId(@Param("filter") String username);

	// @formatter:off
		@Query("""
				SELECT
					j.id as id,
					j.name as name,
					j.summary as summary
				FROM
					JobAdvertisement j 
				WHERE 
					:#{#filter} = j.company.id
				""")
// @formatter:on

	public List<CompanyJobAdvertisementListDto> loadCompanyJobAdvertisements(@Param("filter") Long companyId);

}
