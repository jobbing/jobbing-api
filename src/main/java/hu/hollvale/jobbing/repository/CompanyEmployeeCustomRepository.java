package hu.hollvale.jobbing.repository;

import java.util.List;

import hu.hollvale.jobbing.dto.company.EmployeeDataDto;

public interface CompanyEmployeeCustomRepository {

	Long getCompanyId(String username);

	List<EmployeeDataDto> getcompanyEmployeesData(Long companyId);

}
