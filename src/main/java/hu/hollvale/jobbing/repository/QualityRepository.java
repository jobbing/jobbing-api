package hu.hollvale.jobbing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.hollvale.jobbing.model.jobseeker.Quality;

@Repository
public interface QualityRepository extends JpaRepository<Quality, Long> {

}
