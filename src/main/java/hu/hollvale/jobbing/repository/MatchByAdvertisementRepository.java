package hu.hollvale.jobbing.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hu.hollvale.jobbing.dto.company.JobAdvertisementFindDto;
import hu.hollvale.jobbing.model.match.MatchByAdvertisement;

@Repository
public interface MatchByAdvertisementRepository
		extends JpaRepository<MatchByAdvertisement, Long>, MatchByAdvertisementCustomRepository {

	@Query("""
			SELECT
				m
			FROM
				MatchByAdvertisement m
			WHERE
				m.jobseeker = :#{#filter.jobseeker}
			AND
				m.company = :#{#filter.company}
			AND
				m.jobAdvertisement = :#{#filter.jobAdvertisement}
			AND
				m.state = 'JOBSEEKER_ACCEPT'
				""")
	Optional<MatchByAdvertisement> companyAcceptJobseekerByAdvertisement(
			@Param("filter") MatchByAdvertisement matchByAdvertisement);

	@Query("""
			SELECT
				m
			FROM
				MatchByAdvertisement m
			WHERE
				m.jobseeker = :#{#filter.jobseeker}
			AND
				m.company = :#{#filter.company}
			AND
				m.jobAdvertisement = :#{#filter.jobAdvertisement}
			AND (
				m.state = 'JOBSEEKER_ACCEPT'
			OR
				m.state = 'match')
				""")
	Optional<MatchByAdvertisement> companyRejectJobseekerByAdvertisement(
			@Param("filter") MatchByAdvertisement matchByAdvertisement);

	@Query("""
			SELECT
				m
			FROM
				MatchByAdvertisement m
			WHERE
				m.jobseeker = :#{#filter.jobseeker}
			AND
				m.company = :#{#filter.company}
			AND
				m.jobAdvertisement = :#{#filter.jobAdvertisement}
				""")
	Optional<MatchByAdvertisement> jobseekerJobAdvertisement(
			@Param("filter") MatchByAdvertisement matchByAdvertisement);

//// @formatter:off
 
	@Query("""
			SELECT
				a.id as id,
				a.name as name,
				a.company.logo as companyLogo,
				a.company.name as companyName,
				a.place as place,
				a.summary as summary
			FROM
				JobAdvertisement a
			WHERE 
				a.id IN :#{#filter}
			""")
	
	
// @formatter:on
	Page<JobAdvertisementFindDto> getJobAdvertisementsByIdList(@Param("filter") List<Long> filterMatchAdvertisementIds,
			Pageable pageable);

}
