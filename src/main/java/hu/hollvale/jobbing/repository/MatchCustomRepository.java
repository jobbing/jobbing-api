package hu.hollvale.jobbing.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import hu.hollvale.jobbing.dto.filter.JobseekerFilter;

public interface MatchCustomRepository {

	public Page<Long> filterMatchJobseekers(JobseekerFilter jobseekerFilter, Pageable pageable);

}
