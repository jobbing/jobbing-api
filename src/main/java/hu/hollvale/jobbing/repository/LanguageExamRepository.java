package hu.hollvale.jobbing.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hu.hollvale.jobbing.dto.LanguageExamDto;
import hu.hollvale.jobbing.model.jobseeker.LanguageExam;

@Repository
public interface LanguageExamRepository extends JpaRepository<LanguageExam, Long> {

	@Query("""
			SELECT
				l
			FROM
				LanguageExam l
			WHERE
				:#{#filter.language} LIKE l.language
			AND
				:#{#filter.level} LIKE l.level
			""")

	Optional<LanguageExam> findLanguageExam(@Param("filter") LanguageExamDto languageExamDto);

}
