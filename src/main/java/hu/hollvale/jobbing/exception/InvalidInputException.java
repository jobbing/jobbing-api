package hu.hollvale.jobbing.exception;

import hu.hollvale.jobbing.enums.Message;

public class InvalidInputException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private Message messageCode;

	public InvalidInputException(Message message) {
		super(message.toString());
		this.messageCode = message;
	}

	public Message getMessageCode() {
		return this.messageCode;
	}

	public void setMessageCode(Message message) {
		this.messageCode = message;
	}
}
