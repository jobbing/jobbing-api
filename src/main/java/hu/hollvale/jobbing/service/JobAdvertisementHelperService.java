package hu.hollvale.jobbing.service;

import hu.hollvale.jobbing.dto.GetJobAdvertisementDto;
import hu.hollvale.jobbing.dto.JobseekerJobAdvertisementDto;
import hu.hollvale.jobbing.dto.company.GetListValuesDto;
import hu.hollvale.jobbing.model.company.City;
import hu.hollvale.jobbing.model.company.JobAdvertisement;
import hu.hollvale.jobbing.model.company.WorkCategory;
import hu.hollvale.jobbing.model.company.WorkType;

public interface JobAdvertisementHelperService {

	JobAdvertisement loadJobAdvertisementById(Long id);

	WorkCategory loadWorkCategoryById(Long id);

	WorkType loadWorkTypeById(Long id);

	City loadCityById(Long id);

	JobseekerJobAdvertisementDto getJobseekerJobAdvertisement(Long jobAdvertisementId);

	GetJobAdvertisementDto getJobAdvertisement(Long jobAdvertisementId);

	GetListValuesDto getListValues();
}
