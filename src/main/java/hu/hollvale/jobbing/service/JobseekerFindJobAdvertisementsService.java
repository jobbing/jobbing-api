package hu.hollvale.jobbing.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import hu.hollvale.jobbing.dto.company.JobAdvertisementFindDto;
import hu.hollvale.jobbing.dto.filter.JobAdvertisementsFilter;

public interface JobseekerFindJobAdvertisementsService {

	Page<JobAdvertisementFindDto> pageJobAdvertisements(JobAdvertisementsFilter pageJobAdvertisementsDto,
			Pageable pageable);
}
