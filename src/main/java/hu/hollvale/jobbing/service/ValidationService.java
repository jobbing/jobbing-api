package hu.hollvale.jobbing.service;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public interface ValidationService {

	public void checkJobseekerExist(Long jobseekerId);

	public void checkCompanyExist(Long companyId);

	public void checkUsernameNotExist(String username);

	public void checkUsernameExist(String username);

	public void checkUserEmailNotExist(String email);

	public void checkUserHasRole(Long roleId);

	public void checkCorrectDates(Date startDate, Date endDate);

	public void checkUserNotHaveJobseeker(Long userId);

	public void checkFileNotNull(MultipartFile file);

	public void checkFileDeletable(String file);

	public void checkFileTypePdf(MultipartFile file);

	public void checkFileTypeImage(MultipartFile file);

	public void checkEmailUpdateValid(String oldEmail, String newEmail);

	public void checkEmailFormatValid(String email);

	public void checkPasswordValid(String password);

	public void checkInputUsernameValid(String username);

	public void checkInputCompanyValid(Long companyId);

	public void checkInputJobseekerIdValid(Long jobseekerId);

	public void checkInputQualityIdValid(Long qualityId);

	public void checkInputExperienceIdValid(Long experienceId);

	public void checkEmployeeAndAdvertisementInTheSameCompany(Long jobAdvertisementId);

}
