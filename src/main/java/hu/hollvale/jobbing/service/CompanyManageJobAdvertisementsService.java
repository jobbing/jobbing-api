package hu.hollvale.jobbing.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import hu.hollvale.jobbing.dto.match.CompanyManageJobAdvertisementsDto;

public interface CompanyManageJobAdvertisementsService {

	public Page<CompanyManageJobAdvertisementsDto> getJobAdvertisementWithAppliciants(Long companyId,
			Pageable pageable);
}
