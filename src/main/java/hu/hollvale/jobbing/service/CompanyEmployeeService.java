package hu.hollvale.jobbing.service;

import java.util.List;

import hu.hollvale.jobbing.dto.company.EmployeeDataDto;
import hu.hollvale.jobbing.dto.company.EmployeeRegisterDto;

public interface CompanyEmployeeService {

	public List<EmployeeDataDto> getCompanyEmployees(Long companyId);

	Long registerEmployee(EmployeeRegisterDto employeeRegisterDto);

}
