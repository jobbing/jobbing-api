package hu.hollvale.jobbing.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import hu.hollvale.jobbing.dto.DeleteDrivingLicenceDto;
import hu.hollvale.jobbing.dto.DeleteLanguageExamDto;
import hu.hollvale.jobbing.dto.DrivingLicenceDto;
import hu.hollvale.jobbing.dto.ExperienceDto;
import hu.hollvale.jobbing.dto.IntroductionUpdateDto;
import hu.hollvale.jobbing.dto.LanguageExamDto;
import hu.hollvale.jobbing.dto.PersonalDataUpdateDto;
import hu.hollvale.jobbing.dto.QualityDto;
import hu.hollvale.jobbing.dto.WorkILookForUpdateDto;

public interface JobseekerProfileService {

	public void updateIntroduction(IntroductionUpdateDto introductionUpdateDto);

	public void updateWorkILookFor(WorkILookForUpdateDto workILookForUpdateDto);

	public void updatePersonalDatas(PersonalDataUpdateDto personalDataUpdateDto);

	public void deleteDrivingLicence(DeleteDrivingLicenceDto deleteDrivingLicenceDto);

	public Long deleteLanguageExam(DeleteLanguageExamDto DeleteLanguageExamDto);

	public Long deleteQualtity(Long qualityId);

	public Long deleteExperience(Long exprienceId);

	public Long saveDrivingLicences(DrivingLicenceDto drivingLicenceDto);

	public Long saveLanguageExam(LanguageExamDto languageExamDto);

	public Long saveQuality(QualityDto qualityDto);

	public Long saveExperience(ExperienceDto experienceDto);

	public String savePicture(String username, MultipartFile file) throws IOException;

	public void deletePicture(Long jobseekerId) throws IOException;

	public String saveCV(String username, MultipartFile file) throws IOException;

	public void deleteCV(Long jobseekerId) throws IOException;

}
