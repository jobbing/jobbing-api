package hu.hollvale.jobbing.service;

import hu.hollvale.jobbing.dto.match.JobseekerMatchDto;

public interface MatchByAdvertisementService {

	public void jobseekerRejectJobAdvertisement(JobseekerMatchDto jobseekerMatchDto);

	public void jobseekerAcceptJobAdvertisement(JobseekerMatchDto jobseekerMatchDto);

	public void companyRejectJobseekerByAdvertisement(JobseekerMatchDto jobseekerMatchDto);

	public void companyAcceptJobseekerByAdvertisement(JobseekerMatchDto jobseekerMatchDto);
}
