package hu.hollvale.jobbing.service;

import hu.hollvale.jobbing.dto.company.JobAdvertisementDto;

public interface JobAdvertisementService {

	Long saveJobAdvertisement(JobAdvertisementDto jobAdvertisementDto);

	void deleteJobAdvertisement(Long jobAdevrtisementId);

}
