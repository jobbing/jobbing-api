package hu.hollvale.jobbing.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import hu.hollvale.jobbing.dto.company.CompanyIntroductionUpdateDto;
import hu.hollvale.jobbing.dto.company.CompanyPersonalDataUpdateDto;

public interface CompanyProfileService {

	void updateIntroduction(CompanyIntroductionUpdateDto companyIntroductionUpdateDto);

	Long updatePersonalData(CompanyPersonalDataUpdateDto companyPersonalDataUpdateDto);

	String saveLogo(Long companyId, MultipartFile file) throws IOException;

	void deleteLogo(Long companyId) throws IOException;

	void deleteCompany(Long companyId);

}
