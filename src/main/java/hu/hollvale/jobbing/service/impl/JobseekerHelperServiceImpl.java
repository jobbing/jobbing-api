package hu.hollvale.jobbing.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.dto.DrivingLicenceListDto;
import hu.hollvale.jobbing.dto.ExperienceListDto;
import hu.hollvale.jobbing.dto.LanguageExamListDto;
import hu.hollvale.jobbing.dto.ProfileDataListDto;
import hu.hollvale.jobbing.dto.QualityListDto;
import hu.hollvale.jobbing.enums.Message;
import hu.hollvale.jobbing.exception.NotFoundException;
import hu.hollvale.jobbing.model.jobseeker.DrivingLicence;
import hu.hollvale.jobbing.model.jobseeker.Experience;
import hu.hollvale.jobbing.model.jobseeker.Jobseeker;
import hu.hollvale.jobbing.model.jobseeker.LanguageExam;
import hu.hollvale.jobbing.model.jobseeker.Quality;
import hu.hollvale.jobbing.model.user.User;
import hu.hollvale.jobbing.repository.DrivingLicenceRepository;
import hu.hollvale.jobbing.repository.ExperienceRepository;
import hu.hollvale.jobbing.repository.JobseekerProfileRepository;
import hu.hollvale.jobbing.repository.LanguageExamRepository;
import hu.hollvale.jobbing.repository.QualityRepository;
import hu.hollvale.jobbing.service.JobseekerHelperService;
import hu.hollvale.jobbing.service.UserService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class JobseekerHelperServiceImpl implements JobseekerHelperService {

	private final DrivingLicenceRepository drivingLicenceRepository;
	private final LanguageExamRepository languageExamRepository;
	private final QualityRepository qualityRepository;
	private final ExperienceRepository experienceRepository;
	private final ValidationService validationService;
	private final JobseekerProfileRepository jobseekerProfileRepository;
	private final UserService userService;

	@Override
	public Jobseeker loadJobseekerById(Long id) {
		return this.jobseekerProfileRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(Message.JOBSEEKER_NOT_FOUND));

	}

	@Override
	public DrivingLicence loadDrivingLicenceById(Long id) {
		return this.drivingLicenceRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(Message.DRIVING_LICENCE_NOT_FOUND));

	}

	@Override
	public LanguageExam loadLanguageExamById(Long id) {

		return this.languageExamRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(Message.LANGUAGE_EXAM_NOT_FOUND));
	}

	@Override
	public Experience loadExperienceById(Long id) {
		return this.experienceRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(Message.EXPERIENCE_NOT_FOUND));

	}

	@Override
	public Quality loadQualityById(Long id) {

		return this.qualityRepository.findById(id).orElseThrow(() -> new NotFoundException(Message.QUALITY_NOT_FOUND));
	}

	@Override
	public ProfileDataListDto loadProfileData(String username) {

		this.validationService.checkUsernameExist(username);
		return this.jobseekerProfileRepository.loadProfileData(username);

	}

	@Override
	public ProfileDataListDto companyLoadJobseekerProfileData(Long jobseekerId) {
		Jobseeker jobseeker = this.loadJobseekerById(jobseekerId);

		return this.loadProfileData(jobseeker.getUser().getUsername());
	}

	@Override
	public List<DrivingLicenceListDto> getDrivingLicences(Long jobseekerId) {
		this.validationService.checkJobseekerExist(jobseekerId);
		return this.jobseekerProfileRepository.getDrivingLicences(jobseekerId);
	}

	@Override
	public List<LanguageExamListDto> getLanguageExams(Long jobseekerId) {
		this.validationService.checkJobseekerExist(jobseekerId);
		return this.jobseekerProfileRepository.getLanguageExams(jobseekerId);
	}

	@Override
	public List<QualityListDto> getQualities(Long jobseekerId) {
		this.validationService.checkJobseekerExist(jobseekerId);
		return this.jobseekerProfileRepository.getQualities(jobseekerId);
	}

	@Override
	public List<ExperienceListDto> getExperiences(Long jobseekerId) {
		this.validationService.checkJobseekerExist(jobseekerId);
		return this.jobseekerProfileRepository.getExperiences(jobseekerId);
	}

	@Override
	public Long createJobseeker(Long userId) {
		User user = this.userService.loadUserById(userId);

		Jobseeker jobseeker = new Jobseeker();
		jobseeker.setUser(user);

		this.validationService.checkUserNotHaveJobseeker(userId);

		return this.jobseekerProfileRepository.save(jobseeker).getId();

	}
}
