package hu.hollvale.jobbing.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.dto.JobAdvertisementListDto;
import hu.hollvale.jobbing.dto.filter.AdvertisementStateFilter;
import hu.hollvale.jobbing.dto.match.InterestedCompanyListDTO;
import hu.hollvale.jobbing.repository.JobAdvertisementRepository;
import hu.hollvale.jobbing.repository.MatchRepository;
import hu.hollvale.jobbing.service.JobseekerMyJobAdvertisementsService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS)")
public class JobseekerMyJobAdvertisementsServiceImpl implements JobseekerMyJobAdvertisementsService {

	private final JobAdvertisementRepository jobAdvertisementRepository;
	private final MatchRepository matchRepository;
	private final ValidationService validationService;

	@Override
	public List<JobAdvertisementListDto> listJobAdvertisements(AdvertisementStateFilter advertisementStateFilter) {
		this.validationService.checkJobseekerExist(advertisementStateFilter.getJobseekerId());
		this.validationService.checkInputJobseekerIdValid(advertisementStateFilter.getJobseekerId());

		return this.jobAdvertisementRepository.listJobAdvertisements(advertisementStateFilter);
	}

	@Override
	public List<InterestedCompanyListDTO> interestedCompanies(Long jobseekerId) {
		this.validationService.checkJobseekerExist(jobseekerId);
		this.validationService.checkInputJobseekerIdValid(jobseekerId);

		return this.matchRepository.interestedCompanies(jobseekerId);
	}

}
