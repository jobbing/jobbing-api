package hu.hollvale.jobbing.service.impl;

import java.io.IOException;

import javax.transaction.Transactional;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import hu.hollvale.jobbing.dto.company.CompanyIntroductionUpdateDto;
import hu.hollvale.jobbing.dto.company.CompanyPersonalDataUpdateDto;
import hu.hollvale.jobbing.enums.Message;
import hu.hollvale.jobbing.exception.InvalidInputException;
import hu.hollvale.jobbing.model.company.Company;
import hu.hollvale.jobbing.repository.CompanyProfileRepository;
import hu.hollvale.jobbing.service.CompanyHelperService;
import hu.hollvale.jobbing.service.CompanyProfileService;
import hu.hollvale.jobbing.service.StorageService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_MODIFY_COMPANY_PROFILE)")
public class CompanyProfileServiceImpl implements CompanyProfileService {

	private final StorageService storageService;
	private final CompanyHelperService companyHelperService;
	private final CompanyProfileRepository companyProfileRepository;
	private final ValidationService validationService;

	@Override
	public void updateIntroduction(CompanyIntroductionUpdateDto companyIntroductionUpdateDto) {
		this.validationService.checkInputCompanyValid(companyIntroductionUpdateDto.getCompanyId());

		Company company = companyHelperService.loadCompanyById(companyIntroductionUpdateDto.getCompanyId());
		company.setIntroduction(companyIntroductionUpdateDto.getIntroduction());
		this.companyProfileRepository.save(company);
	}

	@Override
	public Long updatePersonalData(CompanyPersonalDataUpdateDto companyPersonalDataUpdateDto) {
		this.validationService.checkInputCompanyValid(companyPersonalDataUpdateDto.getCompanyId());

		Company company = companyHelperService.loadCompanyById(companyPersonalDataUpdateDto.getCompanyId());
		this.validationService.checkEmailUpdateValid(company.getEmail(), companyPersonalDataUpdateDto.getEmail());
		company.setEmail(companyPersonalDataUpdateDto.getEmail());
		company.setAddress(companyPersonalDataUpdateDto.getAddress());
		company.setContactPerson(companyPersonalDataUpdateDto.getContactPerson());
		company.setPhoneNumber(companyPersonalDataUpdateDto.getPhoneNumber());

		this.companyProfileRepository.save(company);
		return companyPersonalDataUpdateDto.getCompanyId();
	}

	@Override
	public String saveLogo(Long companyId, MultipartFile file) throws IOException {
		this.validationService.checkInputCompanyValid(companyId);
		this.validationService.checkFileTypeImage(file);

		Company company = companyHelperService.loadCompanyById(companyId);

		String fileBasePath = this.buildCompanyBasePath();
		String sPath = company.getLogo();
		String filename = this.storageService.saveFile(fileBasePath, file, companyId.toString());
		company.setLogo(filename);
		this.companyProfileRepository.save(company);

		if (sPath != null) {
			this.archiveCompany(fileBasePath, sPath);
		}

		return filename;

	}

	@Override
	public void deleteLogo(Long companyId) throws IOException {
		this.validationService.checkInputCompanyValid(companyId);
		Company company = companyHelperService.loadCompanyById(companyId);

		String filename = company.getLogo();

		this.validationService.checkFileDeletable(filename);

		String fileBasePath = this.buildCompanyBasePath();
		this.archiveCompany(fileBasePath, filename);
		company.setLogo(null);
		this.companyProfileRepository.save(company);
	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_DELETE_COMPANY)")
	public void deleteCompany(Long companyId) {
		this.validationService.checkInputCompanyValid(companyId);

		Company company = this.companyHelperService.loadCompanyById(companyId);
		if (company.getEmployees().size() > 1 || company.getJobAdvertisements().size() > 0) {
			throw new InvalidInputException(Message.COMPANY_NOT_DELETABLE);
		} else {
			this.companyProfileRepository.delete(company);
		}

	}

	private void archiveFile(String fileBasePath, String newFileBasePath, String sPath) throws IOException {
		this.storageService.archiveFile(fileBasePath, newFileBasePath, sPath);
	}

	private void archiveCompany(String fileBasePath, String sPath) throws IOException {

		String newFileBasePath = this.buildDeletedCompanyBasePath();
		this.archiveFile(fileBasePath, newFileBasePath, sPath);
	}

	private String buildCompanyBasePath() {

		// @formatter:off
		return new StringBuilder()
				.append(this.storageService.getRootLocation())
				.append(this.storageService.getPathDelimeter())
				.append(this.storageService.getCompanyFolder())
		.toString();
		// @formatter:on

	}

	private String buildDeletedCompanyBasePath() {

		// @formatter:off
		return new StringBuilder()
				.append(this.storageService.getRootLocation())
				.append(this.storageService.getPathDelimeter())
				.append(this.storageService.getDeletedCompanyFolder())
		.toString();
		// @formatter:on

	}

}
