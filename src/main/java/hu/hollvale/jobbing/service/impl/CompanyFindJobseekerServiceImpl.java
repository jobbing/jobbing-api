package hu.hollvale.jobbing.service.impl;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.dto.company.JobseekerDto;
import hu.hollvale.jobbing.dto.filter.JobseekerFilter;
import hu.hollvale.jobbing.repository.MatchRepository;
import hu.hollvale.jobbing.service.CompanyFindJobseekerService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Transactional
@Service
@RequiredArgsConstructor
public class CompanyFindJobseekerServiceImpl implements CompanyFindJobseekerService {

	private final MatchRepository matchRepository;
	private final ValidationService validationService;

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_SEARCH)")
	public Page<JobseekerDto> pageJobseekers(JobseekerFilter jobseekerFilter, Pageable pageable) {

		this.validationService.checkInputCompanyValid(jobseekerFilter.getCompanyId());

		Page<Long> jobseekerIdPage = this.matchRepository.filterMatchJobseekers(jobseekerFilter, pageable);

		return this.matchRepository.getJobseekersByIdList(jobseekerIdPage.getContent(), pageable);

	}

}
