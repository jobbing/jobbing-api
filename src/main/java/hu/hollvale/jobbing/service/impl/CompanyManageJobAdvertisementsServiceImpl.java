package hu.hollvale.jobbing.service.impl;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.dto.match.CompanyManageJobAdvertisementsDto;
import hu.hollvale.jobbing.repository.CompanyManageJobAdvertisementRepository;
import hu.hollvale.jobbing.service.CompanyManageJobAdvertisementsService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class CompanyManageJobAdvertisementsServiceImpl implements CompanyManageJobAdvertisementsService {

	private final ValidationService validationService;
	private final CompanyManageJobAdvertisementRepository companyManageJobAdvertisementRepository;

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_DECIDE)")
	public Page<CompanyManageJobAdvertisementsDto> getJobAdvertisementWithAppliciants(Long companyId,
			Pageable pageable) {

		this.validationService.checkInputCompanyValid(companyId);

		return this.companyManageJobAdvertisementRepository.getJobAdvertisementWithAppliciants(companyId, pageable);

	}
}
