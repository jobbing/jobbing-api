package hu.hollvale.jobbing.service.impl;

import java.util.stream.Collectors;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.enums.PermissionType;
import hu.hollvale.jobbing.service.SecurityService;

@Service
public class SecurityServiceImpl implements SecurityService {

	@Override
	public boolean hasPermission(PermissionType permission) {
		return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
				.map(a -> a.getAuthority()).collect(Collectors.toList()).contains(permission.name());
	}

	@Override
	public boolean hasAnyPermission(PermissionType... permissions) {
		for (PermissionType permissionType : permissions) {
			if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
					.map(a -> a.getAuthority()).collect(Collectors.toList()).contains(permissionType.name())) {
				return true;
			}
		}
		return false;
	}

}
