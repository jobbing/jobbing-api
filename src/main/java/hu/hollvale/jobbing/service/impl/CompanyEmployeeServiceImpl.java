package hu.hollvale.jobbing.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.dto.company.EmployeeDataDto;
import hu.hollvale.jobbing.dto.company.EmployeeRegisterDto;
import hu.hollvale.jobbing.enums.Message;
import hu.hollvale.jobbing.exception.InvalidInputException;
import hu.hollvale.jobbing.model.company.Company;
import hu.hollvale.jobbing.model.company.CompanyEmployee;
import hu.hollvale.jobbing.model.user.Role;
import hu.hollvale.jobbing.model.user.User;
import hu.hollvale.jobbing.repository.CompanyEmployeeRepository;
import hu.hollvale.jobbing.repository.UserRepository;
import hu.hollvale.jobbing.service.CompanyEmployeeService;
import hu.hollvale.jobbing.service.CompanyHelperService;
import hu.hollvale.jobbing.service.UserService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Transactional
@Service
@RequiredArgsConstructor
public class CompanyEmployeeServiceImpl implements CompanyEmployeeService {

	private final CompanyEmployeeRepository companyEmployeeRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final UserRepository userRepository;
	private final UserService userService;
	private final ValidationService validationService;
	private final CompanyHelperService companyHelperService;

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_LIST_EMPLOYEES)")
	public List<EmployeeDataDto> getCompanyEmployees(Long companyId) {

		this.validationService.checkInputCompanyValid(companyId);

		return this.companyEmployeeRepository.getcompanyEmployeesData(companyId);

	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_CREATE_EMPLOYEE)")
	public Long registerEmployee(EmployeeRegisterDto employeeRegisterDto) {

		this.validationService.checkInputCompanyValid(employeeRegisterDto.getCompanyId());
		this.validationService.checkUsernameNotExist(employeeRegisterDto.getUsername());
		this.validationService.checkUserEmailNotExist(employeeRegisterDto.getEmail());
		this.validationService.checkUserHasRole(employeeRegisterDto.getRoleId());

		Company company = this.companyHelperService.loadCompanyById(employeeRegisterDto.getCompanyId());
		User user = createNewUser(employeeRegisterDto);

		Long givenRoleId = employeeRegisterDto.getRoleId();
		addRolesToNewUser(user, givenRoleId);

		return makeUserToCompanyEmployee(user, company);

	}

	private User createNewUser(EmployeeRegisterDto employeeRegisterDto) {
		employeeRegisterDto.setPassword(this.bCryptPasswordEncoder.encode(employeeRegisterDto.getPassword()));
		User user = User.fromUser(employeeRegisterDto);

		this.userRepository.save(user);

		return user;

	}

	private void addRolesToNewUser(User user, Long givenRoleId) {

		Role role = this.userService.loadRoleById(givenRoleId);
		if (!role.getHolder().equals("company")) { 
			throw new InvalidInputException(Message.ROLE_NOT_ALLOWED);
		}
		List<Role> myRoles = new ArrayList<>();
		myRoles.add(role);

		user.setRoles(myRoles);
		this.userRepository.save(user);
	}

	private Long makeUserToCompanyEmployee(User user, Company company) {
		CompanyEmployee companyEmployee = new CompanyEmployee();
		companyEmployee.setUser(user);
		companyEmployee.setCompany(company);
		return this.companyEmployeeRepository.save(companyEmployee).getId();

	}
}
