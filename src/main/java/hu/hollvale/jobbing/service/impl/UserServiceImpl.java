package hu.hollvale.jobbing.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.dto.user.EmailChangeDto;
import hu.hollvale.jobbing.dto.user.GetMyDatasDto;
import hu.hollvale.jobbing.dto.user.JobseekerSettingsDto;
import hu.hollvale.jobbing.dto.user.NameChangeDto;
import hu.hollvale.jobbing.dto.user.PasswordChangeDto;
import hu.hollvale.jobbing.dto.user.UsernameChangeDto;
import hu.hollvale.jobbing.enums.HolderType;
import hu.hollvale.jobbing.enums.Message;
import hu.hollvale.jobbing.enums.PermissionType;
import hu.hollvale.jobbing.exception.InvalidInputException;
import hu.hollvale.jobbing.exception.NotFoundException;
import hu.hollvale.jobbing.model.company.CompanyEmployee;
import hu.hollvale.jobbing.model.jobseeker.Jobseeker;
import hu.hollvale.jobbing.model.user.Permission;
import hu.hollvale.jobbing.model.user.Role;
import hu.hollvale.jobbing.model.user.User;
import hu.hollvale.jobbing.repository.CompanyEmployeeRepository;
import hu.hollvale.jobbing.repository.JobseekerProfileRepository;
import hu.hollvale.jobbing.repository.RoleRepository;
import hu.hollvale.jobbing.repository.UserRepository;
import hu.hollvale.jobbing.service.SecurityService;
import hu.hollvale.jobbing.service.UserService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService, UserDetailsService {

	private final UserRepository userRepository;
	private final CompanyEmployeeRepository companyEmployeeRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final RoleRepository roleRepository;
	private final JobseekerProfileRepository jobseekerProfileRepository;
	private final ValidationService validationService;
	private final SecurityService securityService;

	@Override
	@PreAuthorize("@securityServiceImpl.hasAnyPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_DELETE_OWN_PROFILE,"
			+ " T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS, "
			+ " T(hu.hollvale.jobbing.enums.PermissionType).CAN_DELETE_EMPLOYEE)")
	public void deleteUser(String username) {

		User user = this.loadUserObjByUsername(username);
		// if Jobseeker
		if (this.securityService.hasPermission(PermissionType.JOBSEEKER_PERMISSIONS)) {
			validationService.checkInputUsernameValid(username);
			Jobseeker jobseeker = this.jobseekerProfileRepository.getByUserId(user.getId());
			this.jobseekerProfileRepository.delete(jobseeker);
			this.userRepository.delete(user);
			// if companyAdmin
		} else if (this.securityService.hasPermission(PermissionType.CAN_DELETE_EMPLOYEE)) {

			if (compareCompanies(username, getSecurityContextName()) && username != getSecurityContextName()) {
				for (Role role : user.getRoles()) {
					if (!role.getDeletable()) {
						throw new InvalidInputException(Message.NOT_DELETABLE);
					}
				}

				CompanyEmployee companyEmployee = this.companyEmployeeRepository
						.findCompanyEmployeeByUsername(username);

				this.companyEmployeeRepository.delete(companyEmployee);
				this.userRepository.delete(user);

			} else {
				throw new InvalidInputException(Message.NOT_DELETABLE);
			}
			// if employee
		} else if (this.securityService.hasPermission(PermissionType.CAN_DELETE_OWN_PROFILE)) {
			if (username == getSecurityContextName()) {
				CompanyEmployee companyEmployee = this.companyEmployeeRepository
						.findCompanyEmployeeByUsername(username);
				this.companyEmployeeRepository.delete(companyEmployee);
				this.userRepository.delete(user);
			} else {
				throw new InvalidInputException(Message.NOT_DELETABLE);
			}

		}

	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasAnyPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_MODIFY_OWN_PROFILE, "
			+ " T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS)")
	public void changeUsername(UsernameChangeDto usernameChangeDto) {
		if (!usernameChangeDto.getUsername().equals(usernameChangeDto.getNewUsername())) {

			this.validationService.checkInputUsernameValid(usernameChangeDto.getUsername());
			this.validationService.checkUsernameNotExist(usernameChangeDto.getNewUsername());

			User user = this.loadUserObjByUsername(usernameChangeDto.getUsername());
			user.setUsername(usernameChangeDto.getNewUsername());
			this.userRepository.save(user);
		}

	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasAnyPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_MODIFY_OWN_PROFILE, "
			+ " T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS)")
	public void changeName(NameChangeDto nameChangeDto) {

		this.validationService.checkInputUsernameValid(nameChangeDto.getUsername());

		User user = this.loadUserObjByUsername(nameChangeDto.getUsername());
		user.setName(nameChangeDto.getNewName());
		this.userRepository.save(user);
	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasAnyPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_MODIFY_OWN_PROFILE, "
			+ " T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS)")
	public void changePassword(PasswordChangeDto passwordChangeDto) {

		this.validationService.checkInputUsernameValid(passwordChangeDto.getUsername());

		User user = this.loadUserObjByUsername(passwordChangeDto.getUsername());
		if (this.bCryptPasswordEncoder.matches(passwordChangeDto.getOldPassword(), user.getPassword())) {
			user.setPassword(this.bCryptPasswordEncoder.encode(passwordChangeDto.getNewPassword()));
		} else {
			throw new InvalidInputException(Message.PASSWORDS_NOT_MATCH);
		}

	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasAnyPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_MODIFY_OWN_PROFILE, "
			+ " T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS)")
	public void changeEmail(EmailChangeDto emailChangeDto) {

		this.validationService.checkInputUsernameValid(emailChangeDto.getUsername());

		User user = this.loadUserObjByUsername(emailChangeDto.getUsername());
		if (emailChangeDto.getNewEmail() != user.getEmail()) {

			this.validationService.checkUserEmailNotExist(emailChangeDto.getNewEmail());

			user.setEmail(emailChangeDto.getNewEmail());
		}

	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasAnyPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_MODIFY_OWN_PROFILE, "
			+ " T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS)")
	public JobseekerSettingsDto getUserDatasToSettings(String username) {

		this.validationService.checkInputUsernameValid(username);

		return this.userRepository.getSettingsDataByUsername(username)
				.orElseThrow(() -> new NotFoundException(Message.USER_NOT_FOUND));
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = loadUserObjByUsername(username);

		Collection<GrantedAuthority> userAuthorities = this.mapPermissonsToAuthorities(this.getUserPermissions(user));
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				userAuthorities);
	}

	@Override
	public User loadUserById(Long id) {
		return this.userRepository.findById(id).orElseThrow(() -> new NotFoundException(Message.USER_NOT_FOUND));
	}

	@Override
	public User loadUserObjByUsername(String username) {
		return this.userRepository.findByUsername(username)
				.orElseThrow(() -> new NotFoundException(Message.USER_NOT_FOUND));
	}

	@Override
	public Role loadRoleById(Long id) {
		return this.roleRepository.findById(id).orElseThrow(() -> new NotFoundException(Message.ROLE_NOT_FOUND));
	}

	@Override
	public GetMyDatasDto getMyDatas(HolderType holderType) {
		User user = this.loadUserObjByUsername(getSecurityContextName());

		GetMyDatasDto getMyDatasDto = new GetMyDatasDto();
		getMyDatasDto.setName(user.getName());
		getMyDatasDto.setUsername(user.getUsername());
		getMyDatasDto.setUserId(user.getId());
		getMyDatasDto.setHolderType(user.getRoles().get(0).getHolder());
		// getMyDatasDto.setPermissions(getUserPermissions(user));

		if (holderType.equals(HolderType.JOBSEEKER)) {
			Jobseeker jobseeker = this.jobseekerProfileRepository.findByUserId(user.getId());
			getMyDatasDto.setJobseekerId(jobseeker.getId());
		} else if (holderType.equals(HolderType.COMPANY)) {
			getMyDatasDto.setCompanyId(this.companyEmployeeRepository.getCompanyId(user.getUsername()));
		}

		return getMyDatasDto;

	}

	private boolean compareCompanies(String username, String securityContextName) {
		if (this.companyEmployeeRepository.getCompanyId(username) == this.companyEmployeeRepository
				.getCompanyId(securityContextName)) {
			return true;
		}
		return false;
	}

	private String getSecurityContextName() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}

	private Collection<GrantedAuthority> mapPermissonsToAuthorities(List<Permission> permissions) {
		// @formatter:off
		return permissions
				.stream()
				.map(permission -> new SimpleGrantedAuthority(permission.getName()))
				.collect(Collectors.toList());
		// @formatter:on
	}

	private List<Permission> getUserPermissions(User user) {
		// @formatter:off
		return user.getRoles()
			.stream()
			.map(Role::getPermissions)
			.flatMap(Collection::stream)
			.collect(Collectors.toList());
		// @formatter:on

	}

}
