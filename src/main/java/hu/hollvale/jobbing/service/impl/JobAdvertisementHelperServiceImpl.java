package hu.hollvale.jobbing.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.dto.GetJobAdvertisementDto;
import hu.hollvale.jobbing.dto.JobseekerJobAdvertisementDto;
import hu.hollvale.jobbing.dto.company.CityDto;
import hu.hollvale.jobbing.dto.company.GetListValuesDto;
import hu.hollvale.jobbing.dto.company.WorkCategoryDto;
import hu.hollvale.jobbing.dto.company.WorkTypeDto;
import hu.hollvale.jobbing.enums.Message;
import hu.hollvale.jobbing.enums.WorkingType;
import hu.hollvale.jobbing.exception.NotFoundException;
import hu.hollvale.jobbing.model.company.City;
import hu.hollvale.jobbing.model.company.JobAdvertisement;
import hu.hollvale.jobbing.model.company.WorkCategory;
import hu.hollvale.jobbing.model.company.WorkType;
import hu.hollvale.jobbing.repository.CityRepository;
import hu.hollvale.jobbing.repository.JobAdvertisementRepository;
import hu.hollvale.jobbing.repository.WorkCategoryRepository;
import hu.hollvale.jobbing.repository.WorkTypeRepository;
import hu.hollvale.jobbing.service.JobAdvertisementHelperService;
import lombok.RequiredArgsConstructor;

@Transactional
@Service
@RequiredArgsConstructor
public class JobAdvertisementHelperServiceImpl implements JobAdvertisementHelperService {

	private final JobAdvertisementRepository jobAdvertisementRepository;
	private final WorkCategoryRepository workCategoryRepository;
	private final WorkTypeRepository workTypeRepository;
	private final CityRepository cityRepository;

	@Override
	public JobAdvertisement loadJobAdvertisementById(Long id) {

		return this.jobAdvertisementRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(Message.JOBADVERTISEMENT_NOT_FOUND));
	}

	@Override
	public WorkCategory loadWorkCategoryById(Long id) {
		return this.workCategoryRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(Message.WORKCATEGORY_NOT_FOUND));
	}

	@Override
	public WorkType loadWorkTypeById(Long id) {
		return this.workTypeRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(Message.WORKTYPE_NOT_FOUND));
	}

	@Override
	public City loadCityById(Long id) {
		return this.cityRepository.findById(id).orElseThrow(() -> new NotFoundException(Message.CITY_NOT_FOUND));
	}

	@Override
	public GetJobAdvertisementDto getJobAdvertisement(Long jobAdvertisementId) {
		// hirdetés id alapján visszaadja a hirdetés szükséges dolgait
		JobAdvertisement jobAdvertisement = this.loadJobAdvertisementById(jobAdvertisementId);

		return this.createSimpleJobAdvertisementDto(jobAdvertisement);

	}

	@Override
	public JobseekerJobAdvertisementDto getJobseekerJobAdvertisement(Long jobAdvertisementId) {

		JobAdvertisement jobAdvertisement = this.loadJobAdvertisementById(jobAdvertisementId);
		JobseekerJobAdvertisementDto jobseekerJobAdvertisementDto = this
				.makeExtendedJobAdvertisementDto(jobAdvertisement);

		return jobseekerJobAdvertisementDto;
	}

	@Override
	public GetListValuesDto getListValues() {
		GetListValuesDto getListValuesDto = new GetListValuesDto();
		List<CityDto> cityDtoList = new ArrayList<>();
		List<WorkCategoryDto> workCategoryDtoList = new ArrayList<>();
		List<WorkTypeDto> workTypeDtoList = new ArrayList<>();
		
		
		List<City> cities = this.cityRepository.findAll();
		for(City city: cities) {
			CityDto cityDto = new CityDto();
			cityDto.setId(city.getId());
			cityDto.setName(city.getName());
			cityDtoList.add(cityDto);
		}
		
		List<WorkCategory> workCategories = this.workCategoryRepository.findAll();
		for(WorkCategory workCategory: workCategories) {
			WorkCategoryDto workCategoryDto = new WorkCategoryDto();
			workCategoryDto.setId(workCategory.getId());
			workCategoryDto.setName(workCategory.getName());
			workCategoryDtoList.add(workCategoryDto);
		}
		
		List<WorkType> workingTypes = this.workTypeRepository.findAll();
		for(WorkType workType: workingTypes) {
			WorkTypeDto workTypeDto = new WorkTypeDto();
			workTypeDto.setId(workType.getId());
			workTypeDto.setName(workType.getName());
			workTypeDtoList.add(workTypeDto);
		}
		
		getListValuesDto.setCities(cityDtoList);
		getListValuesDto.setWorkCategories(workCategoryDtoList);
		getListValuesDto.setWorkTypes(workTypeDtoList);
		return getListValuesDto;
	}
	
	private GetJobAdvertisementDto createSimpleJobAdvertisementDto(JobAdvertisement jobAdvertisement) {

		GetJobAdvertisementDto getJobAdvertisementDto = new GetJobAdvertisementDto();

		this.makeSimpleJobAdvertisementDto(jobAdvertisement, getJobAdvertisementDto);
		return getJobAdvertisementDto;
	}

	private void makeSimpleJobAdvertisementDto(JobAdvertisement jobAdvertisement,
			GetJobAdvertisementDto getJobAdvertisementDto) {

		List<String> myCityNameList = new ArrayList<>();
		List<WorkingType> myWorkTypeNameList = new ArrayList<>();
		List<String> myWorkCategoryNameList = new ArrayList<>();

		getJobAdvertisementDto.setId(jobAdvertisement.getId());
		getJobAdvertisementDto.setName(jobAdvertisement.getName());
		getJobAdvertisementDto.setPlace(jobAdvertisement.getPlace());
		getJobAdvertisementDto.setSummary(jobAdvertisement.getSummary());
		getJobAdvertisementDto.setTextEditor(jobAdvertisement.getTextEditor());

		if (jobAdvertisement.getCities() != null) {
			for (City city : jobAdvertisement.getCities()) {
				myCityNameList.add(city.getName());
			}
		}

		if (jobAdvertisement.getWorkCategories() != null) {
			for (WorkCategory workCategory : jobAdvertisement.getWorkCategories()) {
				myWorkCategoryNameList.add(workCategory.getName());
			}
		}

		if (jobAdvertisement.getWorkTypes() != null) {
			for (WorkType workType : jobAdvertisement.getWorkTypes()) {
				myWorkTypeNameList.add(workType.getName());
			}
		}
		getJobAdvertisementDto.setCityNameList(myCityNameList);
		getJobAdvertisementDto.setWorkCategoryNameList(myWorkCategoryNameList);
		getJobAdvertisementDto.setWorkTypeNameList(myWorkTypeNameList);
	}

	private JobseekerJobAdvertisementDto makeExtendedJobAdvertisementDto(JobAdvertisement jobAdvertisement) {

		JobseekerJobAdvertisementDto jobseekerJobAdvertisementDto = new JobseekerJobAdvertisementDto();
		makeSimpleJobAdvertisementDto(jobAdvertisement, jobseekerJobAdvertisementDto);
		this.createSimpleJobAdvertisementDto(jobAdvertisement);
		jobseekerJobAdvertisementDto.setCompanyId(jobAdvertisement.getCompany().getId());
		jobseekerJobAdvertisementDto.setCompanyName(jobAdvertisement.getCompany().getName());
		jobseekerJobAdvertisementDto.setCompanyLogo(jobAdvertisement.getCompany().getLogo());
		jobseekerJobAdvertisementDto.setCompanyIntroduction(jobAdvertisement.getCompany().getIntroduction());

		return jobseekerJobAdvertisementDto;

	}

}
