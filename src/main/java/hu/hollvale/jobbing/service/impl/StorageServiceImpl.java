package hu.hollvale.jobbing.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import hu.hollvale.jobbing.config.ApplicationMediaProperties;
import hu.hollvale.jobbing.service.StorageService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class StorageServiceImpl implements StorageService {

	private final ApplicationMediaProperties applicationMediaProperties;
	private final ValidationService validationService;

	@Override
	public String saveFile(String fileBasePath, MultipartFile file, String fileNamePrefix) throws IOException {

		this.validationService.checkFileNotNull(file);

		Path rootLocation = Paths.get(fileBasePath);

		Files.createDirectories(rootLocation);

		String extinsionName = file.getContentType().split("/")[0];

		if (file.getOriginalFilename() != null) {
			String[] fileNameArray = file.getOriginalFilename().split("\\.");
			extinsionName = fileNameArray[fileNameArray.length - 1];
		}

		String fileName = generateFilename(extinsionName, fileNamePrefix);

		Path path = rootLocation.resolve(fileName);
		Files.copy(file.getInputStream(), path);

		return fileName;
	}

	@Override
	public String saveFile(String fileBasePath, MultipartFile file) throws IOException {
		return this.saveFile(fileBasePath, file, null);
	}

	public String generateFilename(String etxinsionName, String fileNamePrefix) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		String formatDateTime = LocalDateTime.now().format(formatter);
		String filename;
		if (fileNamePrefix != null) {
			filename = fileNamePrefix + "_" + formatDateTime + "." + etxinsionName;
		} else {
			filename = formatDateTime + "." + etxinsionName;
		}

		return filename;
	}

	@Override
	public void copyFile(String source, String targetBase, String fileName) throws IOException {
		Path sourcePath = Paths.get(source).resolve(fileName);
		Path targetBasePath = Paths.get(targetBase);

		Files.createDirectories(targetBasePath);

		Files.copy(sourcePath, targetBasePath.resolve(fileName));
	}

	@Override
	public void deleteFile(String path) throws IOException {
		new File(path).delete();
	}

	@Override
	public void archiveFile(String source, String targetBase, String fileName) throws IOException {
		this.copyFile(source, targetBase, fileName);
		this.deleteFile(fileName);

	}

	@Override
	public String getRootLocation() {
		return this.applicationMediaProperties.getRootLocation();
	}

	@Override
	public String getCvFolder() {
		return this.applicationMediaProperties.getCvFolder();
	}

	@Override
	public String getCompanyFolder() {
		return this.applicationMediaProperties.getCompanyFolder();
	}

	@Override
	public String getPathDelimeter() {
		return "/";
	}

	@Override
	public String getProfilFolder() {
		return this.applicationMediaProperties.getProfilFolder();
	}

	@Override
	public String getDeletedProfilFolder() {
		return this.applicationMediaProperties.getDeletedProfilFolder();
	}

	@Override
	public String getDeletedCvFolder() {
		return this.applicationMediaProperties.getDeletedCvFolder();
	}

	@Override
	public String getDeletedCompanyFolder() {
		return this.applicationMediaProperties.getDeletedCompanyFolder();
	}

}
