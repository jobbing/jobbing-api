package hu.hollvale.jobbing.service.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.dto.match.CompanyMatchDto;
import hu.hollvale.jobbing.enums.MatchStateType;
import hu.hollvale.jobbing.enums.Message;
import hu.hollvale.jobbing.exception.InvalidInputException;
import hu.hollvale.jobbing.model.company.Company;
import hu.hollvale.jobbing.model.jobseeker.Jobseeker;
import hu.hollvale.jobbing.model.match.Match;
import hu.hollvale.jobbing.repository.MatchRepository;
import hu.hollvale.jobbing.service.CompanyHelperService;
import hu.hollvale.jobbing.service.JobseekerHelperService;
import hu.hollvale.jobbing.service.MatchService;
import hu.hollvale.jobbing.service.ValidationService;
import hu.hollvale.jobbing.util.MatchUtil;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class MatchServiceImpl implements MatchService {

	private final MatchRepository matchRepository;
	private final CompanyHelperService companyHelperService;
	private final JobseekerHelperService jobseekerHelperService;
	private final ValidationService validationService;

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS)")
	public Long jobseekeerAcceptCompany(CompanyMatchDto companyMatchDto) {

		this.validationService.checkInputJobseekerIdValid(companyMatchDto.getJobseekerId());
		// KELL TARTALMAZNIA A DBNEK, MERT A COMPANY FOGADJA EL ELŐBB
		// (Ez ugye az a match amiben nincs bent a jobAdvertisement. Lásd: figma)
		Jobseeker jobseeker = this.jobseekerHelperService.loadJobseekerById(companyMatchDto.getJobseekerId());
		Company company = this.companyHelperService.loadCompanyById(companyMatchDto.getCompanyId());

		Match newMatch = new Match();
		newMatch.setJobseeker(jobseeker);
		newMatch.setCompany(company);

		Optional<Match> foundMatch = this.matchRepository.jobseekerCompany(newMatch);

		// match, company_reject, jobseeker_accept esetén nem tudja (újra) elfogadni
		// egyébként pedig ha tartalmazza a DB és a state company_accept-tel akkor igen
		foundMatch.ifPresentOrElse(match -> {
			if (match.getState() == MatchStateType.COMPANY_REJECT || match.getState() == MatchStateType.JOBSEEKER_ACCEPT
					|| match.getState() == MatchStateType.MATCH) {
				throw new InvalidInputException(Message.ALREADY_DECIDED);
			} else {
				match.setState(MatchStateType.MATCH);
				this.matchRepository.save(match);
			}
		}, () -> {
			throw new InvalidInputException(Message.MATCH_NOT_FOUND);
		});

		return jobseeker.getId();
	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS)")
	public Long jobseekeerRejectCompany(CompanyMatchDto companyMatchDto) {

		this.validationService.checkInputJobseekerIdValid(companyMatchDto.getJobseekerId());
		// KELL TARTALMAZNIA A DBNEK, MERT A COMPANY FOGADJA EL ELŐBB
		Jobseeker jobseeker = this.jobseekerHelperService.loadJobseekerById(companyMatchDto.getJobseekerId());
		Company company = this.companyHelperService.loadCompanyById(companyMatchDto.getCompanyId());

		Match newMatch = new Match();
		newMatch.setJobseeker(jobseeker);
		newMatch.setCompany(company);

		Optional<Match> foundMatch = this.matchRepository.jobseekerCompany(newMatch);

		Match matchToUpdate = MatchUtil.jobseekerRejectCompany(foundMatch);

		this.matchRepository.save(matchToUpdate);

		return jobseeker.getId();
	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_DECIDE)")
	public void companyAcceptJobseekeer(CompanyMatchDto companyMatchDto) {
		// NEM tartalmazhatja a DB
		// COMPANY_ACCEPT UTÁN LEHET REJECTELNI, DE FORDÍTVA NEM
		this.validationService.checkInputCompanyValid(companyMatchDto.getCompanyId());

		Jobseeker jobseeker = this.jobseekerHelperService.loadJobseekerById(companyMatchDto.getJobseekerId());
		Company company = this.companyHelperService.loadCompanyById(companyMatchDto.getCompanyId());

		Optional<Match> match = this.matchRepository.companyJobseekeer(company, jobseeker);
		if (match.isPresent()) {
			System.err.println("no way");
		}
		Match newMatch = MatchUtil.companyAcceptJobseeker(match);
		newMatch.setCompany(company);
		newMatch.setJobseeker(jobseeker);

		this.matchRepository.save(newMatch);

	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_DECIDE)")
	public void companyRejectJobseekeer(CompanyMatchDto companyMatchDto) {
		// tartalmazhatja a DB
		this.validationService.checkInputCompanyValid(companyMatchDto.getCompanyId());

		Jobseeker jobseeker = this.jobseekerHelperService.loadJobseekerById(companyMatchDto.getJobseekerId());
		Company company = this.companyHelperService.loadCompanyById(companyMatchDto.getCompanyId());

		Match newMatch = new Match();
		newMatch.setJobseeker(jobseeker);
		newMatch.setCompany(company);
		newMatch.setState(MatchStateType.COMPANY_REJECT);

		Optional<Match> match = this.matchRepository.companyJobseekeer(company, jobseeker);

		match.ifPresentOrElse(m -> {
			if (m.getState() == MatchStateType.COMPANY_REJECT || m.getState() == MatchStateType.JOBSEEKER_REJECT) {
				throw new InvalidInputException(Message.ALREADY_DECIDED);
			} else {
				m.setState(MatchStateType.COMPANY_REJECT);
				this.matchRepository.save(m);
			}
		}, () -> {
			this.matchRepository.save(newMatch);
		});

	}
}
