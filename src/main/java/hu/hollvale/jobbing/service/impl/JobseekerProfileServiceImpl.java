package hu.hollvale.jobbing.service.impl;

import java.io.IOException;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import hu.hollvale.jobbing.dto.DeleteDrivingLicenceDto;
import hu.hollvale.jobbing.dto.DeleteLanguageExamDto;
import hu.hollvale.jobbing.dto.DrivingLicenceDto;
import hu.hollvale.jobbing.dto.ExperienceDto;
import hu.hollvale.jobbing.dto.IntroductionUpdateDto;
import hu.hollvale.jobbing.dto.LanguageExamDto;
import hu.hollvale.jobbing.dto.PersonalDataUpdateDto;
import hu.hollvale.jobbing.dto.QualityDto;
import hu.hollvale.jobbing.dto.WorkILookForUpdateDto;
import hu.hollvale.jobbing.enums.DrivingLicenceType;
import hu.hollvale.jobbing.enums.Message;
import hu.hollvale.jobbing.exception.InvalidInputException;
import hu.hollvale.jobbing.exception.NotFoundException;
import hu.hollvale.jobbing.model.jobseeker.DrivingLicence;
import hu.hollvale.jobbing.model.jobseeker.Experience;
import hu.hollvale.jobbing.model.jobseeker.Jobseeker;
import hu.hollvale.jobbing.model.jobseeker.LanguageExam;
import hu.hollvale.jobbing.model.jobseeker.Quality;
import hu.hollvale.jobbing.repository.DrivingLicenceRepository;
import hu.hollvale.jobbing.repository.ExperienceRepository;
import hu.hollvale.jobbing.repository.JobseekerProfileRepository;
import hu.hollvale.jobbing.repository.LanguageExamRepository;
import hu.hollvale.jobbing.repository.QualityRepository;
import hu.hollvale.jobbing.service.JobseekerHelperService;
import hu.hollvale.jobbing.service.JobseekerProfileService;
import hu.hollvale.jobbing.service.StorageService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS)")
public class JobseekerProfileServiceImpl implements JobseekerProfileService {

	private final JobseekerProfileRepository jobseekerProfileRepository;
	private final DrivingLicenceRepository drivingLicenceRepository;
	private final LanguageExamRepository languageExamRepository;
	private final QualityRepository qualityRepository;
	private final ExperienceRepository experienceRepository;
	private final StorageService storageService;
	private final ValidationService validationService;
	private final JobseekerHelperService jobseekerHelperService;

	@Override
	public void updateIntroduction(IntroductionUpdateDto introductionUpdateDto) {
		this.validationService.checkInputJobseekerIdValid(introductionUpdateDto.getJobseekerId());
		Jobseeker jobseeker = jobseekerHelperService.loadJobseekerById(introductionUpdateDto.getJobseekerId());
		jobseeker.setIntroduction(introductionUpdateDto.getIntroduction());
		this.jobseekerProfileRepository.save(jobseeker);

	}

	@Override
	public void updateWorkILookFor(WorkILookForUpdateDto workILookForUpdateDto) {
		this.validationService.checkInputJobseekerIdValid(workILookForUpdateDto.getJobseekerId());
		Jobseeker jobseeker = jobseekerHelperService.loadJobseekerById(workILookForUpdateDto.getJobseekerId());
		jobseeker.setWorkILookFor(workILookForUpdateDto.getWorkILookFor());
		this.jobseekerProfileRepository.save(jobseeker);

	}

	@Override
	public void updatePersonalDatas(PersonalDataUpdateDto personalDataUpdateDto) {
		this.validationService.checkInputJobseekerIdValid(personalDataUpdateDto.getJobseekerId());
		this.jobseekerProfileRepository.updatePersonalData(personalDataUpdateDto);

	}

	@Override
	public void deleteDrivingLicence(DeleteDrivingLicenceDto deleteDrivingLicenceDto) {

		this.validationService.checkInputJobseekerIdValid(deleteDrivingLicenceDto.getJobseekerId());

		DrivingLicence drivingLicence = this.jobseekerHelperService
				.loadDrivingLicenceById(deleteDrivingLicenceDto.getDrivingLicenceId());
		Jobseeker jobseeker = jobseekerHelperService.loadJobseekerById(deleteDrivingLicenceDto.getJobseekerId());

		if (jobseeker.getDrivingLicences().contains(drivingLicence)) {
			jobseeker.getDrivingLicences().remove(drivingLicence);
		} else {
			throw new NotFoundException(Message.PAIR_NOT_FOUND);
		}

	}

	@Override
	public Long deleteLanguageExam(DeleteLanguageExamDto deleteLanguageExamDto) {

		this.validationService.checkInputJobseekerIdValid(deleteLanguageExamDto.getJobseekerId());

		LanguageExam languageExam = this.jobseekerHelperService
				.loadLanguageExamById(deleteLanguageExamDto.getLanguageExamId());
		Jobseeker jobseeker = jobseekerHelperService.loadJobseekerById(deleteLanguageExamDto.getJobseekerId());

		if (jobseeker.getLanguageExams().contains(languageExam)) {
			jobseeker.getLanguageExams().remove(languageExam);
		} else {
			throw new NotFoundException(Message.PAIR_NOT_FOUND);
		}

		return deleteLanguageExamDto.getJobseekerId();
	}

	@Override
	public Long deleteQualtity(Long qualityId) {
		this.validationService.checkInputQualityIdValid(qualityId);

		Quality quality = this.jobseekerHelperService.loadQualityById(qualityId);
		this.qualityRepository.delete(quality);
		return quality.getJobseeker().getId();
	}

	@Override
	public Long deleteExperience(Long exprienceId) {
		this.validationService.checkInputExperienceIdValid(exprienceId);

		Experience experience = this.jobseekerHelperService.loadExperienceById(exprienceId);
		this.experienceRepository.delete(experience);
		return experience.getJobseeker().getId();

	}

	@Override
	public Long saveDrivingLicences(DrivingLicenceDto drivingLicenceDto) {

		this.validationService.checkInputJobseekerIdValid(drivingLicenceDto.getJobseekerId());

		Jobseeker jobseeker = jobseekerHelperService.loadJobseekerById(drivingLicenceDto.getJobseekerId());

		jobseeker.getDrivingLicences().clear();

		for (DrivingLicenceType value : drivingLicenceDto.getValue()) {
			DrivingLicence drivingLicence = this.drivingLicenceRepository.findByValue(value)
					.orElseThrow(() -> new NotFoundException(Message.DRIVING_LICENCE_NOT_FOUND));

			jobseeker.getDrivingLicences().add(drivingLicence);
		}

		return jobseeker.getId();

	}

	@Override
	public Long saveLanguageExam(LanguageExamDto languageExamDto) {

		this.validationService.checkInputJobseekerIdValid(languageExamDto.getJobseekerId());
		Jobseeker jobseeker = jobseekerHelperService.loadJobseekerById(languageExamDto.getJobseekerId());
		Optional<LanguageExam> optLanguageExam = this.languageExamRepository.findLanguageExam(languageExamDto);

		if (optLanguageExam.isPresent()) {
			if (jobseeker.getLanguageExams().contains(optLanguageExam.get())) {
				throw new InvalidInputException(Message.JOBSEEKER_ALREADY_HAS_THIS_LANGUAGEEXAM);
			} else {
				jobseeker.getLanguageExams().add(optLanguageExam.get());
				this.jobseekerProfileRepository.save(jobseeker);
			}
		} else {
			LanguageExam languageExam = LanguageExam.fromLanguageExam(languageExamDto);
			this.languageExamRepository.save(languageExam);
			jobseeker.getLanguageExams().add(languageExam);
		}

		return this.jobseekerProfileRepository.save(jobseeker).getId();

	}

	@Override
	public Long saveQuality(QualityDto qualityDto) {

		this.validationService.checkInputJobseekerIdValid(qualityDto.getJobseekerId());
		Jobseeker jobseeker = jobseekerHelperService.loadJobseekerById(qualityDto.getJobseekerId());
		Boolean qualityExist = this.jobseekerProfileRepository.checkQualityExist(qualityDto);

		if (qualityExist) {
			throw new InvalidInputException(Message.QUALITY_ALREADY_EXIST);
		}

		Quality quality = Quality.fromQuality(qualityDto);
		quality.setJobseeker(jobseeker);
		this.qualityRepository.save(quality);

		return quality.getJobseeker().getId();

	}

	@Override
	public Long saveExperience(ExperienceDto experienceDto) {

		this.validationService.checkInputJobseekerIdValid(experienceDto.getJobseekerId());
		this.validationService.checkCorrectDates(experienceDto.getStartDate(), experienceDto.getEndDate());

		Jobseeker jobseeker = jobseekerHelperService.loadJobseekerById(experienceDto.getJobseekerId());
		Boolean experienceExist = this.jobseekerProfileRepository.checkExperienceExist(experienceDto);

		if (experienceExist) {
			throw new InvalidInputException(Message.EXPERIENCE_ALREADY_EXIST);
		}

		Experience experience = Experience.fromExperience(experienceDto);
		experience.setJobseeker(jobseeker);

		this.experienceRepository.save(experience);

		return jobseeker.getId();

	}

	@Override
	public String savePicture(String username, MultipartFile file) throws IOException {

		this.validationService.checkInputUsernameValid(username);
		this.validationService.checkFileTypeImage(file);

		Long jobseekerId = this.jobseekerProfileRepository.JobseekerIdByUsername(username);

		Jobseeker jobseeker = jobseekerHelperService.loadJobseekerById(jobseekerId);
		String sPath = jobseeker.getPicture();

		String fileBasePath = this.buildProfilePictureBasePath();

		String filename = this.storageService.saveFile(fileBasePath, file, username);
		jobseeker.setPicture(filename);
		this.jobseekerProfileRepository.save(jobseeker);

		if (sPath != null) {
			this.archivePicture(fileBasePath, sPath);
		}

		return filename;
	}

	@Override
	public void deletePicture(Long jobseekerId) throws IOException {

		this.validationService.checkInputJobseekerIdValid(jobseekerId);
		Jobseeker jobseeker = jobseekerHelperService.loadJobseekerById(jobseekerId);

		String filename = jobseeker.getPicture();
		this.validationService.checkFileDeletable(filename);
		String fileBasePath = this.buildProfilePictureBasePath();
		this.archivePicture(fileBasePath, filename);
		jobseeker.setPicture(null);
		this.jobseekerProfileRepository.save(jobseeker);

	}

	@Override
	public String saveCV(String username, MultipartFile file) throws IOException {

		this.validationService.checkInputUsernameValid(username);
		this.validationService.checkFileTypePdf(file);

		Long jobseekerId = this.jobseekerProfileRepository.JobseekerIdByUsername(username);

		Jobseeker jobseeker = jobseekerHelperService.loadJobseekerById(jobseekerId);

		String fileBasePath = this.buildCvBasePath();
		String sPath = jobseeker.getCV();
		String filename = this.storageService.saveFile(fileBasePath, file, username);
		jobseeker.setCV(filename);
		this.jobseekerProfileRepository.save(jobseeker);

		if (sPath != null) {
			this.archiveCV(fileBasePath, sPath);
		}

		return filename;

	}

	@Override
	public void deleteCV(Long jobseekerId) throws IOException {

		this.validationService.checkInputJobseekerIdValid(jobseekerId);
		Jobseeker jobseeker = jobseekerHelperService.loadJobseekerById(jobseekerId);

		String filename = jobseeker.getCV();

		this.validationService.checkFileDeletable(filename);

		String fileBasePath = this.buildCvBasePath();
		this.archiveCV(fileBasePath, filename);
		jobseeker.setCV(null);
		this.jobseekerProfileRepository.save(jobseeker);
	}

	private void archiveFile(String fileBasePath, String newFileBasePath, String sPath) throws IOException {
		this.storageService.archiveFile(fileBasePath, newFileBasePath, sPath);
	}

	private void archivePicture(String fileBasePath, String sPath) throws IOException {

		String newFileBasePath = this.buildDeletedProfilePictureBasePath();
		this.archiveFile(fileBasePath, newFileBasePath, sPath);
	}

	private void archiveCV(String fileBasePath, String sPath) throws IOException {

		String newFileBasePath = this.buildDeletedCvBasePath();
		this.archiveFile(fileBasePath, newFileBasePath, sPath);
	}

	private String buildProfilePictureBasePath() {

		// @formatter:off
		return new StringBuilder()
				.append(this.storageService.getRootLocation())
				.append(this.storageService.getPathDelimeter())
				.append(this.storageService.getProfilFolder())
		.toString();
		// @formatter:on

	}

	private String buildDeletedProfilePictureBasePath() {

		// @formatter:off
		return new StringBuilder()
				.append(this.storageService.getRootLocation())
				.append(this.storageService.getPathDelimeter())
				.append(this.storageService.getDeletedProfilFolder())
		.toString();
		// @formatter:on

	}

	private String buildCvBasePath() {

		// @formatter:off
		return new StringBuilder()
				.append(this.storageService.getRootLocation())
				.append(this.storageService.getPathDelimeter())
				.append(this.storageService.getCvFolder())
		.toString();
		// @formatter:on

	}

	private String buildDeletedCvBasePath() {

		// @formatter:off
		return new StringBuilder()
				.append(this.storageService.getRootLocation())
				.append(this.storageService.getPathDelimeter())
				.append(this.storageService.getDeletedCvFolder())
		.toString();
		// @formatter:on

	}

}
