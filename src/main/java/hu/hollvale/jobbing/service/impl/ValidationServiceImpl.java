package hu.hollvale.jobbing.service.impl;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import hu.hollvale.jobbing.enums.Message;
import hu.hollvale.jobbing.exception.InvalidInputException;
import hu.hollvale.jobbing.exception.NotFoundException;
import hu.hollvale.jobbing.model.company.JobAdvertisement;
import hu.hollvale.jobbing.model.jobseeker.Experience;
import hu.hollvale.jobbing.model.jobseeker.Jobseeker;
import hu.hollvale.jobbing.model.jobseeker.Quality;
import hu.hollvale.jobbing.model.user.User;
import hu.hollvale.jobbing.repository.CompanyEmployeeRepository;
import hu.hollvale.jobbing.repository.CompanyProfileRepository;
import hu.hollvale.jobbing.repository.ExperienceRepository;
import hu.hollvale.jobbing.repository.JobseekerProfileRepository;
import hu.hollvale.jobbing.repository.QualityRepository;
import hu.hollvale.jobbing.repository.RoleRepository;
import hu.hollvale.jobbing.repository.UserRepository;
import hu.hollvale.jobbing.service.JobAdvertisementHelperService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class ValidationServiceImpl implements ValidationService {

	private final JobseekerProfileRepository jobseekerProfileRepository;
	private final CompanyProfileRepository companyProfileRepository;
	private final CompanyEmployeeRepository companyEmployeeRepository;
	private final RoleRepository roleRepository;
	private final UserRepository userRepository;
	private final QualityRepository qualityRepository;
	private final ExperienceRepository experienceRepository;
	private final JobAdvertisementHelperService jobAdvertisementHelperService;

	@Override
	public void checkJobseekerExist(Long jobseekerId) {

		this.jobseekerProfileRepository.findById(jobseekerId)
				.orElseThrow(() -> new InvalidInputException(Message.JOBSEEKER_NOT_FOUND));
	}

	@Override
	public void checkCompanyExist(Long companyId) {

		this.companyProfileRepository.findById(companyId)
				.orElseThrow(() -> new NotFoundException(Message.COMPANY_NOT_FOUND));
	}

	@Override
	public void checkUsernameNotExist(String username) {

		if (this.userRepository.usernameExist(username)) {
			throw new InvalidInputException(Message.USER_EXIST);
		}

	}

	@Override
	public void checkUsernameExist(String username) {

		this.userRepository.findByUsername(username)
				.orElseThrow(() -> new InvalidInputException(Message.USER_NOT_FOUND));

	}

	@Override
	public void checkUserEmailNotExist(String email) {

		if (this.userRepository.userEmailExist(email).isPresent()) {
			throw new InvalidInputException(Message.USER_EMAIL_EXIST);

		}

	}

	@Override
	public void checkUserHasRole(Long roleId) {

		if (roleId == null) {
			throw new InvalidInputException(Message.MUST_HAVE_ROLE);
		}

		this.roleRepository.findById(roleId).orElseThrow(() -> new InvalidInputException(Message.ROLE_NOT_FOUND));

	}

	@Override
	public void checkCorrectDates(Date startDate, Date endDate) {
		// endDate is before startDate
		if (startDate.compareTo(endDate) == 1) {
			throw new InvalidInputException(Message.BAD_DATES);
		}

	}

	@Override
	public void checkUserNotHaveJobseeker(Long userId) {

		if (this.jobseekerProfileRepository.userHasJobseeker(userId).isPresent()) {
			throw new InvalidInputException(Message.USER_ALREADY_HAS_JOBSEEKER);
		}

	}

	@Override
	public void checkFileNotNull(MultipartFile file) {

		if (file == null) {
			throw new InvalidInputException(Message.FILE_NULL);
		}

	}

	@Override
	public void checkFileDeletable(String file) {

		// only deletable if exist
		if (file == null) {
			throw new InvalidInputException(Message.FILE_NULL);
		}

	}

	@Override
	public void checkFileTypePdf(MultipartFile file) {

		if (!file.getContentType().equals("application/pdf")) {
			throw new InvalidInputException(Message.PDF_REQUIRED);
		}
	}

	@Override
	public void checkFileTypeImage(MultipartFile file) {

		if (!file.getContentType().equals("image/png")) {
			throw new InvalidInputException(Message.IMAGE_REQUIRED);
		}
	}

	@Override
	public void checkEmailUpdateValid(String oldEmail, String newEmail) {
		// ez így nem szebb, mint && -del?
		if (oldEmail != null) {
			if (!oldEmail.equals(newEmail)) {
				if (this.companyProfileRepository.companyEmailExist(newEmail).isPresent()) {
					throw new NotFoundException(Message.COMPANY_EMAIL_EXIST);
				}
			}
		}

	}

	@Override
	public void checkEmailFormatValid(String email) {
		String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		if (!matcher.matches()) {
			throw new InvalidInputException(Message.INVALID_EMAIL_FORMAT);
		}

	}

	@Override
	public void checkPasswordValid(String password) {

		/*
		 * ^ # start-of-string (?=.*[0-9]) # a digit must occur at least once
		 * (?=.*[a-z]) # a lower case letter must occur at least once (?=.*[A-Z]) # an
		 * upper case letter must occur at least once (?=.*[@#$%^&+=]) # a special
		 * character must occur at least once (?=\S+$) # no whitespace allowed in the
		 * entire string .{8,} # anything, at least eight places though $ #
		 * end-of-string
		 */

		String regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(password);
		if (!matcher.matches()) {
			throw new InvalidInputException(Message.INVALID_PASSWORD_FORMAT);
		}
	}

	@Override
	public void checkInputUsernameValid(String username) {
		if (!username.equals(SecurityContextHolder.getContext().getAuthentication().getName())) {
			throw new InvalidInputException(Message.INVALID_USERNAME_INPUT);
		}
	}

	@Override
	public void checkInputCompanyValid(Long companyId) {
		if (companyId != this.companyEmployeeRepository
				.getCompanyId(SecurityContextHolder.getContext().getAuthentication().getName())) {
			throw new InvalidInputException(Message.INVALID_COMPANY_INPUT);
		}
	}

	@Override
	public void checkInputJobseekerIdValid(Long jobseekerId) {

		if (getUserJobseekerId() != jobseekerId) {
			throw new InvalidInputException(Message.INVALID_JOBSEEKER_INPUT);
		}

	}

	@Override
	public void checkInputQualityIdValid(Long qualityId) {

		Quality quality = this.qualityRepository.findById(qualityId)
				.orElseThrow(() -> new NotFoundException(Message.QUALITY_NOT_FOUND));

		if (getUserJobseekerId() != quality.getJobseeker().getId()) {
			throw new InvalidInputException(Message.INVALID_QUALITY_INPUT);
		}

	}

	@Override
	public void checkInputExperienceIdValid(Long experienceId) {

		Experience experience = this.experienceRepository.findById(experienceId)
				.orElseThrow(() -> new NotFoundException(Message.EXPERIENCE_NOT_FOUND));

		if (getUserJobseekerId() != experience.getJobseeker().getId()) {
			throw new InvalidInputException(Message.INVALID_EXPERIENCE_INPUT);
		}

	}

	@Override
	public void checkEmployeeAndAdvertisementInTheSameCompany(Long jobAdvertisementId) {

		Long companyId = this.companyEmployeeRepository
				.getCompanyId(SecurityContextHolder.getContext().getAuthentication().getName());

		JobAdvertisement jobAdvertisement = this.jobAdvertisementHelperService
				.loadJobAdvertisementById(jobAdvertisementId);

		if (companyId != jobAdvertisement.getCompany().getId()) {
			throw new InvalidInputException(Message.COMPANY_NOT_MATCH);
		}
	}

	private Long getUserJobseekerId() {
		User user = this.userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName())
				.orElseThrow(() -> new NotFoundException(Message.USER_NOT_FOUND));

		Jobseeker jobseeker = this.jobseekerProfileRepository.getByUserId(user.getId());

		return jobseeker.getId();
	}

}
