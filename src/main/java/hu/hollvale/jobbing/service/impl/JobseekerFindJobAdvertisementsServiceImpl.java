package hu.hollvale.jobbing.service.impl;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.dto.company.JobAdvertisementFindDto;
import hu.hollvale.jobbing.dto.filter.JobAdvertisementsFilter;
import hu.hollvale.jobbing.repository.MatchByAdvertisementRepository;
import hu.hollvale.jobbing.service.JobseekerFindJobAdvertisementsService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor

@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS)")
public class JobseekerFindJobAdvertisementsServiceImpl implements JobseekerFindJobAdvertisementsService {

	private final MatchByAdvertisementRepository matchByAdvertisementRepository;
	private final ValidationService validationService;

	@Override
	public Page<JobAdvertisementFindDto> pageJobAdvertisements(JobAdvertisementsFilter jobAdvertisementFilter,
			Pageable pageable) {

		this.validationService.checkJobseekerExist(jobAdvertisementFilter.getJobseekerId());
		this.validationService.checkInputJobseekerIdValid(jobAdvertisementFilter.getJobseekerId());

		Page<Long> filterMatchAdvertisementIds = this.matchByAdvertisementRepository
				.filterMatchAdvertisements(jobAdvertisementFilter, pageable);

		return this.matchByAdvertisementRepository
				.getJobAdvertisementsByIdList(filterMatchAdvertisementIds.getContent(), pageable);

	}

}
