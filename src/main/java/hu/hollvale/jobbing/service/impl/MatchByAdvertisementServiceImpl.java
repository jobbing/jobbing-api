package hu.hollvale.jobbing.service.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.dto.match.JobseekerMatchDto;
import hu.hollvale.jobbing.enums.MatchStateType;
import hu.hollvale.jobbing.enums.Message;
import hu.hollvale.jobbing.exception.InvalidInputException;
import hu.hollvale.jobbing.model.company.Company;
import hu.hollvale.jobbing.model.company.JobAdvertisement;
import hu.hollvale.jobbing.model.jobseeker.Jobseeker;
import hu.hollvale.jobbing.model.match.MatchByAdvertisement;
import hu.hollvale.jobbing.repository.CompanyEmployeeRepository;
import hu.hollvale.jobbing.repository.MatchByAdvertisementRepository;
import hu.hollvale.jobbing.service.JobAdvertisementHelperService;
import hu.hollvale.jobbing.service.JobseekerHelperService;
import hu.hollvale.jobbing.service.MatchByAdvertisementService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class MatchByAdvertisementServiceImpl implements MatchByAdvertisementService {

	private final MatchByAdvertisementRepository matchByAdvertisementRepository;
	private final JobAdvertisementHelperService jobAdvertisementHelperService;
	private final JobseekerHelperService jobseekerHelperService;
	private final ValidationService validationService;
	private final CompanyEmployeeRepository companyEmployeeRepository;

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS)")
	public void jobseekerRejectJobAdvertisement(JobseekerMatchDto jobseekerMatchDto) {
		// FONTOS: itt mindig csak a jobseeker által kerülhet be a rekordok közé (JS
		// fogja keresni a JA-kat)
		this.validationService.checkInputJobseekerIdValid(jobseekerMatchDto.getJobseekerId());

		Jobseeker jobseeker = this.jobseekerHelperService.loadJobseekerById(jobseekerMatchDto.getJobseekerId());
		JobAdvertisement jobAdvertisement = this.jobAdvertisementHelperService
				.loadJobAdvertisementById(jobseekerMatchDto.getJobAdvertisementId());
		Company company = jobAdvertisement.getCompany();

		MatchByAdvertisement ma = new MatchByAdvertisement();
		ma.setJobAdvertisement(jobAdvertisement);
		ma.setJobseeker(jobseeker);
		ma.setCompany(jobAdvertisement.getCompany());
		// lekérem a DB-ből azt az objektumot, amit megadtunk
		Optional<MatchByAdvertisement> foundMatch = this.matchByAdvertisementRepository.jobseekerJobAdvertisement(ma);
		// 1, tartalmazza a DB, mivel a jelentkezéseknél nézi a JS
		// 2, NEM tartalmazza a DB, mert a keresésnél nézi a JS

		foundMatch.ifPresentOrElse(m -> {
			// tartalmazza: módosítás
			m.setState(MatchStateType.JOBSEEKER_REJECT);
			this.matchByAdvertisementRepository.save(m);
		}, () -> {
			// nem tartalmazza, új létrehozás
			MatchByAdvertisement matchByAdvertisement = new MatchByAdvertisement();
			matchByAdvertisement.setCompany(company);
			matchByAdvertisement.setJobAdvertisement(jobAdvertisement);
			matchByAdvertisement.setJobseeker(jobseeker);
			matchByAdvertisement.setState(MatchStateType.JOBSEEKER_REJECT);
			this.matchByAdvertisementRepository.save(matchByAdvertisement);
		});

	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).JOBSEEKER_PERMISSIONS)")
	public void jobseekerAcceptJobAdvertisement(JobseekerMatchDto jobseekerMatchDto) {
		// FONTOS: itt mindig csak a jobseeker által kerülhet be a rekordok közé (JS
		// fogja keresni a JA-kat)
		this.validationService.checkInputJobseekerIdValid(jobseekerMatchDto.getJobseekerId());

		Jobseeker jobseeker = this.jobseekerHelperService.loadJobseekerById(jobseekerMatchDto.getJobseekerId());
		JobAdvertisement jobAdvertisement = this.jobAdvertisementHelperService
				.loadJobAdvertisementById(jobseekerMatchDto.getJobAdvertisementId());
		Company company = jobAdvertisement.getCompany();

		MatchByAdvertisement ma = new MatchByAdvertisement();
		ma.setJobAdvertisement(jobAdvertisement);
		ma.setJobseeker(jobseeker);
		ma.setCompany(jobAdvertisement.getCompany());

		// lekérem a DB-ből azt az objektumot, amit megadtunk
		Optional<MatchByAdvertisement> foundMatch = this.matchByAdvertisementRepository.jobseekerJobAdvertisement(ma);
		// NEM tartalmazhatja a DB, mert a JS tud először dönteni és ha rejectelte, már
		// nem szabad látnia, újra acceptelni pedig már nem tudja

		foundMatch.ifPresentOrElse(m -> {
			throw new InvalidInputException(Message.ALREADY_DECIDED);
		}, () -> {
			MatchByAdvertisement matchByAdvertisement = new MatchByAdvertisement();
			matchByAdvertisement.setCompany(company);
			matchByAdvertisement.setJobAdvertisement(jobAdvertisement);
			matchByAdvertisement.setJobseeker(jobseeker);
			matchByAdvertisement.setState(MatchStateType.JOBSEEKER_ACCEPT);
			this.matchByAdvertisementRepository.save(matchByAdvertisement);
		});

	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_DECIDE)")
	public void companyRejectJobseekerByAdvertisement(JobseekerMatchDto jobseekerMatchDto) {
		// HIBÁT KELL DOBNIA, HA NINCS ÉRTÉK A DB-BEN
		this.validationService.checkEmployeeAndAdvertisementInTheSameCompany(jobseekerMatchDto.getJobAdvertisementId());

		Jobseeker jobseeker = this.jobseekerHelperService.loadJobseekerById(jobseekerMatchDto.getJobseekerId());
		JobAdvertisement jobAdvertisement = this.jobAdvertisementHelperService
				.loadJobAdvertisementById(jobseekerMatchDto.getJobAdvertisementId());
		MatchByAdvertisement ma = new MatchByAdvertisement();
		ma.setJobAdvertisement(jobAdvertisement);
		ma.setJobseeker(jobseeker);
		ma.setCompany(jobAdvertisement.getCompany());

		Optional<MatchByAdvertisement> foundMatch = this.matchByAdvertisementRepository
				.companyRejectJobseekerByAdvertisement(ma);
		// companyreject esetén biztosak lehetünk benne, hogy előtte volt egy
		// JobseekerAccept (vagy match), mivel ez csak ezután következhet.
		foundMatch.ifPresentOrElse(m -> {
			m.setState(MatchStateType.COMPANY_REJECT);
			this.matchByAdvertisementRepository.save(m);
		}, () -> {
			throw new InvalidInputException(Message.MATCH_NOT_FOUND);
		});
	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_DECIDE)")
	public void companyAcceptJobseekerByAdvertisement(JobseekerMatchDto jobseekerMatchDto) {
		// HIBÁT KELL DOBNIA, HA NINCS ÉRTÉK A DB-BEN
		this.validationService.checkEmployeeAndAdvertisementInTheSameCompany(jobseekerMatchDto.getJobAdvertisementId());

		Jobseeker jobseeker = this.jobseekerHelperService.loadJobseekerById(jobseekerMatchDto.getJobseekerId());
		JobAdvertisement jobAdvertisement = this.jobAdvertisementHelperService
				.loadJobAdvertisementById(jobseekerMatchDto.getJobAdvertisementId());
		MatchByAdvertisement matchByAdvertisement = new MatchByAdvertisement();
		matchByAdvertisement.setJobAdvertisement(jobAdvertisement);
		matchByAdvertisement.setJobseeker(jobseeker);
		matchByAdvertisement.setCompany(jobAdvertisement.getCompany());

		Optional<MatchByAdvertisement> foundMatch = this.matchByAdvertisementRepository
				.companyAcceptJobseekerByAdvertisement(matchByAdvertisement);
		// companyAccept esetén biztosak lehetünk benne, hogy előtte volt egy
		// JobseekerAccept, mivel ez csak ezután következhet.
		foundMatch.ifPresentOrElse(match -> {
			match.setState(MatchStateType.MATCH);
			this.matchByAdvertisementRepository.save(match);
		}, () -> {
			throw new InvalidInputException(Message.MATCH_NOT_FOUND);
		});

	}

}
