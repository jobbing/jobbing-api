package hu.hollvale.jobbing.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.dto.company.CompanyJobAdvertisementListDto;
import hu.hollvale.jobbing.dto.company.CompanyJobAdvertisementSummariesDto;
import hu.hollvale.jobbing.dto.company.CompanyProfileDataListDto;
import hu.hollvale.jobbing.dto.company.JobseekerDto;
import hu.hollvale.jobbing.enums.Message;
import hu.hollvale.jobbing.exception.NotFoundException;
import hu.hollvale.jobbing.model.company.Company;
import hu.hollvale.jobbing.model.company.JobAdvertisement;
import hu.hollvale.jobbing.repository.CompanyProfileRepository;
import hu.hollvale.jobbing.repository.MatchRepository;
import hu.hollvale.jobbing.service.CompanyHelperService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class CompanyHelperServiceImpl implements CompanyHelperService {

	private final CompanyProfileRepository companyProfileRepository;
	private final ValidationService validationService;

	@Autowired
	private final MatchRepository matchRepository;

	@Override
	public boolean companyEmailExist(String email) {
		return this.companyProfileRepository.companyEmailExist(email).isPresent();
	}

	@Override
	public List<JobseekerDto> getMatchedJobseekers(Long companyId) {
		return this.matchRepository.getMatchedJobseekers(companyId);

	}

	@Override
	public CompanyProfileDataListDto loadProfileData(Long companyId) {
		this.validationService.checkCompanyExist(companyId);
		return this.companyProfileRepository.loadProfileData(companyId);
	}

	@Override
	public List<CompanyJobAdvertisementListDto> loadCompanyJobAdvertisements(Long companyId) {
		this.validationService.checkCompanyExist(companyId);
		return this.companyProfileRepository.loadCompanyJobAdvertisements(companyId);
	}

	@Override
	public List<CompanyJobAdvertisementSummariesDto> loadCompanyJobAdvertisementSummaries(Long companyId) {

		this.validationService.checkCompanyExist(companyId);

		List<CompanyJobAdvertisementSummariesDto> companyJobAdvertisementSummariesList = new ArrayList<>();
		Company company = this.loadCompanyById(companyId);

		if (!company.getJobAdvertisements().isEmpty()) {
			for (JobAdvertisement jobAdvertisement : company.getJobAdvertisements()) {
				CompanyJobAdvertisementSummariesDto companyJobAdvertisementSummariesDto = new CompanyJobAdvertisementSummariesDto();
				companyJobAdvertisementSummariesDto.setId(jobAdvertisement.getId());
				companyJobAdvertisementSummariesDto.setName(jobAdvertisement.getName());
				companyJobAdvertisementSummariesDto.setSummary(jobAdvertisement.getSummary());
				companyJobAdvertisementSummariesList.add(companyJobAdvertisementSummariesDto);
			}
		}

		return companyJobAdvertisementSummariesList;
	}

	@Override
	public Company loadCompanyById(Long id) {
		return this.companyProfileRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(Message.COMPANY_NOT_FOUND));
	}

}
