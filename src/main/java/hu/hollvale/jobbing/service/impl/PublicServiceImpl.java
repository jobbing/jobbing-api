package hu.hollvale.jobbing.service.impl;

import java.util.Arrays;

import javax.transaction.Transactional;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.config.JwtTokenUtil;
import hu.hollvale.jobbing.dto.UserRegisterDto;
import hu.hollvale.jobbing.dto.company.CompanyRegisterDto;
import hu.hollvale.jobbing.dto.user.LoginDto;
import hu.hollvale.jobbing.dtol.authentication.AuthenticationRequest;
import hu.hollvale.jobbing.enums.HolderType;
import hu.hollvale.jobbing.enums.Message;
import hu.hollvale.jobbing.exception.InvalidInputException;
import hu.hollvale.jobbing.model.company.Company;
import hu.hollvale.jobbing.model.company.CompanyEmployee;
import hu.hollvale.jobbing.model.user.Role;
import hu.hollvale.jobbing.model.user.User;
import hu.hollvale.jobbing.repository.CompanyEmployeeRepository;
import hu.hollvale.jobbing.repository.CompanyProfileRepository;
import hu.hollvale.jobbing.repository.PublicRepository;
import hu.hollvale.jobbing.repository.RoleRepository;
import hu.hollvale.jobbing.service.JobseekerHelperService;
import hu.hollvale.jobbing.service.PublicService;
import hu.hollvale.jobbing.service.UserService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class PublicServiceImpl implements PublicService {

	private final PublicRepository publicRepository;
	private final UserService userService;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final CompanyProfileRepository companyProfileRepository;
	private final CompanyEmployeeRepository companyEmployeeRepository;
	private final UserDetailsService userDetailsService;
	private final RoleRepository roleRepository;
	private final JobseekerHelperService jobseekerHelperService;
	private final ValidationService validationService;
	private final JwtTokenUtil jwtTokenUtil;

	@Override
	public Long registerUser(UserRegisterDto userRegisterDto) {
		// ez az álláskereső user és jobseeker táblájának a létrehozása
		final String roleName = "JOBSEEKER";

		User user = new User();

		this.validationService.checkUsernameNotExist(userRegisterDto.getUsername());
		this.validationService.checkUserEmailNotExist(userRegisterDto.getEmail());
		this.validationService.checkEmailFormatValid(userRegisterDto.getEmail());
		this.validationService.checkPasswordValid(userRegisterDto.getPassword());

		user.setPassword(this.bCryptPasswordEncoder.encode(userRegisterDto.getPassword()));
		user.setName(userRegisterDto.getName());
		user.setEmail(userRegisterDto.getEmail());
		user.setUsername(userRegisterDto.getUsername());
		Role role = this.roleRepository.findByName(roleName);
		System.err.println(role.getName());
		user.setRoles(Arrays.asList(role));

		Long userId = this.publicRepository.save(user).getId();

		return this.jobseekerHelperService.createJobseeker(userId);
	}

	@Override
	public LoginDto loginUser(AuthenticationRequest authenticationRequest) {
		try {
			new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),
					authenticationRequest.getPassword());

			final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

			if (!this.bCryptPasswordEncoder.matches(authenticationRequest.getPassword(), userDetails.getPassword())) {
				throw new AuthenticationServiceException("Invalid username or password");
			}

			final String jwt = jwtTokenUtil.generateToken(userDetails);

			LoginDto loginDto = new LoginDto();
			loginDto.setJwt(jwt);
			loginDto.setHolderType(this.holderType(authenticationRequest.getUsername()));

			return loginDto;

		} catch (InvalidInputException e) {
			throw new InvalidInputException(Message.INVALID_INPUTS);
		}

	}

	@Override
	public Long registerCompany(CompanyRegisterDto companyRegisterDto) {

		this.validationService.checkUsernameNotExist(companyRegisterDto.getUsername());
		this.validationService.checkUserEmailNotExist(companyRegisterDto.getEmail());
		this.validationService.checkEmailFormatValid(companyRegisterDto.getEmail());
		this.validationService.checkPasswordValid(companyRegisterDto.getPassword());

		// cég regisztrációkor regisztrál egy usert is, aki egy employee
		companyRegisterDto.setPassword(this.bCryptPasswordEncoder.encode(companyRegisterDto.getPassword()));
		User user = User.fromUser(companyRegisterDto);
		Role role = this.roleRepository.findByName("COMPANY_ADMIN");
		user.setRoles(Arrays.asList(role));
		this.publicRepository.save(user);

		Company company = new Company();
		company.setName(companyRegisterDto.getCompanyName());
		Long companyId = this.companyProfileRepository.save(company).getId();

		CompanyEmployee companyEmployee = new CompanyEmployee();
		companyEmployee.setUser(user);
		companyEmployee.setCompany(company);
		this.companyEmployeeRepository.save(companyEmployee);

		return companyId;
	}

	private HolderType holderType(String username) {
		User user = this.userService.loadUserObjByUsername(username);

		return user.getRoles().get(0).getHolder();
	}
}
