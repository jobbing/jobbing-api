package hu.hollvale.jobbing.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import hu.hollvale.jobbing.dto.company.JobAdvertisementDto;
import hu.hollvale.jobbing.model.company.City;
import hu.hollvale.jobbing.model.company.Company;
import hu.hollvale.jobbing.model.company.JobAdvertisement;
import hu.hollvale.jobbing.model.company.WorkCategory;
import hu.hollvale.jobbing.model.company.WorkType;
import hu.hollvale.jobbing.repository.JobAdvertisementRepository;
import hu.hollvale.jobbing.repository.WorkCategoryRepository;
import hu.hollvale.jobbing.service.CompanyHelperService;
import hu.hollvale.jobbing.service.JobAdvertisementHelperService;
import hu.hollvale.jobbing.service.JobAdvertisementService;
import hu.hollvale.jobbing.service.ValidationService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class JobAdvertisementServiceImpl implements JobAdvertisementService {

	private final JobAdvertisementRepository jobAdvertisementRepository;
	private final WorkCategoryRepository workCategoryRepository;
	private final JobAdvertisementHelperService jobAdvertisementHelperService;
	private final CompanyHelperService companyHelperService;
	private final ValidationService validationService;

	@Override
	@PreAuthorize("@securityServiceImpl.hasAnyPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_CREATE_JOB_ADVERTISEMENT,"
			+ " T(hu.hollvale.jobbing.enums.PermissionType).CAN_MODIFY_JOB_ADVERTISEMENT)")
	public Long saveJobAdvertisement(JobAdvertisementDto jobAdvertisementDto) {

		JobAdvertisement jobAdvertisement = this.loadJobAdvertisement(jobAdvertisementDto);
		this.validationService.checkInputCompanyValid(jobAdvertisement.getCompany().getId());

		jobAdvertisement = this.addListValuesToAdvertisement(jobAdvertisementDto, jobAdvertisement);

		return this.jobAdvertisementRepository.save(jobAdvertisement).getId();
	}

	@Override
	@PreAuthorize("@securityServiceImpl.hasPermission(T(hu.hollvale.jobbing.enums.PermissionType).CAN_DELETE_JOB_ADVERTISEMENT)")
	public void deleteJobAdvertisement(Long jobAdvertisementId) {
		JobAdvertisement jobAdvertisement = this.jobAdvertisementHelperService
				.loadJobAdvertisementById(jobAdvertisementId);
		this.validationService.checkInputCompanyValid(jobAdvertisement.getCompany().getId());

		this.jobAdvertisementRepository.delete(jobAdvertisement);
	}

	private JobAdvertisement loadJobAdvertisement(JobAdvertisementDto jobAdvertisementDto) {

		JobAdvertisement jobAdvertisement = new JobAdvertisement();

		if (jobAdvertisementDto.getId() == null) { // új
			Company company = this.companyHelperService.loadCompanyById(jobAdvertisementDto.getCompanyId());
			jobAdvertisement.setCompany(company);
		} else {
			jobAdvertisement = this.jobAdvertisementHelperService.loadJobAdvertisementById(jobAdvertisementDto.getId());

		}
		jobAdvertisement.setName(jobAdvertisementDto.getName());
		jobAdvertisement.setPlace(jobAdvertisementDto.getPlace());
		jobAdvertisement.setSummary(jobAdvertisementDto.getSummary());
		jobAdvertisement.setTextEditor(jobAdvertisementDto.getTextEditor());

		return jobAdvertisement;

	}

	private JobAdvertisement addListValuesToAdvertisement(JobAdvertisementDto jobAdvertisementDto,
			JobAdvertisement jobAdvertisement) {

		List<City> cityList = this.jobAdvertisementRepository
				.getCitiesByIdList(jobAdvertisementDto.getCityIdList());
		jobAdvertisement.setCities(cityList);

		List<WorkCategory> workCategoryList = this.jobAdvertisementRepository
				.getWorkCategoriesByIdList(jobAdvertisementDto.getWorkCategoryIdList());
		jobAdvertisement.setWorkCategories(workCategoryList);

		List<WorkType> workTypeList = this.jobAdvertisementRepository
				.getWorkTypesByIdList(jobAdvertisementDto.getWorkTypeIdList());
		jobAdvertisement.setWorkTypes(workTypeList);

		addnewWorkCategories(jobAdvertisementDto.getNewWorkCategoryNameList(), jobAdvertisement);

		return jobAdvertisement;
	}

	private JobAdvertisement addnewWorkCategories(List<String> workCategoryNameList,
			JobAdvertisement jobAdvertisement) {

		for (String name : workCategoryNameList) {
			WorkCategory workCategory = new WorkCategory();
			workCategory.setName(name);
			this.workCategoryRepository.save(workCategory);
			jobAdvertisement.getWorkCategories().add(workCategory);
		}

		return jobAdvertisement;
	}
}
