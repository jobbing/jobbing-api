package hu.hollvale.jobbing.service;

import java.util.List;

import hu.hollvale.jobbing.dto.DrivingLicenceListDto;
import hu.hollvale.jobbing.dto.ExperienceListDto;
import hu.hollvale.jobbing.dto.LanguageExamListDto;
import hu.hollvale.jobbing.dto.ProfileDataListDto;
import hu.hollvale.jobbing.dto.QualityListDto;
import hu.hollvale.jobbing.model.jobseeker.DrivingLicence;
import hu.hollvale.jobbing.model.jobseeker.Experience;
import hu.hollvale.jobbing.model.jobseeker.Jobseeker;
import hu.hollvale.jobbing.model.jobseeker.LanguageExam;
import hu.hollvale.jobbing.model.jobseeker.Quality;

public interface JobseekerHelperService {

	public Jobseeker loadJobseekerById(Long id);

	public DrivingLicence loadDrivingLicenceById(Long id);

	public LanguageExam loadLanguageExamById(Long id);

	public Experience loadExperienceById(Long id);

	public Quality loadQualityById(Long id);

	public ProfileDataListDto loadProfileData(String username);

	public ProfileDataListDto companyLoadJobseekerProfileData(Long jobseekerId);

	public List<DrivingLicenceListDto> getDrivingLicences(Long jobseekerId);

	public List<LanguageExamListDto> getLanguageExams(Long jobseekerId);

	public List<QualityListDto> getQualities(Long jobseekerId);

	public List<ExperienceListDto> getExperiences(Long jobseekerId);

	public Long createJobseeker(Long userId);
}
