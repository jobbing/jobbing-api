package hu.hollvale.jobbing.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import hu.hollvale.jobbing.dto.user.EmailChangeDto;
import hu.hollvale.jobbing.dto.user.GetMyDatasDto;
import hu.hollvale.jobbing.dto.user.JobseekerSettingsDto;
import hu.hollvale.jobbing.dto.user.NameChangeDto;
import hu.hollvale.jobbing.dto.user.PasswordChangeDto;
import hu.hollvale.jobbing.dto.user.UsernameChangeDto;
import hu.hollvale.jobbing.enums.HolderType;
import hu.hollvale.jobbing.model.user.Role;
import hu.hollvale.jobbing.model.user.User;

public interface UserService {

	void deleteUser(String username);

	void changeUsername(UsernameChangeDto usernameChangeDto);

	void changeName(NameChangeDto nameChangeDto);

	void changePassword(PasswordChangeDto passwordChangeDto);

	void changeEmail(EmailChangeDto emailChangeDto);

	public JobseekerSettingsDto getUserDatasToSettings(String username);

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

	public User loadUserById(Long id);

	public User loadUserObjByUsername(String username);

	public Role loadRoleById(Long id);

	public GetMyDatasDto getMyDatas(HolderType holderType);

}
