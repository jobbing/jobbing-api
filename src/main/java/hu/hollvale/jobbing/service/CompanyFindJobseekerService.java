package hu.hollvale.jobbing.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import hu.hollvale.jobbing.dto.company.JobseekerDto;
import hu.hollvale.jobbing.dto.filter.JobseekerFilter;

public interface CompanyFindJobseekerService {

	Page<JobseekerDto> pageJobseekers(JobseekerFilter jobseekerFilter, Pageable pageable);

}
