package hu.hollvale.jobbing.service;

import hu.hollvale.jobbing.dto.match.CompanyMatchDto;

public interface MatchService {

	public Long jobseekeerAcceptCompany(CompanyMatchDto companyMatchDto);

	public Long jobseekeerRejectCompany(CompanyMatchDto companyMatchDto);

	public void companyAcceptJobseekeer(CompanyMatchDto companyMatchDto);

	public void companyRejectJobseekeer(CompanyMatchDto companyMatchDto);

}
