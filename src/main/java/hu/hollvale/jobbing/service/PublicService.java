package hu.hollvale.jobbing.service;

import hu.hollvale.jobbing.dto.UserRegisterDto;
import hu.hollvale.jobbing.dto.company.CompanyRegisterDto;
import hu.hollvale.jobbing.dto.user.LoginDto;
import hu.hollvale.jobbing.dtol.authentication.AuthenticationRequest;

public interface PublicService {

	Long registerUser(UserRegisterDto userRegisterDto);

	LoginDto loginUser(AuthenticationRequest authenticationRequest);

	Long registerCompany(CompanyRegisterDto companyRegisterDto);

}
