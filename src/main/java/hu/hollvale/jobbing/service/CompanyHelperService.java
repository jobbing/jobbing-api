package hu.hollvale.jobbing.service;

import java.util.List;

import hu.hollvale.jobbing.dto.company.CompanyJobAdvertisementListDto;
import hu.hollvale.jobbing.dto.company.CompanyJobAdvertisementSummariesDto;
import hu.hollvale.jobbing.dto.company.CompanyProfileDataListDto;
import hu.hollvale.jobbing.dto.company.JobseekerDto;
import hu.hollvale.jobbing.model.company.Company;

public interface CompanyHelperService {

	boolean companyEmailExist(String email);

	CompanyProfileDataListDto loadProfileData(Long companyId);

	List<CompanyJobAdvertisementListDto> loadCompanyJobAdvertisements(Long companyId);

	List<CompanyJobAdvertisementSummariesDto> loadCompanyJobAdvertisementSummaries(Long companyId);

	public Company loadCompanyById(Long id);

	List<JobseekerDto> getMatchedJobseekers(Long companyId);

}
