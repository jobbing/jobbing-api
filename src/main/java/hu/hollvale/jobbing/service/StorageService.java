package hu.hollvale.jobbing.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public interface StorageService {

	public String saveFile(String fileBasePath, MultipartFile file) throws IOException;

	public String saveFile(String fileBasePath, MultipartFile file, String fileNamePrefix) throws IOException;

	void copyFile(String source, String targetBase, String fileName) throws IOException;

	void deleteFile(String path) throws IOException;

	void archiveFile(String source, String targetBase, String fileName) throws IOException;

	String getRootLocation();

	String getCvFolder();

	String getCompanyFolder();

	String getPathDelimeter();

	String getProfilFolder();

	String getDeletedProfilFolder();

	String getDeletedCvFolder();

	String getDeletedCompanyFolder();
}
