package hu.hollvale.jobbing.service;

import hu.hollvale.jobbing.enums.PermissionType;

public interface SecurityService {

	public boolean hasPermission(PermissionType permission);

	public boolean hasAnyPermission(PermissionType... permissions);
}
