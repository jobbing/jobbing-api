package hu.hollvale.jobbing.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "app")
public class ApplicationMediaProperties {

	private String rootLocation;
	private String cvFolder;
	private String companyFolder;
	private String profilFolder;
	private String deletedCvFolder;
	private String deletedCompanyFolder;
	private String deletedProfilFolder;

}
