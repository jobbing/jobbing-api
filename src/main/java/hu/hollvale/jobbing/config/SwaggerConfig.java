package hu.hollvale.jobbing.config;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;

import io.swagger.annotations.ApiParam;
import lombok.Getter;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		//// @formatter:off

		return new Docket(DocumentationType.SWAGGER_2).securitySchemes(Arrays.asList(this.apiKey()))
				.securityContexts(Arrays.asList(SecurityContext.builder()
						.securityReferences(Collections.singletonList(
								SecurityReference.builder().reference("JWT").scopes(new AuthorizationScope[0]).build()))
						.build()))
				.select()
				.apis(RequestHandlerSelectors
					.any())
				.build()
				.directModelSubstitute(Pageable.class, SwaggerPageable.class)
				.apiInfo(apiEndPointsInfo());

		 
		// @formatter:on

	}

	private ApiKey apiKey() {
		return new ApiKey("JWT", "Authorization", "header");
	}

	private ApiInfo apiEndPointsInfo() {
		return new ApiInfoBuilder().title("JOBBING REST API")
				.description("Szakdolgozat: állás és alkalmazottkereső weboldal")
				.contact(new Contact("Hollósi Valentin", "jobbing.hu", "hollosi.valentin3@gmail.com"))
				.license("Apache 2.0").licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html").version("1.0.0")
				.build();
	}

	@Getter
	private static class SwaggerPageable {

		@ApiParam(value = "Number of records per page", example = "0")
		@Nullable
		private Integer size;

		@ApiParam(value = "Results page you want to retrieve (0..N)", example = "0")
		@Nullable
		private Integer page;

		@ApiParam(value = "Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.")
		@Nullable
		private String sort;

	}
}
