package hu.hollvale.jobbing.config;

import org.jooq.conf.RenderNameCase;
import org.jooq.conf.RenderQuotedNames;
import org.jooq.conf.Settings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@Configuration
public class PersistenceConfig {

	// @formatter:off
	@Bean
	Settings jooqSettings() {
		return new Settings().withRenderQuotedNames(RenderQuotedNames.NEVER)
				.withRenderNameCase(RenderNameCase.LOWER_IF_UNQUOTED);
	}
	
	// @formatter:on

}
