package hu.hollvale.jobbing.model.jobseeker;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import hu.hollvale.jobbing.dto.LanguageExamDto;
import hu.hollvale.jobbing.enums.LanguageType;
import hu.hollvale.jobbing.enums.LevelType;
import hu.hollvale.jobbing.model.AbstractModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Audited
@Table(name = "t_language_exam")
public class LanguageExam extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@Enumerated(EnumType.STRING)
	@Column(name = "language", length = 45, nullable = false)
	private LanguageType language;

	@Enumerated(EnumType.STRING)
	@Column(name = "level", length = 45, nullable = false)
	private LevelType level;

	public static LanguageExam fromLanguageExam(LanguageExamDto languageExamDto) {
		LanguageExam languageExam = new LanguageExam();
		languageExam.setLanguage(languageExamDto.getLanguage());
		languageExam.setLevel(languageExamDto.getLevel());

		return languageExam;
	}

}
