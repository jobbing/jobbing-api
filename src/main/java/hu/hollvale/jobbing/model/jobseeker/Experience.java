package hu.hollvale.jobbing.model.jobseeker;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import hu.hollvale.jobbing.dto.ExperienceDto;
import hu.hollvale.jobbing.model.AbstractModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Audited
@Table(name = "t_experience")
public class Experience extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@Column(name = "name", length = 45, nullable = false)
	private String name;

	@Column(name = "start_date", length = 45, nullable = false)
	private Date startDate;

	@Column(name = "end_date", length = 45, nullable = false)
	private Date endDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobseeker_id")
	private Jobseeker jobseeker;

	public static Experience fromExperience(ExperienceDto experienceDto) {
		Experience experience = new Experience();
		experience.setName(experienceDto.getName());
		experience.setStartDate(experienceDto.getStartDate());
		experience.setEndDate(experienceDto.getEndDate());

		return experience;
	}
}
