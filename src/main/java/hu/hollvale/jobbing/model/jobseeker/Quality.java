package hu.hollvale.jobbing.model.jobseeker;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import hu.hollvale.jobbing.dto.QualityDto;
import hu.hollvale.jobbing.enums.QualityType;
import hu.hollvale.jobbing.model.AbstractModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Audited
@Table(name = "t_quality")
public class Quality extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@Column(name = "school", nullable = false, length = 100)
	private String school;

	@Column(name = "city", nullable = false, length = 45)
	private String city;

	@Column(name = "year", nullable = false)
	private Integer year;

	@Enumerated(EnumType.STRING)
	@Column(name = "type", nullable = false)
	private QualityType type;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobseeker_id")
	private Jobseeker jobseeker;

	public static Quality fromQuality(QualityDto qualityDto) {
		Quality quality = new Quality();
		quality.setSchool(qualityDto.getSchool());
		quality.setCity(qualityDto.getCity());
		quality.setType(qualityDto.getType());
		quality.setYear(qualityDto.getYear());

		return quality;
	};
}
