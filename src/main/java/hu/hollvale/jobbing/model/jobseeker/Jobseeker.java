package hu.hollvale.jobbing.model.jobseeker;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import hu.hollvale.jobbing.model.AbstractModel;
import hu.hollvale.jobbing.model.match.MatchByAdvertisement;
import hu.hollvale.jobbing.model.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Audited
@Table(name = "t_jobseeker")
public class Jobseeker extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@Column(name = "picture", nullable = true)
	private String picture;

	@Column(name = "introduction", nullable = true)
	private String introduction;

	@Column(name = "work_i_look_for", nullable = true)
	private String workILookFor;

	@Column(name = "CV", nullable = true)
	private String CV;

	@Column(name = "birth_place", length = 45, nullable = true)
	private String birthPlace;

	@Column(name = "birth_time", nullable = true)
	private Date birthTime;

	@Column(name = "address", length = 45, nullable = true)
	private String address;

	@Column(name = "residence", length = 45, nullable = true)
	private String residence;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@ManyToMany
	@JoinTable(name = "jobseeker_x_driving_licence", joinColumns = @JoinColumn(name = "jobseeker_id"), inverseJoinColumns = @JoinColumn(name = "driving_licence_id"))
	private List<DrivingLicence> drivingLicences;

	@ManyToMany
	@JoinTable(name = "jobseeker_x_language_exam", joinColumns = @JoinColumn(name = "jobseeker_id"), inverseJoinColumns = @JoinColumn(name = "language_exam_id"))
	private List<LanguageExam> languageExams;

	@OneToMany(mappedBy = "jobseeker")
	private List<Experience> experiences = new ArrayList<>();

	@OneToMany(mappedBy = "jobseeker")
	private List<Quality> qualities = new ArrayList<>();

	@OneToMany(mappedBy = "jobseeker")
	private List<MatchByAdvertisement> matchesByAdvertisement = new ArrayList<>();
}
