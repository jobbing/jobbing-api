package hu.hollvale.jobbing.model.company;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import hu.hollvale.jobbing.model.AbstractModel;
import hu.hollvale.jobbing.model.match.MatchByAdvertisement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Audited
@Table(name = "t_job_advertisement")
public class JobAdvertisement extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@Column(name = "name", length = 45, nullable = true)
	private String name;

	@Column(name = "place", length = 100, nullable = true)
	private String place;

	@Column(name = "summary", length = 200, nullable = true)
	private String summary;

	@Column(name = "text_editor", nullable = true)
	private String textEditor;

	@OneToMany(mappedBy = "jobAdvertisement")
	private List<MatchByAdvertisement> matchesByAdvertisement;

	@ManyToMany
	@JoinTable(name = "job_advertisement_x_work_category", joinColumns = @JoinColumn(name = "job_advertisement_id"), inverseJoinColumns = @JoinColumn(name = "work_category_id"))
	private List<WorkCategory> workCategories;

	@ManyToMany
	@JoinTable(name = "job_advertisement_x_work_type", joinColumns = @JoinColumn(name = "job_advertisement_id"), inverseJoinColumns = @JoinColumn(name = "work_type_id"))
	private List<WorkType> workTypes;

	@ManyToMany
	@JoinTable(name = "job_advertisement_x_city", joinColumns = @JoinColumn(name = "job_advertisement_id"), inverseJoinColumns = @JoinColumn(name = "city_id"))
	private List<City> cities;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id")
	private Company company;
}
