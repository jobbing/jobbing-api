package hu.hollvale.jobbing.model.company;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.envers.Audited;

import hu.hollvale.jobbing.model.AbstractModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Audited
@Table(name = "t_company")
public class Company extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Company name is mandatory")
	@Column(name = "name", length = 45, nullable = false)
	private String name;

	@Column(name = "email", length = 100, nullable = true)
	private String email;

	@Column(name = "logo", length = 255, nullable = true)
	private String logo;

	@Column(name = "introduction", nullable = true)
	private String introduction;

	@Column(name = "phone_number", length = 20, nullable = true)
	private String phoneNumber;

	@Column(name = "address", length = 100, nullable = true)
	private String address;

	@Column(name = "contact_person", length = 45, nullable = true)
	private String contactPerson;

	@OneToMany(mappedBy = "company")
	private List<JobAdvertisement> jobAdvertisements = new ArrayList<>();

	@OneToMany(mappedBy = "company")
	private List<CompanyEmployee> employees = new ArrayList<>();

}
