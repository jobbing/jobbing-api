package hu.hollvale.jobbing.model.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import hu.hollvale.jobbing.model.AbstractModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Audited
@Table(name = "t_city")
public class City extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@Column(name = "name", nullable = false)
	private String name;
}
