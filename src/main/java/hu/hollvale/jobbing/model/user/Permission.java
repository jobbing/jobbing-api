package hu.hollvale.jobbing.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import hu.hollvale.jobbing.model.AbstractModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Audited
@Table(name = "t_permission")
public class Permission extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@Column(name = "name", length = 45, nullable = false, unique = true)
	private String name;

}
