package hu.hollvale.jobbing.model.user;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import hu.hollvale.jobbing.dto.company.CompanyRegisterDto;
import hu.hollvale.jobbing.dto.company.EmployeeRegisterDto;
import hu.hollvale.jobbing.model.AbstractModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Audited
@Table(name = "t_user")
public class User extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Name is mandatory")
	@Column(name = "name", length = 45, nullable = true)
	private String name;

	@Column(name = "username", length = 45, nullable = false, unique = true)
	private String username;

	@Column(name = "email", length = 100, nullable = false, unique = true)
	private String email;

	@JsonProperty(access = Access.WRITE_ONLY)
	@Column(name = "password", length = 100, nullable = false)
	private String password;

	@ManyToMany
	@JoinTable(name = "user_x_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private List<Role> roles;

	public static User fromUser(CompanyRegisterDto companyRegisterDto) {
		User user = new User();
		user.setName(companyRegisterDto.getName());
		user.setUsername(companyRegisterDto.getUsername());
		user.setEmail(companyRegisterDto.getEmail());
		user.setPassword(companyRegisterDto.getPassword());
		return user;
	}

	public static User fromUser(EmployeeRegisterDto employeeRegisterDto) {
		User user = new User();
		user.setUsername(employeeRegisterDto.getUsername());
		user.setName(employeeRegisterDto.getName());
		user.setEmail(employeeRegisterDto.getEmail());
		user.setPassword(employeeRegisterDto.getPassword());
		return user;
	}

}
