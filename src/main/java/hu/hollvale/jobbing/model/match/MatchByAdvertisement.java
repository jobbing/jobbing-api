package hu.hollvale.jobbing.model.match;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import hu.hollvale.jobbing.enums.MatchStateType;
import hu.hollvale.jobbing.model.AbstractModel;
import hu.hollvale.jobbing.model.company.Company;
import hu.hollvale.jobbing.model.company.JobAdvertisement;
import hu.hollvale.jobbing.model.jobseeker.Jobseeker;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Audited
@Table(name = "t_match_by_advertisement")
public class MatchByAdvertisement extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobseeker_id")
	private Jobseeker jobseeker;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "job_advertisement_id")
	private JobAdvertisement jobAdvertisement;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id")
	private Company company;

	@Enumerated(EnumType.STRING)
	@Column(name = "state", nullable = false)
	private MatchStateType state;

}
