package hu.hollvale.jobbing.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

@Data
@EntityListeners(AuditingEntityListener.class)
@MappedSuperclass
public class AbstractAuditedModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty(access = Access.READ_ONLY)
	@CreatedDate
	@Column(name = "creation_time", nullable = false, updatable = false)
	private LocalDateTime creationTime;

	@JsonIgnore
	@LastModifiedDate
	@Column(name = "modified_time")
	private LocalDateTime modifiedTime;

	@JsonIgnore
	@CreatedBy
	@Column(name = "created_by", updatable = false)
	private String createdBy;

	@JsonIgnore
	@LastModifiedBy
	@Column(name = "modified_by")
	private String modifiedBy;

}
