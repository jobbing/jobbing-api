FROM maven:3.8.1-openjdk-15 AS MAVEN_BUILD
ARG SPRING_ACTIVE_PROFILE
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn package -DskipTests -B -e -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE

FROM openjdk:15-jdk-slim
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/jobbing-api-*.jar /app/jobbing-api.jar
ENTRYPOINT ["java", "-jar", "jobbing-api.jar"]