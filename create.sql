create table job_advertisement_x_city (job_advertisement_id bigint not null, city_id bigint not null) engine=InnoDB;
create table job_advertisement_x_city_aud (rev integer not null, job_advertisement_id bigint not null, city_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, city_id)) engine=InnoDB;
create table job_advertisement_x_work_category (job_advertisement_id bigint not null, work_category_id bigint not null) engine=InnoDB;
create table job_advertisement_x_work_category_aud (rev integer not null, job_advertisement_id bigint not null, work_category_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, work_category_id)) engine=InnoDB;
create table job_advertisement_x_work_type (job_advertisement_id bigint not null, work_type_id bigint not null) engine=InnoDB;
create table job_advertisement_x_work_type_aud (rev integer not null, job_advertisement_id bigint not null, work_type_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, work_type_id)) engine=InnoDB;
create table jobseeker_x_driving_licence (jobseeker_id bigint not null, driving_licence_id bigint not null) engine=InnoDB;
create table jobseeker_x_driving_licence_aud (rev integer not null, jobseeker_id bigint not null, driving_licence_id bigint not null, revtype tinyint, primary key (rev, jobseeker_id, driving_licence_id)) engine=InnoDB;
create table jobseeker_x_language_exam (jobseeker_id bigint not null, language_exam_id bigint not null) engine=InnoDB;
create table jobseeker_x_language_exam_aud (rev integer not null, jobseeker_id bigint not null, language_exam_id bigint not null, revtype tinyint, primary key (rev, jobseeker_id, language_exam_id)) engine=InnoDB;
create table revinfo (rev integer not null auto_increment, revtstmp bigint, primary key (rev)) engine=InnoDB;
create table role_x_permission (role_id bigint not null, permission_id bigint not null) engine=InnoDB;
create table role_x_permission_aud (rev integer not null, role_id bigint not null, permission_id bigint not null, revtype tinyint, primary key (rev, role_id, permission_id)) engine=InnoDB;
create table t_city (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB;
create table t_city_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB;
create table t_company (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), address varchar(100), contact_person varchar(45), email varchar(100), introduction varchar(255), logo varchar(255), name varchar(45) not null, phone_number varchar(20), primary key (id)) engine=InnoDB;
create table t_company_aud (id bigint not null, rev integer not null, revtype tinyint, address varchar(100), contact_person varchar(45), email varchar(100), introduction varchar(255), logo varchar(255), name varchar(45), phone_number varchar(20), primary key (id, rev)) engine=InnoDB;
create table t_company_employee (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), company_id bigint, user_id bigint, primary key (id)) engine=InnoDB;
create table t_company_employee_aud (id bigint not null, rev integer not null, revtype tinyint, company_id bigint, user_id bigint, primary key (id, rev)) engine=InnoDB;
create table t_driving_licence (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), value varchar(45) not null, primary key (id)) engine=InnoDB;
create table t_driving_licence_aud (id bigint not null, rev integer not null, revtype tinyint, value varchar(45), primary key (id, rev)) engine=InnoDB;
create table t_experience (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), end_date date not null, name varchar(45) not null, start_date date not null, jobseeker_id bigint, primary key (id)) engine=InnoDB;
create table t_experience_aud (id bigint not null, rev integer not null, revtype tinyint, end_date date, name varchar(45), start_date date, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB;
create table t_job_advertisement (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45), place varchar(100), summary varchar(200), text_editor varchar(255), company_id bigint, primary key (id)) engine=InnoDB;
create table t_job_advertisement_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), place varchar(100), summary varchar(200), text_editor varchar(255), company_id bigint, primary key (id, rev)) engine=InnoDB;
create table t_jobseeker (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), cv varchar(255), address varchar(45), birth_place varchar(45), birth_time date, introduction varchar(255), picture varchar(255), residence varchar(45), work_i_look_for varchar(255), user_id bigint, primary key (id)) engine=InnoDB;
create table t_jobseeker_aud (id bigint not null, rev integer not null, revtype tinyint, cv varchar(255), address varchar(45), birth_place varchar(45), birth_time date, introduction varchar(255), picture varchar(255), residence varchar(45), work_i_look_for varchar(255), user_id bigint, primary key (id, rev)) engine=InnoDB;
create table t_language_exam (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), language varchar(45) not null, level varchar(45) not null, primary key (id)) engine=InnoDB;
create table t_language_exam_aud (id bigint not null, rev integer not null, revtype tinyint, language varchar(45), level varchar(45), primary key (id, rev)) engine=InnoDB;
create table t_match (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), state varchar(255) not null, company_id bigint, jobseeker_id bigint, primary key (id)) engine=InnoDB;
create table t_match_aud (id bigint not null, rev integer not null, revtype tinyint, state varchar(255), company_id bigint, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB;
create table t_match_by_advertisement (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), state varchar(255) not null, company_id bigint, job_advertisement_id bigint, jobseeker_id bigint, primary key (id)) engine=InnoDB;
create table t_match_by_advertisement_aud (id bigint not null, rev integer not null, revtype tinyint, state varchar(255), company_id bigint, job_advertisement_id bigint, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB;
create table t_permission (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45) not null, primary key (id)) engine=InnoDB;
create table t_permission_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), primary key (id, rev)) engine=InnoDB;
create table t_quality (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), city varchar(45) not null, school varchar(100) not null, type varchar(255) not null, year integer not null, jobseeker_id bigint, primary key (id)) engine=InnoDB;
create table t_quality_aud (id bigint not null, rev integer not null, revtype tinyint, city varchar(45), school varchar(100), type varchar(255), year integer, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB;
create table t_role (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45) not null, primary key (id)) engine=InnoDB;
create table t_role_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), primary key (id, rev)) engine=InnoDB;
create table t_user (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), email varchar(100) not null, name varchar(45), password varchar(100) not null, username varchar(45) not null, primary key (id)) engine=InnoDB;
create table t_user_aud (id bigint not null, rev integer not null, revtype tinyint, email varchar(100), name varchar(45), password varchar(100), username varchar(45), primary key (id, rev)) engine=InnoDB;
create table t_work_category (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB;
create table t_work_category_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB;
create table t_work_type (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB;
create table t_work_type_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB;
create table user_x_role (user_id bigint not null, role_id bigint not null) engine=InnoDB;
create table user_x_role_aud (rev integer not null, user_id bigint not null, role_id bigint not null, revtype tinyint, primary key (rev, user_id, role_id)) engine=InnoDB;
alter table t_permission add constraint UK_sm6ir47566j150iradcgtsw9y unique (name);
alter table t_user add constraint UK_i6qjjoe560mee5ajdg7v1o6mi unique (email);
alter table t_user add constraint UK_jhib4legehrm4yscx9t3lirqi unique (username);
alter table job_advertisement_x_city add constraint FKye648e8ahxdq7wjxfieif9rw foreign key (city_id) references t_city (id);
alter table job_advertisement_x_city add constraint FKjogmhw4ump0240wl41vvubsjo foreign key (job_advertisement_id) references t_job_advertisement (id);
alter table job_advertisement_x_city_aud add constraint FKrst402tut0ben5cne9yonsja5 foreign key (rev) references revinfo (rev);
alter table job_advertisement_x_work_category add constraint FKdp0liv02fc5w4eidb7xprv2g4 foreign key (work_category_id) references t_work_category (id);
alter table job_advertisement_x_work_category add constraint FKs2aggnd9rl640cyg2d9jgysd8 foreign key (job_advertisement_id) references t_job_advertisement (id);
alter table job_advertisement_x_work_category_aud add constraint FKti44kt6vsbgv9fnl9qnvuwo3h foreign key (rev) references revinfo (rev);
alter table job_advertisement_x_work_type add constraint FKbx5gbe7bjaykfke6dwxe6vvgb foreign key (work_type_id) references t_work_type (id);
alter table job_advertisement_x_work_type add constraint FK1pq5rqudgrj910pjfyu6kfk1v foreign key (job_advertisement_id) references t_job_advertisement (id);
alter table job_advertisement_x_work_type_aud add constraint FK9k2eslrqrpn1fpw71a5rnu39v foreign key (rev) references revinfo (rev);
alter table jobseeker_x_driving_licence add constraint FKqbsbh9iw32eaqhf0j600uf8y foreign key (driving_licence_id) references t_driving_licence (id);
alter table jobseeker_x_driving_licence add constraint FKmkvmxlf9aihd6vangxkrw24m4 foreign key (jobseeker_id) references t_jobseeker (id);
alter table jobseeker_x_driving_licence_aud add constraint FK128kp89al9vcm6f2n6ab4gvrw foreign key (rev) references revinfo (rev);
alter table jobseeker_x_language_exam add constraint FKf9y7mvxxh1nv7899f61nl3djf foreign key (language_exam_id) references t_language_exam (id);
alter table jobseeker_x_language_exam add constraint FK4it7q6n6gw7h07cvpp42c5noa foreign key (jobseeker_id) references t_jobseeker (id);
alter table jobseeker_x_language_exam_aud add constraint FKnr0kvjxyp58d29pqlhr6wwat foreign key (rev) references revinfo (rev);
alter table role_x_permission add constraint FK4pwk3vbvrty2hpq3214okypwj foreign key (permission_id) references t_permission (id);
alter table role_x_permission add constraint FKhdni9x66uaq6n4xyav0nq7rg8 foreign key (role_id) references t_role (id);
alter table role_x_permission_aud add constraint FKfepsn3lx64g80xk5196t0n88t foreign key (rev) references revinfo (rev);
alter table t_city_aud add constraint FKr6exoiw6ycugv7ioyvj4f7w2i foreign key (rev) references revinfo (rev);
alter table t_company_aud add constraint FKvk9m3ms51chv4evfpafh18el foreign key (rev) references revinfo (rev);
alter table t_company_employee add constraint FKlxhe08mlnm9km18f44b5c8uco foreign key (company_id) references t_company (id);
alter table t_company_employee add constraint FK32o4qfp0oc3s8adsyt1a8jwb2 foreign key (user_id) references t_user (id);
alter table t_company_employee_aud add constraint FKiwr3ilj0s1bl0vn4t3af38iwx foreign key (rev) references revinfo (rev);
alter table t_driving_licence_aud add constraint FKtjpbrh57c2sc2jddr72t8ovgx foreign key (rev) references revinfo (rev);
alter table t_experience add constraint FKbmgtg7ymqptlaro4uyn99f3s5 foreign key (jobseeker_id) references t_jobseeker (id);
alter table t_experience_aud add constraint FKpkauemkd0ipdwwg1ok2uxprm1 foreign key (rev) references revinfo (rev);
alter table t_job_advertisement add constraint FKp1phbasyym1hg4rm3qwd02gow foreign key (company_id) references t_company (id);
alter table t_job_advertisement_aud add constraint FK14qhowamfj1kluill3nobr9ml foreign key (rev) references revinfo (rev);
alter table t_jobseeker add constraint FK4g0k867aundjt2vd7noxeka81 foreign key (user_id) references t_user (id);
alter table t_jobseeker_aud add constraint FKyao0b6d1p3cg5yp5kmse5tpw foreign key (rev) references revinfo (rev);
alter table t_language_exam_aud add constraint FKthor6eq84ftlbli9q4i9i9ely foreign key (rev) references revinfo (rev);
alter table t_match add constraint FK6aq8qete1jif3a0d9nvcmxr5b foreign key (company_id) references t_company (id);
alter table t_match add constraint FKkk0ccxu13u47ikt6yqm0un797 foreign key (jobseeker_id) references t_jobseeker (id);
alter table t_match_aud add constraint FKd46712n1sbwfxf5k8nh4giwx3 foreign key (rev) references revinfo (rev);
alter table t_match_by_advertisement add constraint FKejsv30d599usqtaf1xv3yqji2 foreign key (company_id) references t_company (id);
alter table t_match_by_advertisement add constraint FKlyxmssbhhrwv0fxqyp1hst371 foreign key (job_advertisement_id) references t_job_advertisement (id);
alter table t_match_by_advertisement add constraint FK80vgnfkkfsx2gh9cdhigvpsv1 foreign key (jobseeker_id) references t_jobseeker (id);
alter table t_match_by_advertisement_aud add constraint FK95si3guu1ljmoc03rcoycakhl foreign key (rev) references revinfo (rev);
alter table t_permission_aud add constraint FK3d8o1y33wn5kk5dcstp7yieoq foreign key (rev) references revinfo (rev);
alter table t_quality add constraint FKkpqbphvaxphe0xh78mw2f6qo1 foreign key (jobseeker_id) references t_jobseeker (id);
alter table t_quality_aud add constraint FKe3qn3ffpn61sq65xlmhwa4cu6 foreign key (rev) references revinfo (rev);
alter table t_role_aud add constraint FK9se3aaaqj71s2edoy9pnauitb foreign key (rev) references revinfo (rev);
alter table t_user_aud add constraint FKs4lwpwui7j4s8kl2bacqv0t0 foreign key (rev) references revinfo (rev);
alter table t_work_category_aud add constraint FK66ggi7odmllq47u1k6y5fx8nl foreign key (rev) references revinfo (rev);
alter table t_work_type_aud add constraint FKlhx8g72fxpmb69wub59h8xr2b foreign key (rev) references revinfo (rev);
alter table user_x_role add constraint FK5f3jmour16c9i1774c45h1s4p foreign key (role_id) references t_role (id);
alter table user_x_role add constraint FK9x2yn9ksiw3glh4oybx6ijilg foreign key (user_id) references t_user (id);
alter table user_x_role_aud add constraint FKahh8f69m1afcmhcynx27tpkqy foreign key (rev) references revinfo (rev);
create table job_advertisement_x_city (job_advertisement_id bigint not null, city_id bigint not null) engine=InnoDB
create table job_advertisement_x_city_aud (rev integer not null, job_advertisement_id bigint not null, city_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, city_id)) engine=InnoDB
create table job_advertisement_x_work_category (job_advertisement_id bigint not null, work_category_id bigint not null) engine=InnoDB
create table job_advertisement_x_work_category_aud (rev integer not null, job_advertisement_id bigint not null, work_category_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, work_category_id)) engine=InnoDB
create table job_advertisement_x_work_type (job_advertisement_id bigint not null, work_type_id bigint not null) engine=InnoDB
create table job_advertisement_x_work_type_aud (rev integer not null, job_advertisement_id bigint not null, work_type_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, work_type_id)) engine=InnoDB
create table jobseeker_x_driving_licence (jobseeker_id bigint not null, driving_licence_id bigint not null) engine=InnoDB
create table jobseeker_x_driving_licence_aud (rev integer not null, jobseeker_id bigint not null, driving_licence_id bigint not null, revtype tinyint, primary key (rev, jobseeker_id, driving_licence_id)) engine=InnoDB
create table jobseeker_x_language_exam (jobseeker_id bigint not null, language_exam_id bigint not null) engine=InnoDB
create table jobseeker_x_language_exam_aud (rev integer not null, jobseeker_id bigint not null, language_exam_id bigint not null, revtype tinyint, primary key (rev, jobseeker_id, language_exam_id)) engine=InnoDB
create table revinfo (rev integer not null auto_increment, revtstmp bigint, primary key (rev)) engine=InnoDB
create table role_x_permission (role_id bigint not null, permission_id bigint not null) engine=InnoDB
create table role_x_permission_aud (rev integer not null, role_id bigint not null, permission_id bigint not null, revtype tinyint, primary key (rev, role_id, permission_id)) engine=InnoDB
create table t_city (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB
create table t_city_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB
create table t_company (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), address varchar(100), contact_person varchar(45), email varchar(100), introduction varchar(255), logo varchar(255), name varchar(45) not null, phone_number varchar(20), primary key (id)) engine=InnoDB
create table t_company_aud (id bigint not null, rev integer not null, revtype tinyint, address varchar(100), contact_person varchar(45), email varchar(100), introduction varchar(255), logo varchar(255), name varchar(45), phone_number varchar(20), primary key (id, rev)) engine=InnoDB
create table t_company_employee (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), company_id bigint, user_id bigint, primary key (id)) engine=InnoDB
create table t_company_employee_aud (id bigint not null, rev integer not null, revtype tinyint, company_id bigint, user_id bigint, primary key (id, rev)) engine=InnoDB
create table t_driving_licence (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), value varchar(45) not null, primary key (id)) engine=InnoDB
create table t_driving_licence_aud (id bigint not null, rev integer not null, revtype tinyint, value varchar(45), primary key (id, rev)) engine=InnoDB
create table t_experience (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), end_date date not null, name varchar(45) not null, start_date date not null, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_experience_aud (id bigint not null, rev integer not null, revtype tinyint, end_date date, name varchar(45), start_date date, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_job_advertisement (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45), place varchar(100), summary varchar(200), text_editor varchar(255), company_id bigint, primary key (id)) engine=InnoDB
create table t_job_advertisement_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), place varchar(100), summary varchar(200), text_editor varchar(255), company_id bigint, primary key (id, rev)) engine=InnoDB
create table t_jobseeker (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), cv varchar(255), address varchar(45), birth_place varchar(45), birth_time date, introduction varchar(255), picture varchar(255), residence varchar(45), work_i_look_for varchar(255), user_id bigint, primary key (id)) engine=InnoDB
create table t_jobseeker_aud (id bigint not null, rev integer not null, revtype tinyint, cv varchar(255), address varchar(45), birth_place varchar(45), birth_time date, introduction varchar(255), picture varchar(255), residence varchar(45), work_i_look_for varchar(255), user_id bigint, primary key (id, rev)) engine=InnoDB
create table t_language_exam (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), language varchar(45) not null, level varchar(45) not null, primary key (id)) engine=InnoDB
create table t_language_exam_aud (id bigint not null, rev integer not null, revtype tinyint, language varchar(45), level varchar(45), primary key (id, rev)) engine=InnoDB
create table t_match (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), state varchar(255) not null, company_id bigint, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_match_aud (id bigint not null, rev integer not null, revtype tinyint, state varchar(255), company_id bigint, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_match_by_advertisement (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), state varchar(255) not null, company_id bigint, job_advertisement_id bigint, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_match_by_advertisement_aud (id bigint not null, rev integer not null, revtype tinyint, state varchar(255), company_id bigint, job_advertisement_id bigint, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_permission (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45) not null, primary key (id)) engine=InnoDB
create table t_permission_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), primary key (id, rev)) engine=InnoDB
create table t_quality (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), city varchar(45) not null, school varchar(100) not null, type varchar(255) not null, year integer not null, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_quality_aud (id bigint not null, rev integer not null, revtype tinyint, city varchar(45), school varchar(100), type varchar(255), year integer, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_role (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45) not null, primary key (id)) engine=InnoDB
create table t_role_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), primary key (id, rev)) engine=InnoDB
create table t_user (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), email varchar(100) not null, name varchar(45), password varchar(100) not null, username varchar(45) not null, primary key (id)) engine=InnoDB
create table t_user_aud (id bigint not null, rev integer not null, revtype tinyint, email varchar(100), name varchar(45), password varchar(100), username varchar(45), primary key (id, rev)) engine=InnoDB
create table t_work_category (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB
create table t_work_category_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB
create table t_work_type (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB
create table t_work_type_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB
create table user_x_role (user_id bigint not null, role_id bigint not null) engine=InnoDB
create table user_x_role_aud (rev integer not null, user_id bigint not null, role_id bigint not null, revtype tinyint, primary key (rev, user_id, role_id)) engine=InnoDB
alter table t_permission add constraint UK_sm6ir47566j150iradcgtsw9y unique (name)
alter table t_user add constraint UK_i6qjjoe560mee5ajdg7v1o6mi unique (email)
alter table t_user add constraint UK_jhib4legehrm4yscx9t3lirqi unique (username)
alter table job_advertisement_x_city add constraint FKye648e8ahxdq7wjxfieif9rw foreign key (city_id) references t_city (id)
alter table job_advertisement_x_city add constraint FKjogmhw4ump0240wl41vvubsjo foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table job_advertisement_x_city_aud add constraint FKrst402tut0ben5cne9yonsja5 foreign key (rev) references revinfo (rev)
alter table job_advertisement_x_work_category add constraint FKdp0liv02fc5w4eidb7xprv2g4 foreign key (work_category_id) references t_work_category (id)
alter table job_advertisement_x_work_category add constraint FKs2aggnd9rl640cyg2d9jgysd8 foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table job_advertisement_x_work_category_aud add constraint FKti44kt6vsbgv9fnl9qnvuwo3h foreign key (rev) references revinfo (rev)
alter table job_advertisement_x_work_type add constraint FKbx5gbe7bjaykfke6dwxe6vvgb foreign key (work_type_id) references t_work_type (id)
alter table job_advertisement_x_work_type add constraint FK1pq5rqudgrj910pjfyu6kfk1v foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table job_advertisement_x_work_type_aud add constraint FK9k2eslrqrpn1fpw71a5rnu39v foreign key (rev) references revinfo (rev)
alter table jobseeker_x_driving_licence add constraint FKqbsbh9iw32eaqhf0j600uf8y foreign key (driving_licence_id) references t_driving_licence (id)
alter table jobseeker_x_driving_licence add constraint FKmkvmxlf9aihd6vangxkrw24m4 foreign key (jobseeker_id) references t_jobseeker (id)
alter table jobseeker_x_driving_licence_aud add constraint FK128kp89al9vcm6f2n6ab4gvrw foreign key (rev) references revinfo (rev)
alter table jobseeker_x_language_exam add constraint FKf9y7mvxxh1nv7899f61nl3djf foreign key (language_exam_id) references t_language_exam (id)
alter table jobseeker_x_language_exam add constraint FK4it7q6n6gw7h07cvpp42c5noa foreign key (jobseeker_id) references t_jobseeker (id)
alter table jobseeker_x_language_exam_aud add constraint FKnr0kvjxyp58d29pqlhr6wwat foreign key (rev) references revinfo (rev)
alter table role_x_permission add constraint FK4pwk3vbvrty2hpq3214okypwj foreign key (permission_id) references t_permission (id)
alter table role_x_permission add constraint FKhdni9x66uaq6n4xyav0nq7rg8 foreign key (role_id) references t_role (id)
alter table role_x_permission_aud add constraint FKfepsn3lx64g80xk5196t0n88t foreign key (rev) references revinfo (rev)
alter table t_city_aud add constraint FKr6exoiw6ycugv7ioyvj4f7w2i foreign key (rev) references revinfo (rev)
alter table t_company_aud add constraint FKvk9m3ms51chv4evfpafh18el foreign key (rev) references revinfo (rev)
alter table t_company_employee add constraint FKlxhe08mlnm9km18f44b5c8uco foreign key (company_id) references t_company (id)
alter table t_company_employee add constraint FK32o4qfp0oc3s8adsyt1a8jwb2 foreign key (user_id) references t_user (id)
alter table t_company_employee_aud add constraint FKiwr3ilj0s1bl0vn4t3af38iwx foreign key (rev) references revinfo (rev)
alter table t_driving_licence_aud add constraint FKtjpbrh57c2sc2jddr72t8ovgx foreign key (rev) references revinfo (rev)
alter table t_experience add constraint FKbmgtg7ymqptlaro4uyn99f3s5 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_experience_aud add constraint FKpkauemkd0ipdwwg1ok2uxprm1 foreign key (rev) references revinfo (rev)
alter table t_job_advertisement add constraint FKp1phbasyym1hg4rm3qwd02gow foreign key (company_id) references t_company (id)
alter table t_job_advertisement_aud add constraint FK14qhowamfj1kluill3nobr9ml foreign key (rev) references revinfo (rev)
alter table t_jobseeker add constraint FK4g0k867aundjt2vd7noxeka81 foreign key (user_id) references t_user (id)
alter table t_jobseeker_aud add constraint FKyao0b6d1p3cg5yp5kmse5tpw foreign key (rev) references revinfo (rev)
alter table t_language_exam_aud add constraint FKthor6eq84ftlbli9q4i9i9ely foreign key (rev) references revinfo (rev)
alter table t_match add constraint FK6aq8qete1jif3a0d9nvcmxr5b foreign key (company_id) references t_company (id)
alter table t_match add constraint FKkk0ccxu13u47ikt6yqm0un797 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_match_aud add constraint FKd46712n1sbwfxf5k8nh4giwx3 foreign key (rev) references revinfo (rev)
alter table t_match_by_advertisement add constraint FKejsv30d599usqtaf1xv3yqji2 foreign key (company_id) references t_company (id)
alter table t_match_by_advertisement add constraint FKlyxmssbhhrwv0fxqyp1hst371 foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table t_match_by_advertisement add constraint FK80vgnfkkfsx2gh9cdhigvpsv1 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_match_by_advertisement_aud add constraint FK95si3guu1ljmoc03rcoycakhl foreign key (rev) references revinfo (rev)
alter table t_permission_aud add constraint FK3d8o1y33wn5kk5dcstp7yieoq foreign key (rev) references revinfo (rev)
alter table t_quality add constraint FKkpqbphvaxphe0xh78mw2f6qo1 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_quality_aud add constraint FKe3qn3ffpn61sq65xlmhwa4cu6 foreign key (rev) references revinfo (rev)
alter table t_role_aud add constraint FK9se3aaaqj71s2edoy9pnauitb foreign key (rev) references revinfo (rev)
alter table t_user_aud add constraint FKs4lwpwui7j4s8kl2bacqv0t0 foreign key (rev) references revinfo (rev)
alter table t_work_category_aud add constraint FK66ggi7odmllq47u1k6y5fx8nl foreign key (rev) references revinfo (rev)
alter table t_work_type_aud add constraint FKlhx8g72fxpmb69wub59h8xr2b foreign key (rev) references revinfo (rev)
alter table user_x_role add constraint FK5f3jmour16c9i1774c45h1s4p foreign key (role_id) references t_role (id)
alter table user_x_role add constraint FK9x2yn9ksiw3glh4oybx6ijilg foreign key (user_id) references t_user (id)
alter table user_x_role_aud add constraint FKahh8f69m1afcmhcynx27tpkqy foreign key (rev) references revinfo (rev)
create table job_advertisement_x_city (job_advertisement_id bigint not null, city_id bigint not null) engine=InnoDB
create table job_advertisement_x_city_aud (rev integer not null, job_advertisement_id bigint not null, city_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, city_id)) engine=InnoDB
create table job_advertisement_x_work_category (job_advertisement_id bigint not null, work_category_id bigint not null) engine=InnoDB
create table job_advertisement_x_work_category_aud (rev integer not null, job_advertisement_id bigint not null, work_category_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, work_category_id)) engine=InnoDB
create table job_advertisement_x_work_type (job_advertisement_id bigint not null, work_type_id bigint not null) engine=InnoDB
create table job_advertisement_x_work_type_aud (rev integer not null, job_advertisement_id bigint not null, work_type_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, work_type_id)) engine=InnoDB
create table jobseeker_x_driving_licence (jobseeker_id bigint not null, driving_licence_id bigint not null) engine=InnoDB
create table jobseeker_x_driving_licence_aud (rev integer not null, jobseeker_id bigint not null, driving_licence_id bigint not null, revtype tinyint, primary key (rev, jobseeker_id, driving_licence_id)) engine=InnoDB
create table jobseeker_x_language_exam (jobseeker_id bigint not null, language_exam_id bigint not null) engine=InnoDB
create table jobseeker_x_language_exam_aud (rev integer not null, jobseeker_id bigint not null, language_exam_id bigint not null, revtype tinyint, primary key (rev, jobseeker_id, language_exam_id)) engine=InnoDB
create table revinfo (rev integer not null auto_increment, revtstmp bigint, primary key (rev)) engine=InnoDB
create table role_x_permission (role_id bigint not null, permission_id bigint not null) engine=InnoDB
create table role_x_permission_aud (rev integer not null, role_id bigint not null, permission_id bigint not null, revtype tinyint, primary key (rev, role_id, permission_id)) engine=InnoDB
create table t_city (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB
create table t_city_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB
create table t_company (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), address varchar(100), contact_person varchar(45), email varchar(100), introduction varchar(255), logo varchar(255), name varchar(45) not null, phone_number varchar(20), primary key (id)) engine=InnoDB
create table t_company_aud (id bigint not null, rev integer not null, revtype tinyint, address varchar(100), contact_person varchar(45), email varchar(100), introduction varchar(255), logo varchar(255), name varchar(45), phone_number varchar(20), primary key (id, rev)) engine=InnoDB
create table t_company_employee (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), company_id bigint, user_id bigint, primary key (id)) engine=InnoDB
create table t_company_employee_aud (id bigint not null, rev integer not null, revtype tinyint, company_id bigint, user_id bigint, primary key (id, rev)) engine=InnoDB
create table t_driving_licence (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), value varchar(45) not null, primary key (id)) engine=InnoDB
create table t_driving_licence_aud (id bigint not null, rev integer not null, revtype tinyint, value varchar(45), primary key (id, rev)) engine=InnoDB
create table t_experience (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), end_date date not null, name varchar(45) not null, start_date date not null, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_experience_aud (id bigint not null, rev integer not null, revtype tinyint, end_date date, name varchar(45), start_date date, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_job_advertisement (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45), place varchar(100), summary varchar(200), text_editor varchar(255), company_id bigint, primary key (id)) engine=InnoDB
create table t_job_advertisement_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), place varchar(100), summary varchar(200), text_editor varchar(255), company_id bigint, primary key (id, rev)) engine=InnoDB
create table t_jobseeker (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), cv varchar(255), address varchar(45), birth_place varchar(45), birth_time date, introduction varchar(255), picture varchar(255), residence varchar(45), work_i_look_for varchar(255), user_id bigint, primary key (id)) engine=InnoDB
create table t_jobseeker_aud (id bigint not null, rev integer not null, revtype tinyint, cv varchar(255), address varchar(45), birth_place varchar(45), birth_time date, introduction varchar(255), picture varchar(255), residence varchar(45), work_i_look_for varchar(255), user_id bigint, primary key (id, rev)) engine=InnoDB
create table t_language_exam (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), language varchar(45) not null, level varchar(45) not null, primary key (id)) engine=InnoDB
create table t_language_exam_aud (id bigint not null, rev integer not null, revtype tinyint, language varchar(45), level varchar(45), primary key (id, rev)) engine=InnoDB
create table t_match (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), state varchar(255) not null, company_id bigint, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_match_aud (id bigint not null, rev integer not null, revtype tinyint, state varchar(255), company_id bigint, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_match_by_advertisement (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), state varchar(255) not null, company_id bigint, job_advertisement_id bigint, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_match_by_advertisement_aud (id bigint not null, rev integer not null, revtype tinyint, state varchar(255), company_id bigint, job_advertisement_id bigint, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_permission (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45) not null, primary key (id)) engine=InnoDB
create table t_permission_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), primary key (id, rev)) engine=InnoDB
create table t_quality (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), city varchar(45) not null, school varchar(100) not null, type varchar(255) not null, year integer not null, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_quality_aud (id bigint not null, rev integer not null, revtype tinyint, city varchar(45), school varchar(100), type varchar(255), year integer, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_role (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45) not null, primary key (id)) engine=InnoDB
create table t_role_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), primary key (id, rev)) engine=InnoDB
create table t_user (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), email varchar(100) not null, name varchar(45), password varchar(100) not null, username varchar(45) not null, primary key (id)) engine=InnoDB
create table t_user_aud (id bigint not null, rev integer not null, revtype tinyint, email varchar(100), name varchar(45), password varchar(100), username varchar(45), primary key (id, rev)) engine=InnoDB
create table t_work_category (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB
create table t_work_category_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB
create table t_work_type (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB
create table t_work_type_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB
create table user_x_role (user_id bigint not null, role_id bigint not null) engine=InnoDB
create table user_x_role_aud (rev integer not null, user_id bigint not null, role_id bigint not null, revtype tinyint, primary key (rev, user_id, role_id)) engine=InnoDB
alter table t_permission add constraint UK_sm6ir47566j150iradcgtsw9y unique (name)
alter table t_user add constraint UK_i6qjjoe560mee5ajdg7v1o6mi unique (email)
alter table t_user add constraint UK_jhib4legehrm4yscx9t3lirqi unique (username)
alter table job_advertisement_x_city add constraint FKye648e8ahxdq7wjxfieif9rw foreign key (city_id) references t_city (id)
alter table job_advertisement_x_city add constraint FKjogmhw4ump0240wl41vvubsjo foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table job_advertisement_x_city_aud add constraint FKrst402tut0ben5cne9yonsja5 foreign key (rev) references revinfo (rev)
alter table job_advertisement_x_work_category add constraint FKdp0liv02fc5w4eidb7xprv2g4 foreign key (work_category_id) references t_work_category (id)
alter table job_advertisement_x_work_category add constraint FKs2aggnd9rl640cyg2d9jgysd8 foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table job_advertisement_x_work_category_aud add constraint FKti44kt6vsbgv9fnl9qnvuwo3h foreign key (rev) references revinfo (rev)
alter table job_advertisement_x_work_type add constraint FKbx5gbe7bjaykfke6dwxe6vvgb foreign key (work_type_id) references t_work_type (id)
alter table job_advertisement_x_work_type add constraint FK1pq5rqudgrj910pjfyu6kfk1v foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table job_advertisement_x_work_type_aud add constraint FK9k2eslrqrpn1fpw71a5rnu39v foreign key (rev) references revinfo (rev)
alter table jobseeker_x_driving_licence add constraint FKqbsbh9iw32eaqhf0j600uf8y foreign key (driving_licence_id) references t_driving_licence (id)
alter table jobseeker_x_driving_licence add constraint FKmkvmxlf9aihd6vangxkrw24m4 foreign key (jobseeker_id) references t_jobseeker (id)
alter table jobseeker_x_driving_licence_aud add constraint FK128kp89al9vcm6f2n6ab4gvrw foreign key (rev) references revinfo (rev)
alter table jobseeker_x_language_exam add constraint FKf9y7mvxxh1nv7899f61nl3djf foreign key (language_exam_id) references t_language_exam (id)
alter table jobseeker_x_language_exam add constraint FK4it7q6n6gw7h07cvpp42c5noa foreign key (jobseeker_id) references t_jobseeker (id)
alter table jobseeker_x_language_exam_aud add constraint FKnr0kvjxyp58d29pqlhr6wwat foreign key (rev) references revinfo (rev)
alter table role_x_permission add constraint FK4pwk3vbvrty2hpq3214okypwj foreign key (permission_id) references t_permission (id)
alter table role_x_permission add constraint FKhdni9x66uaq6n4xyav0nq7rg8 foreign key (role_id) references t_role (id)
alter table role_x_permission_aud add constraint FKfepsn3lx64g80xk5196t0n88t foreign key (rev) references revinfo (rev)
alter table t_city_aud add constraint FKr6exoiw6ycugv7ioyvj4f7w2i foreign key (rev) references revinfo (rev)
alter table t_company_aud add constraint FKvk9m3ms51chv4evfpafh18el foreign key (rev) references revinfo (rev)
alter table t_company_employee add constraint FKlxhe08mlnm9km18f44b5c8uco foreign key (company_id) references t_company (id)
alter table t_company_employee add constraint FK32o4qfp0oc3s8adsyt1a8jwb2 foreign key (user_id) references t_user (id)
alter table t_company_employee_aud add constraint FKiwr3ilj0s1bl0vn4t3af38iwx foreign key (rev) references revinfo (rev)
alter table t_driving_licence_aud add constraint FKtjpbrh57c2sc2jddr72t8ovgx foreign key (rev) references revinfo (rev)
alter table t_experience add constraint FKbmgtg7ymqptlaro4uyn99f3s5 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_experience_aud add constraint FKpkauemkd0ipdwwg1ok2uxprm1 foreign key (rev) references revinfo (rev)
alter table t_job_advertisement add constraint FKp1phbasyym1hg4rm3qwd02gow foreign key (company_id) references t_company (id)
alter table t_job_advertisement_aud add constraint FK14qhowamfj1kluill3nobr9ml foreign key (rev) references revinfo (rev)
alter table t_jobseeker add constraint FK4g0k867aundjt2vd7noxeka81 foreign key (user_id) references t_user (id)
alter table t_jobseeker_aud add constraint FKyao0b6d1p3cg5yp5kmse5tpw foreign key (rev) references revinfo (rev)
alter table t_language_exam_aud add constraint FKthor6eq84ftlbli9q4i9i9ely foreign key (rev) references revinfo (rev)
alter table t_match add constraint FK6aq8qete1jif3a0d9nvcmxr5b foreign key (company_id) references t_company (id)
alter table t_match add constraint FKkk0ccxu13u47ikt6yqm0un797 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_match_aud add constraint FKd46712n1sbwfxf5k8nh4giwx3 foreign key (rev) references revinfo (rev)
alter table t_match_by_advertisement add constraint FKejsv30d599usqtaf1xv3yqji2 foreign key (company_id) references t_company (id)
alter table t_match_by_advertisement add constraint FKlyxmssbhhrwv0fxqyp1hst371 foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table t_match_by_advertisement add constraint FK80vgnfkkfsx2gh9cdhigvpsv1 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_match_by_advertisement_aud add constraint FK95si3guu1ljmoc03rcoycakhl foreign key (rev) references revinfo (rev)
alter table t_permission_aud add constraint FK3d8o1y33wn5kk5dcstp7yieoq foreign key (rev) references revinfo (rev)
alter table t_quality add constraint FKkpqbphvaxphe0xh78mw2f6qo1 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_quality_aud add constraint FKe3qn3ffpn61sq65xlmhwa4cu6 foreign key (rev) references revinfo (rev)
alter table t_role_aud add constraint FK9se3aaaqj71s2edoy9pnauitb foreign key (rev) references revinfo (rev)
alter table t_user_aud add constraint FKs4lwpwui7j4s8kl2bacqv0t0 foreign key (rev) references revinfo (rev)
alter table t_work_category_aud add constraint FK66ggi7odmllq47u1k6y5fx8nl foreign key (rev) references revinfo (rev)
alter table t_work_type_aud add constraint FKlhx8g72fxpmb69wub59h8xr2b foreign key (rev) references revinfo (rev)
alter table user_x_role add constraint FK5f3jmour16c9i1774c45h1s4p foreign key (role_id) references t_role (id)
alter table user_x_role add constraint FK9x2yn9ksiw3glh4oybx6ijilg foreign key (user_id) references t_user (id)
alter table user_x_role_aud add constraint FKahh8f69m1afcmhcynx27tpkqy foreign key (rev) references revinfo (rev)
create table job_advertisement_x_city (job_advertisement_id bigint not null, city_id bigint not null) engine=InnoDB
create table job_advertisement_x_city_aud (rev integer not null, job_advertisement_id bigint not null, city_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, city_id)) engine=InnoDB
create table job_advertisement_x_work_category (job_advertisement_id bigint not null, work_category_id bigint not null) engine=InnoDB
create table job_advertisement_x_work_category_aud (rev integer not null, job_advertisement_id bigint not null, work_category_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, work_category_id)) engine=InnoDB
create table job_advertisement_x_work_type (job_advertisement_id bigint not null, work_type_id bigint not null) engine=InnoDB
create table job_advertisement_x_work_type_aud (rev integer not null, job_advertisement_id bigint not null, work_type_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, work_type_id)) engine=InnoDB
create table jobseeker_x_driving_licence (jobseeker_id bigint not null, driving_licence_id bigint not null) engine=InnoDB
create table jobseeker_x_driving_licence_aud (rev integer not null, jobseeker_id bigint not null, driving_licence_id bigint not null, revtype tinyint, primary key (rev, jobseeker_id, driving_licence_id)) engine=InnoDB
create table jobseeker_x_language_exam (jobseeker_id bigint not null, language_exam_id bigint not null) engine=InnoDB
create table jobseeker_x_language_exam_aud (rev integer not null, jobseeker_id bigint not null, language_exam_id bigint not null, revtype tinyint, primary key (rev, jobseeker_id, language_exam_id)) engine=InnoDB
create table revinfo (rev integer not null auto_increment, revtstmp bigint, primary key (rev)) engine=InnoDB
create table role_x_permission (role_id bigint not null, permission_id bigint not null) engine=InnoDB
create table role_x_permission_aud (rev integer not null, role_id bigint not null, permission_id bigint not null, revtype tinyint, primary key (rev, role_id, permission_id)) engine=InnoDB
create table t_city (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB
create table t_city_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB
create table t_company (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), address varchar(100), contact_person varchar(45), email varchar(100), introduction varchar(255), logo varchar(255), name varchar(45) not null, phone_number varchar(20), primary key (id)) engine=InnoDB
create table t_company_aud (id bigint not null, rev integer not null, revtype tinyint, address varchar(100), contact_person varchar(45), email varchar(100), introduction varchar(255), logo varchar(255), name varchar(45), phone_number varchar(20), primary key (id, rev)) engine=InnoDB
create table t_company_employee (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), company_id bigint, user_id bigint, primary key (id)) engine=InnoDB
create table t_company_employee_aud (id bigint not null, rev integer not null, revtype tinyint, company_id bigint, user_id bigint, primary key (id, rev)) engine=InnoDB
create table t_driving_licence (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), value varchar(45) not null, primary key (id)) engine=InnoDB
create table t_driving_licence_aud (id bigint not null, rev integer not null, revtype tinyint, value varchar(45), primary key (id, rev)) engine=InnoDB
create table t_experience (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), end_date date not null, name varchar(45) not null, start_date date not null, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_experience_aud (id bigint not null, rev integer not null, revtype tinyint, end_date date, name varchar(45), start_date date, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_job_advertisement (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45), place varchar(100), summary varchar(200), text_editor varchar(255), company_id bigint, primary key (id)) engine=InnoDB
create table t_job_advertisement_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), place varchar(100), summary varchar(200), text_editor varchar(255), company_id bigint, primary key (id, rev)) engine=InnoDB
create table t_jobseeker (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), cv varchar(255), address varchar(45), birth_place varchar(45), birth_time date, introduction varchar(255), picture varchar(255), residence varchar(45), work_i_look_for varchar(255), user_id bigint, primary key (id)) engine=InnoDB
create table t_jobseeker_aud (id bigint not null, rev integer not null, revtype tinyint, cv varchar(255), address varchar(45), birth_place varchar(45), birth_time date, introduction varchar(255), picture varchar(255), residence varchar(45), work_i_look_for varchar(255), user_id bigint, primary key (id, rev)) engine=InnoDB
create table t_language_exam (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), language varchar(45) not null, level varchar(45) not null, primary key (id)) engine=InnoDB
create table t_language_exam_aud (id bigint not null, rev integer not null, revtype tinyint, language varchar(45), level varchar(45), primary key (id, rev)) engine=InnoDB
create table t_match (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), state varchar(255) not null, company_id bigint, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_match_aud (id bigint not null, rev integer not null, revtype tinyint, state varchar(255), company_id bigint, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_match_by_advertisement (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), state varchar(255) not null, company_id bigint, job_advertisement_id bigint, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_match_by_advertisement_aud (id bigint not null, rev integer not null, revtype tinyint, state varchar(255), company_id bigint, job_advertisement_id bigint, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_permission (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45) not null, primary key (id)) engine=InnoDB
create table t_permission_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), primary key (id, rev)) engine=InnoDB
create table t_quality (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), city varchar(45) not null, school varchar(100) not null, type varchar(255) not null, year integer not null, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_quality_aud (id bigint not null, rev integer not null, revtype tinyint, city varchar(45), school varchar(100), type varchar(255), year integer, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_role (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45) not null, primary key (id)) engine=InnoDB
create table t_role_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), primary key (id, rev)) engine=InnoDB
create table t_user (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), email varchar(100) not null, name varchar(45), password varchar(100) not null, username varchar(45) not null, primary key (id)) engine=InnoDB
create table t_user_aud (id bigint not null, rev integer not null, revtype tinyint, email varchar(100), name varchar(45), password varchar(100), username varchar(45), primary key (id, rev)) engine=InnoDB
create table t_work_category (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB
create table t_work_category_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB
create table t_work_type (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB
create table t_work_type_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB
create table user_x_role (user_id bigint not null, role_id bigint not null) engine=InnoDB
create table user_x_role_aud (rev integer not null, user_id bigint not null, role_id bigint not null, revtype tinyint, primary key (rev, user_id, role_id)) engine=InnoDB
alter table t_permission add constraint UK_sm6ir47566j150iradcgtsw9y unique (name)
alter table t_user add constraint UK_i6qjjoe560mee5ajdg7v1o6mi unique (email)
alter table t_user add constraint UK_jhib4legehrm4yscx9t3lirqi unique (username)
alter table job_advertisement_x_city add constraint FKye648e8ahxdq7wjxfieif9rw foreign key (city_id) references t_city (id)
alter table job_advertisement_x_city add constraint FKjogmhw4ump0240wl41vvubsjo foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table job_advertisement_x_city_aud add constraint FKrst402tut0ben5cne9yonsja5 foreign key (rev) references revinfo (rev)
alter table job_advertisement_x_work_category add constraint FKdp0liv02fc5w4eidb7xprv2g4 foreign key (work_category_id) references t_work_category (id)
alter table job_advertisement_x_work_category add constraint FKs2aggnd9rl640cyg2d9jgysd8 foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table job_advertisement_x_work_category_aud add constraint FKti44kt6vsbgv9fnl9qnvuwo3h foreign key (rev) references revinfo (rev)
alter table job_advertisement_x_work_type add constraint FKbx5gbe7bjaykfke6dwxe6vvgb foreign key (work_type_id) references t_work_type (id)
alter table job_advertisement_x_work_type add constraint FK1pq5rqudgrj910pjfyu6kfk1v foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table job_advertisement_x_work_type_aud add constraint FK9k2eslrqrpn1fpw71a5rnu39v foreign key (rev) references revinfo (rev)
alter table jobseeker_x_driving_licence add constraint FKqbsbh9iw32eaqhf0j600uf8y foreign key (driving_licence_id) references t_driving_licence (id)
alter table jobseeker_x_driving_licence add constraint FKmkvmxlf9aihd6vangxkrw24m4 foreign key (jobseeker_id) references t_jobseeker (id)
alter table jobseeker_x_driving_licence_aud add constraint FK128kp89al9vcm6f2n6ab4gvrw foreign key (rev) references revinfo (rev)
alter table jobseeker_x_language_exam add constraint FKf9y7mvxxh1nv7899f61nl3djf foreign key (language_exam_id) references t_language_exam (id)
alter table jobseeker_x_language_exam add constraint FK4it7q6n6gw7h07cvpp42c5noa foreign key (jobseeker_id) references t_jobseeker (id)
alter table jobseeker_x_language_exam_aud add constraint FKnr0kvjxyp58d29pqlhr6wwat foreign key (rev) references revinfo (rev)
alter table role_x_permission add constraint FK4pwk3vbvrty2hpq3214okypwj foreign key (permission_id) references t_permission (id)
alter table role_x_permission add constraint FKhdni9x66uaq6n4xyav0nq7rg8 foreign key (role_id) references t_role (id)
alter table role_x_permission_aud add constraint FKfepsn3lx64g80xk5196t0n88t foreign key (rev) references revinfo (rev)
alter table t_city_aud add constraint FKr6exoiw6ycugv7ioyvj4f7w2i foreign key (rev) references revinfo (rev)
alter table t_company_aud add constraint FKvk9m3ms51chv4evfpafh18el foreign key (rev) references revinfo (rev)
alter table t_company_employee add constraint FKlxhe08mlnm9km18f44b5c8uco foreign key (company_id) references t_company (id)
alter table t_company_employee add constraint FK32o4qfp0oc3s8adsyt1a8jwb2 foreign key (user_id) references t_user (id)
alter table t_company_employee_aud add constraint FKiwr3ilj0s1bl0vn4t3af38iwx foreign key (rev) references revinfo (rev)
alter table t_driving_licence_aud add constraint FKtjpbrh57c2sc2jddr72t8ovgx foreign key (rev) references revinfo (rev)
alter table t_experience add constraint FKbmgtg7ymqptlaro4uyn99f3s5 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_experience_aud add constraint FKpkauemkd0ipdwwg1ok2uxprm1 foreign key (rev) references revinfo (rev)
alter table t_job_advertisement add constraint FKp1phbasyym1hg4rm3qwd02gow foreign key (company_id) references t_company (id)
alter table t_job_advertisement_aud add constraint FK14qhowamfj1kluill3nobr9ml foreign key (rev) references revinfo (rev)
alter table t_jobseeker add constraint FK4g0k867aundjt2vd7noxeka81 foreign key (user_id) references t_user (id)
alter table t_jobseeker_aud add constraint FKyao0b6d1p3cg5yp5kmse5tpw foreign key (rev) references revinfo (rev)
alter table t_language_exam_aud add constraint FKthor6eq84ftlbli9q4i9i9ely foreign key (rev) references revinfo (rev)
alter table t_match add constraint FK6aq8qete1jif3a0d9nvcmxr5b foreign key (company_id) references t_company (id)
alter table t_match add constraint FKkk0ccxu13u47ikt6yqm0un797 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_match_aud add constraint FKd46712n1sbwfxf5k8nh4giwx3 foreign key (rev) references revinfo (rev)
alter table t_match_by_advertisement add constraint FKejsv30d599usqtaf1xv3yqji2 foreign key (company_id) references t_company (id)
alter table t_match_by_advertisement add constraint FKlyxmssbhhrwv0fxqyp1hst371 foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table t_match_by_advertisement add constraint FK80vgnfkkfsx2gh9cdhigvpsv1 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_match_by_advertisement_aud add constraint FK95si3guu1ljmoc03rcoycakhl foreign key (rev) references revinfo (rev)
alter table t_permission_aud add constraint FK3d8o1y33wn5kk5dcstp7yieoq foreign key (rev) references revinfo (rev)
alter table t_quality add constraint FKkpqbphvaxphe0xh78mw2f6qo1 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_quality_aud add constraint FKe3qn3ffpn61sq65xlmhwa4cu6 foreign key (rev) references revinfo (rev)
alter table t_role_aud add constraint FK9se3aaaqj71s2edoy9pnauitb foreign key (rev) references revinfo (rev)
alter table t_user_aud add constraint FKs4lwpwui7j4s8kl2bacqv0t0 foreign key (rev) references revinfo (rev)
alter table t_work_category_aud add constraint FK66ggi7odmllq47u1k6y5fx8nl foreign key (rev) references revinfo (rev)
alter table t_work_type_aud add constraint FKlhx8g72fxpmb69wub59h8xr2b foreign key (rev) references revinfo (rev)
alter table user_x_role add constraint FK5f3jmour16c9i1774c45h1s4p foreign key (role_id) references t_role (id)
alter table user_x_role add constraint FK9x2yn9ksiw3glh4oybx6ijilg foreign key (user_id) references t_user (id)
alter table user_x_role_aud add constraint FKahh8f69m1afcmhcynx27tpkqy foreign key (rev) references revinfo (rev)
create table job_advertisement_x_city (job_advertisement_id bigint not null, city_id bigint not null) engine=InnoDB
create table job_advertisement_x_city_aud (rev integer not null, job_advertisement_id bigint not null, city_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, city_id)) engine=InnoDB
create table job_advertisement_x_work_category (job_advertisement_id bigint not null, work_category_id bigint not null) engine=InnoDB
create table job_advertisement_x_work_category_aud (rev integer not null, job_advertisement_id bigint not null, work_category_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, work_category_id)) engine=InnoDB
create table job_advertisement_x_work_type (job_advertisement_id bigint not null, work_type_id bigint not null) engine=InnoDB
create table job_advertisement_x_work_type_aud (rev integer not null, job_advertisement_id bigint not null, work_type_id bigint not null, revtype tinyint, primary key (rev, job_advertisement_id, work_type_id)) engine=InnoDB
create table jobseeker_x_driving_licence (jobseeker_id bigint not null, driving_licence_id bigint not null) engine=InnoDB
create table jobseeker_x_driving_licence_aud (rev integer not null, jobseeker_id bigint not null, driving_licence_id bigint not null, revtype tinyint, primary key (rev, jobseeker_id, driving_licence_id)) engine=InnoDB
create table jobseeker_x_language_exam (jobseeker_id bigint not null, language_exam_id bigint not null) engine=InnoDB
create table jobseeker_x_language_exam_aud (rev integer not null, jobseeker_id bigint not null, language_exam_id bigint not null, revtype tinyint, primary key (rev, jobseeker_id, language_exam_id)) engine=InnoDB
create table revinfo (rev integer not null auto_increment, revtstmp bigint, primary key (rev)) engine=InnoDB
create table role_x_permission (role_id bigint not null, permission_id bigint not null) engine=InnoDB
create table role_x_permission_aud (rev integer not null, role_id bigint not null, permission_id bigint not null, revtype tinyint, primary key (rev, role_id, permission_id)) engine=InnoDB
create table t_city (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB
create table t_city_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB
create table t_company (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), address varchar(100), contact_person varchar(45), email varchar(100), introduction varchar(255), logo varchar(255), name varchar(45) not null, phone_number varchar(20), primary key (id)) engine=InnoDB
create table t_company_aud (id bigint not null, rev integer not null, revtype tinyint, address varchar(100), contact_person varchar(45), email varchar(100), introduction varchar(255), logo varchar(255), name varchar(45), phone_number varchar(20), primary key (id, rev)) engine=InnoDB
create table t_company_employee (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), company_id bigint, user_id bigint, primary key (id)) engine=InnoDB
create table t_company_employee_aud (id bigint not null, rev integer not null, revtype tinyint, company_id bigint, user_id bigint, primary key (id, rev)) engine=InnoDB
create table t_driving_licence (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), value varchar(45) not null, primary key (id)) engine=InnoDB
create table t_driving_licence_aud (id bigint not null, rev integer not null, revtype tinyint, value varchar(45), primary key (id, rev)) engine=InnoDB
create table t_experience (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), end_date date not null, name varchar(45) not null, start_date date not null, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_experience_aud (id bigint not null, rev integer not null, revtype tinyint, end_date date, name varchar(45), start_date date, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_job_advertisement (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45), place varchar(100), summary varchar(200), text_editor varchar(255), company_id bigint, primary key (id)) engine=InnoDB
create table t_job_advertisement_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), place varchar(100), summary varchar(200), text_editor varchar(255), company_id bigint, primary key (id, rev)) engine=InnoDB
create table t_jobseeker (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), cv varchar(255), address varchar(45), birth_place varchar(45), birth_time date, introduction varchar(255), picture varchar(255), residence varchar(45), work_i_look_for varchar(255), user_id bigint, primary key (id)) engine=InnoDB
create table t_jobseeker_aud (id bigint not null, rev integer not null, revtype tinyint, cv varchar(255), address varchar(45), birth_place varchar(45), birth_time date, introduction varchar(255), picture varchar(255), residence varchar(45), work_i_look_for varchar(255), user_id bigint, primary key (id, rev)) engine=InnoDB
create table t_language_exam (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), language varchar(45) not null, level varchar(45) not null, primary key (id)) engine=InnoDB
create table t_language_exam_aud (id bigint not null, rev integer not null, revtype tinyint, language varchar(45), level varchar(45), primary key (id, rev)) engine=InnoDB
create table t_match (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), state varchar(255) not null, company_id bigint, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_match_aud (id bigint not null, rev integer not null, revtype tinyint, state varchar(255), company_id bigint, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_match_by_advertisement (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), state varchar(255) not null, company_id bigint, job_advertisement_id bigint, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_match_by_advertisement_aud (id bigint not null, rev integer not null, revtype tinyint, state varchar(255), company_id bigint, job_advertisement_id bigint, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_permission (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45) not null, primary key (id)) engine=InnoDB
create table t_permission_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), primary key (id, rev)) engine=InnoDB
create table t_quality (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), city varchar(45) not null, school varchar(100) not null, type varchar(255) not null, year integer not null, jobseeker_id bigint, primary key (id)) engine=InnoDB
create table t_quality_aud (id bigint not null, rev integer not null, revtype tinyint, city varchar(45), school varchar(100), type varchar(255), year integer, jobseeker_id bigint, primary key (id, rev)) engine=InnoDB
create table t_role (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(45) not null, primary key (id)) engine=InnoDB
create table t_role_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(45), primary key (id, rev)) engine=InnoDB
create table t_user (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), email varchar(100) not null, name varchar(45), password varchar(100) not null, username varchar(45) not null, primary key (id)) engine=InnoDB
create table t_user_aud (id bigint not null, rev integer not null, revtype tinyint, email varchar(100), name varchar(45), password varchar(100), username varchar(45), primary key (id, rev)) engine=InnoDB
create table t_work_category (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB
create table t_work_category_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB
create table t_work_type (id bigint not null auto_increment, created_by varchar(255), creation_time datetime(6) not null, modified_by varchar(255), modified_time datetime(6), name varchar(255) not null, primary key (id)) engine=InnoDB
create table t_work_type_aud (id bigint not null, rev integer not null, revtype tinyint, name varchar(255), primary key (id, rev)) engine=InnoDB
create table user_x_role (user_id bigint not null, role_id bigint not null) engine=InnoDB
create table user_x_role_aud (rev integer not null, user_id bigint not null, role_id bigint not null, revtype tinyint, primary key (rev, user_id, role_id)) engine=InnoDB
alter table t_permission add constraint UK_sm6ir47566j150iradcgtsw9y unique (name)
alter table t_user add constraint UK_i6qjjoe560mee5ajdg7v1o6mi unique (email)
alter table t_user add constraint UK_jhib4legehrm4yscx9t3lirqi unique (username)
alter table job_advertisement_x_city add constraint FKye648e8ahxdq7wjxfieif9rw foreign key (city_id) references t_city (id)
alter table job_advertisement_x_city add constraint FKjogmhw4ump0240wl41vvubsjo foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table job_advertisement_x_city_aud add constraint FKrst402tut0ben5cne9yonsja5 foreign key (rev) references revinfo (rev)
alter table job_advertisement_x_work_category add constraint FKdp0liv02fc5w4eidb7xprv2g4 foreign key (work_category_id) references t_work_category (id)
alter table job_advertisement_x_work_category add constraint FKs2aggnd9rl640cyg2d9jgysd8 foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table job_advertisement_x_work_category_aud add constraint FKti44kt6vsbgv9fnl9qnvuwo3h foreign key (rev) references revinfo (rev)
alter table job_advertisement_x_work_type add constraint FKbx5gbe7bjaykfke6dwxe6vvgb foreign key (work_type_id) references t_work_type (id)
alter table job_advertisement_x_work_type add constraint FK1pq5rqudgrj910pjfyu6kfk1v foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table job_advertisement_x_work_type_aud add constraint FK9k2eslrqrpn1fpw71a5rnu39v foreign key (rev) references revinfo (rev)
alter table jobseeker_x_driving_licence add constraint FKqbsbh9iw32eaqhf0j600uf8y foreign key (driving_licence_id) references t_driving_licence (id)
alter table jobseeker_x_driving_licence add constraint FKmkvmxlf9aihd6vangxkrw24m4 foreign key (jobseeker_id) references t_jobseeker (id)
alter table jobseeker_x_driving_licence_aud add constraint FK128kp89al9vcm6f2n6ab4gvrw foreign key (rev) references revinfo (rev)
alter table jobseeker_x_language_exam add constraint FKf9y7mvxxh1nv7899f61nl3djf foreign key (language_exam_id) references t_language_exam (id)
alter table jobseeker_x_language_exam add constraint FK4it7q6n6gw7h07cvpp42c5noa foreign key (jobseeker_id) references t_jobseeker (id)
alter table jobseeker_x_language_exam_aud add constraint FKnr0kvjxyp58d29pqlhr6wwat foreign key (rev) references revinfo (rev)
alter table role_x_permission add constraint FK4pwk3vbvrty2hpq3214okypwj foreign key (permission_id) references t_permission (id)
alter table role_x_permission add constraint FKhdni9x66uaq6n4xyav0nq7rg8 foreign key (role_id) references t_role (id)
alter table role_x_permission_aud add constraint FKfepsn3lx64g80xk5196t0n88t foreign key (rev) references revinfo (rev)
alter table t_city_aud add constraint FKr6exoiw6ycugv7ioyvj4f7w2i foreign key (rev) references revinfo (rev)
alter table t_company_aud add constraint FKvk9m3ms51chv4evfpafh18el foreign key (rev) references revinfo (rev)
alter table t_company_employee add constraint FKlxhe08mlnm9km18f44b5c8uco foreign key (company_id) references t_company (id)
alter table t_company_employee add constraint FK32o4qfp0oc3s8adsyt1a8jwb2 foreign key (user_id) references t_user (id)
alter table t_company_employee_aud add constraint FKiwr3ilj0s1bl0vn4t3af38iwx foreign key (rev) references revinfo (rev)
alter table t_driving_licence_aud add constraint FKtjpbrh57c2sc2jddr72t8ovgx foreign key (rev) references revinfo (rev)
alter table t_experience add constraint FKbmgtg7ymqptlaro4uyn99f3s5 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_experience_aud add constraint FKpkauemkd0ipdwwg1ok2uxprm1 foreign key (rev) references revinfo (rev)
alter table t_job_advertisement add constraint FKp1phbasyym1hg4rm3qwd02gow foreign key (company_id) references t_company (id)
alter table t_job_advertisement_aud add constraint FK14qhowamfj1kluill3nobr9ml foreign key (rev) references revinfo (rev)
alter table t_jobseeker add constraint FK4g0k867aundjt2vd7noxeka81 foreign key (user_id) references t_user (id)
alter table t_jobseeker_aud add constraint FKyao0b6d1p3cg5yp5kmse5tpw foreign key (rev) references revinfo (rev)
alter table t_language_exam_aud add constraint FKthor6eq84ftlbli9q4i9i9ely foreign key (rev) references revinfo (rev)
alter table t_match add constraint FK6aq8qete1jif3a0d9nvcmxr5b foreign key (company_id) references t_company (id)
alter table t_match add constraint FKkk0ccxu13u47ikt6yqm0un797 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_match_aud add constraint FKd46712n1sbwfxf5k8nh4giwx3 foreign key (rev) references revinfo (rev)
alter table t_match_by_advertisement add constraint FKejsv30d599usqtaf1xv3yqji2 foreign key (company_id) references t_company (id)
alter table t_match_by_advertisement add constraint FKlyxmssbhhrwv0fxqyp1hst371 foreign key (job_advertisement_id) references t_job_advertisement (id)
alter table t_match_by_advertisement add constraint FK80vgnfkkfsx2gh9cdhigvpsv1 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_match_by_advertisement_aud add constraint FK95si3guu1ljmoc03rcoycakhl foreign key (rev) references revinfo (rev)
alter table t_permission_aud add constraint FK3d8o1y33wn5kk5dcstp7yieoq foreign key (rev) references revinfo (rev)
alter table t_quality add constraint FKkpqbphvaxphe0xh78mw2f6qo1 foreign key (jobseeker_id) references t_jobseeker (id)
alter table t_quality_aud add constraint FKe3qn3ffpn61sq65xlmhwa4cu6 foreign key (rev) references revinfo (rev)
alter table t_role_aud add constraint FK9se3aaaqj71s2edoy9pnauitb foreign key (rev) references revinfo (rev)
alter table t_user_aud add constraint FKs4lwpwui7j4s8kl2bacqv0t0 foreign key (rev) references revinfo (rev)
alter table t_work_category_aud add constraint FK66ggi7odmllq47u1k6y5fx8nl foreign key (rev) references revinfo (rev)
alter table t_work_type_aud add constraint FKlhx8g72fxpmb69wub59h8xr2b foreign key (rev) references revinfo (rev)
alter table user_x_role add constraint FK5f3jmour16c9i1774c45h1s4p foreign key (role_id) references t_role (id)
alter table user_x_role add constraint FK9x2yn9ksiw3glh4oybx6ijilg foreign key (user_id) references t_user (id)
alter table user_x_role_aud add constraint FKahh8f69m1afcmhcynx27tpkqy foreign key (rev) references revinfo (rev)
